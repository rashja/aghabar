## Menu routes

```mermaid
graph LR
    MENU[Menu >> src/components/Menu]-->HOME[Home >> src/components/OrdersList]
    MENU -->AddRequest[New Request >> src/components/AddRequest]
    MENU -->Supports[Support >> src/components/Supports]
    MENU -->ExternalTransactions[External Transactions >> src/components/ExternalTransactions]
    MENU -->Announcements[Announcements >> src/components/Announcements]
    MENU -->CriticsAndSuggestions[Critics And Suggestions >> src/components/CriticsAndSuggestions]
    MENU -->FinancialManagment[New Request >> src/components/FinancialManagment]
    MENU -->Exit
```

## Menu includes all pages but Login page

```mermaid
graph TD
    renderChooseLogin[login >>   src/components/Login renderChooseLogin]
    renderChooseLogin-->renderSignIn[renderSignIn]
    renderChooseLogin-->renderSignUp[renderSignUp]
    renderSignIn-->renderOtpCode[renderOtpCode]
    renderSignUp-->renderSignIn
    renderOtpCode--success-->Home[Home >> src/components/OrdersList]

    Home -->renderTabs[renderTabs]
    renderTabs --> currentOrders[Tab -> renderDoingOrders]
    CurrentOrderItem --btn pursuit request--> pursuitRequest[pursuit request >> src/components/PursuitRequest]
    renderTabs --> successOrders[Tab -> renderSuccessOrders]
    renderTabs --> cancelOrders[Tab -> renderFailedOrders]
    renderTabs --> addRequest[Btn Add New Load -> New Request >> src/components/AddRequest]
    addRequest --> renderHeaders[renderHeaders]
    addRequest --> renderProvinceAndCityModal[renderProvinceAndCityModal]
    addRequest --> renderMoreDetailsModal[renderMoreDetailsModal]
    addRequest --> handleConfirmAddresses[handleConfirmAddresses]
    handleConfirmAddresses --> infoBar[InfoBar >> src/components/InfoBar]
    infoBar --success--> Home
    currentOrders -->CurrentOrderItem[src/components/CurrentOrderItem]
    successOrders -->SuccessOrderItem[src/components/SuccessOrderItem]
    SuccessOrderItem --btn show details -->SuccessDetails
    SuccessOrderItem --btn resend request-->addRequest
    cancelOrders -->CancelOrderItem[src/components/CancelOrderItem]
    CancelOrderItem --btn resend request-->addRequest

    Menu --> Home
    Menu --> Supports
    Supports -->renderSupportInformations[renderSupportInformations]
    Menu --> ExternalTransactions
    ExternalTransactions --btn details order--> renderInformations[renderInformations]
    Menu --> Announcements
    Announcements --> AnnouncementItem
    Menu --> CriticsAndSuggestions
    CriticsAndSuggestions --after fill the fields--> handleSubmit
    Menu --> FinancialManagment
    FinancialManagment --> renderChargePayment[renderChargePayment]
    FinancialManagment --> renderTransactions[renderTransactions]
    renderTransactions --> FinancialTransactions
    Menu --> Exit
    Exit --> renderChooseLogin

```
