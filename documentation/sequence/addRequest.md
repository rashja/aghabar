### add request proccess

```mermaid
    sequenceDiagram
        user-->>MapIrserver: https://map.ir/search/v2
        par success
            MapIrserver-->>user: status:succ
        and failed
            MapIrserver-->>user:status:error
        end

        user-->>server: BaseUrl/UAPI7
            Note over server,user:method name : getListTruckAndPackageByClassBarId
        par success
            server-->>user: status:succ
        and faild
            server-->>user: status:error
        end

        user-->>server: BaseUrl/UAPI14
            Note over server,user:method name : newRequest
        par success
            server-->>user: status:succ
        and faild
            server-->>user: status:error
        end
```
