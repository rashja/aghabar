```mermaid
    sequenceDiagram
        user->>server: BaseUrl/UAPI8
            Note over server,user:method name : checkVersionOfSystem
        par success
            server-->>user: status:succ
        and faild
            server-->>user: status:error
        end

        user->>server: BaseUrl/UAPI32
            Note over server,user:method name : oldTransactions
        par success
            server-->>user: status:succ
               user-->>server: BaseUrl/UAPI16
                    Note over server,user:method name : getCurrentItemTrOfUsersByUserId
                par success
                    server-->>user: status:succ
                and faild
                    server-->>user: status:error
                end
        and faild
            server-->>user: status:error
        end

        user->>server: BaseUrl/UAPI33
            Note over server,user:method name : cancelingTransactions
        par success
            server-->>user: status:succ
                user-->>server: BaseUrl/UAPI21
                    Note over server,user:method name : resendRequest
                par success
                    server-->>user: status:succ
                and faild
                    server-->>user: status:error
                end
        and faild
            server-->>user: status:error
        end

        user->>server: BaseUrl/UAPI15
            Note over server,user:method name : currentTransactions
        par success
            server-->>user: status:succ
                 user-->>server: BaseUrl/UAPI21
                    Note over server,user:method name : resendRequest
                par success
                    server-->>user: status:succ
                and faild
                    server-->>user: status:error
                end
        and faild
            server-->>user: status:error
        end
```
