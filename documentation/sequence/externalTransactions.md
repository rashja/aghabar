```mermaid
    sequenceDiagram
        user-->>server: BaseUrl/UAPI26
        Note over server,user:method name : getTrsOutOfSystem
        par success
            server-->>user: status:succ
        and failed
            server-->>user:status:error
        end
```
