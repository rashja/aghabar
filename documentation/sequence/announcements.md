```mermaid
    sequenceDiagram
        user-->>server: BaseUrl/UAPI36
        Note over server,user:method name : getNotifications
        par success
            server-->>user: status:succ
        and failed
            server-->>user:status:error
        end
```
