```mermaid
    sequenceDiagram
        user-->>server: BaseUrl/UAPI12
        Note over server,user:method name : getRules
        par success
            server-->>user: status:succ
        and faild
            server-->>user: status:error
        end
```

### sign up proccess

```mermaid
    sequenceDiagram
        user-->>server: BaseUrl/UAPI8
        Note over server,user:method name : checkVersionOfSystem
        par success
            server-->>user: status:succ
            user-->>server: BaseUrl/UAPI1
            Note over server,user:method name : register
            par success
                server-->>user: status:succ
            and error
                server-->>user: error message
            end
        and failed
            server-->>user:status:error
        end
```

### sign in proccess

```mermaid
    sequenceDiagram
        user-->>server: BaseUrl/UAPI3
        Note over server,user:method name : login
        par success
            server-->>user: status:succ
                    user-->>server: BaseUrl/UAPI4
        Note over server,user:method name : checkLogin
        par success
            server-->>user: status:succ
        and failed
            server-->>user:status:error
        end
        loop Healthcheck
            server->>user: till log in
        end
        and error
        server -->user:error message
        and failed
            server-->>user:status:error
        end
        loop Healthcheck
            server->>user: every minute
        end


```
