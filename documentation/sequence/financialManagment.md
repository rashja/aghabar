```mermaid
    sequenceDiagram
        user-->>server: BaseUrl/UAPI34
        Note over server,user:method name : getPaymentList
        par success
            server-->>user: status:succ
        and failed
            server-->>user:status:error
        end
```
