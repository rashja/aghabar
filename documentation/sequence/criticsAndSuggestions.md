```mermaid
    sequenceDiagram
        user-->>server: BaseUrl/UAPI35
        Note over server,user:method name : setComments
        par success
            server-->>user: status:succ
        and failed
            server-->>user:status:error
        end
```
