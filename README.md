## Project Name & Pitch

### Taban bar

A web app used to transportation of loads, built with React, Redux, Redux-Saga , Sass, Bootstap

[link to Web app!](http://webapp.aghabar.com)

## Base Url

https://aghabar.com/

## Installation and Setup Instructions

Clone down this repository. You will need node and npm installed globally on your machine.

Installation:

* npm install

To Run Test Suite:

* npm test

To Start Server:

* npm start

To Visit App:

* localhost:3000 

Server Requirement

* npm run build
