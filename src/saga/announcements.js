import { put, takeEvery } from "redux-saga/effects";
import * as announcements from "../actions/announcements";
import { showToastify, showLoading, hideLoading } from "../actions/loading";
import { fecthAnnouncements } from "../apis/announcements";

/*--------------------------- cancel orders ---------------------- */
function* getAnnouncements({ time, tok, userId, pageNum }) {
  yield put(showLoading());

  const result = yield fecthAnnouncements(time, tok, userId, pageNum);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(announcements.payloadAnnouncements(result.data));
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*------------------------------------------------------------- */

function* actionAnnouncementsWatcher() {
  yield takeEvery(announcements.REQUEST_ANNOUNCEMENT, getAnnouncements);
}

export function* announcementsWatcher() {
  yield actionAnnouncementsWatcher();
}
