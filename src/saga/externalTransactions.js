import { put, takeEvery } from "redux-saga/effects";
import * as externalTransactions from "../actions/externalTransactions";
import { showToastify, showLoading, hideLoading } from "../actions/loading";
import {
  fetchExternalTransactions,
  fetchFinishWork,
} from "../apis/externalTransactions";

/*---------------------------- payment List ----------------------- */
function* externalTransactionsList({ time, tok, userId, pageNum }) {
  yield put(showLoading());

  const result = yield fetchExternalTransactions(time, tok, userId, pageNum);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(externalTransactions.payloadExtrenalTransactions(result.data));
    yield put(externalTransactions.finishWorkSuccessOf());
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*---------------------------- finish work ------------------------ */
function* finishWork({ token, time, user_id, trId }) {
  yield put(showLoading());

  const result = yield fetchFinishWork(token, time, user_id, trId);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(externalTransactions.finishWorkSuccessOn());
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*----------------------------------------------------------------- */
function* actionExternalTransactionsWatcher() {
  yield takeEvery(
    externalTransactions.REQUEST_EXTERNAL_TRANSACTIONS,
    externalTransactionsList
  );
  yield takeEvery(externalTransactions.REQUEST_FINISH_WORK, finishWork);
}

export function* externalTransactionsWatcher() {
  yield actionExternalTransactionsWatcher();
}
