import { put, takeEvery } from "redux-saga/effects";
import * as financialManagement from "../actions/financialManagement";
import { showToastify, showLoading, hideLoading } from "../actions/loading";
import { fecthPaymentList } from "../apis/financialManagement";

/*---------------------------- payment List ----------------------- */
function* paymentList({ tok, time, user_id }) {
  yield put(showLoading());

  const result = yield fecthPaymentList(tok, time, user_id);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(financialManagement.payloadPaymentList(result.data));
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*----------------------------------------------------------------- */
function* actionFinancialManagementWatcher() {
  yield takeEvery(financialManagement.REQUEST_PAYMRNT_LIST, paymentList);
}

export function* financialManagementWatcher() {
  yield actionFinancialManagementWatcher();
}
