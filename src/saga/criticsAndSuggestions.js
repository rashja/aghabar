import { put, takeEvery } from "redux-saga/effects";
import * as criticsAndSuggestions from "../actions/criticsAndSuggestions";
import { showToastify, showLoading, hideLoading } from "../actions/loading";
import { fecthSetComments } from "../apis/criticsAndSuggestions";

/*--------------------------- cancel orders ---------------------- */
function* setComments({ time, tok, user_id, title, text }) {
  yield put(showLoading());

  const result = yield fecthSetComments(time, tok, user_id, title, text);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(showToastify(" پیشنهاد شما با موفقیت  ثبت شد.", "success"));
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*------------------------------------------------------------- */

function* actionCriticsAndSuggestionsWatcher() {
  yield takeEvery(criticsAndSuggestions.REQUEST_SET_COMMENTS, setComments);
}

export function* criticsAndSuggestionWatcher() {
  yield actionCriticsAndSuggestionsWatcher();
}
