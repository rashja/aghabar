import { put, takeEvery } from "redux-saga/effects";
import * as newRequests from "../actions/newRequests";
import { showToastify, showLoading, hideLoading } from "../actions/loading";
import { fecthProvinceCoordinates } from "../apis/newRequests";

/*--------------------------- cancel orders ---------------------- */
function* getCityCoordinates({ province, city }) {
  yield put(showLoading());

  const result = yield fecthProvinceCoordinates(province);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(newRequests.getCityCoordinatePayload(result.data.value, city));
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*------------------------------------------------------------- */

function* actionNewRequestsWatcher() {
  yield takeEvery(newRequests.GET_CITY_COORDINATE, getCityCoordinates);
}

export function* newRequestsWatcher() {
  yield actionNewRequestsWatcher();
}
