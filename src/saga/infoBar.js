import { put, takeEvery } from "redux-saga/effects";
import * as infoBar from "../actions/infoBar";
import { showToastify, showLoading, hideLoading } from "../actions/loading";
import { fecthBarClass, fecthNewRequest } from "../apis/infoBar";

/*--------------------------- get class bar ---------------------- */
function* getClassBar({ tok, time, userId, classBarId }) {
  yield put(showLoading());

  const result = yield fecthBarClass(tok, time, userId, classBarId);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(infoBar.payloadRequestClassBar(result.data));
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*--------------------------- get class bar ---------------------- */
function* sendNewRequest({
  time,
  tok,
  user_id,
  title_bar,
  class_bar,
  loading_mode,
  des_insurance,
  ispackaged,
  dimensions,
  date_sender,
  clock_sender,
  trucks_id,
  type_pay,
  weight,
  addresses,
  imageFiles,
}) {
  yield put(showLoading());

  const result = yield fecthNewRequest(
    time,
    tok,
    user_id,
    title_bar,
    class_bar,
    loading_mode,
    des_insurance,
    ispackaged,
    dimensions,
    date_sender,
    clock_sender,
    trucks_id,
    type_pay,
    weight,
    addresses,
    imageFiles
  );

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(infoBar.newRequestSuccess());
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*----------------------------------------------------------------- */
function* actionInfoBarWatcher() {
  yield takeEvery(infoBar.REQUEST_CLASS_BAR, getClassBar);
  yield takeEvery(infoBar.SEND_NEW_REQUEST, sendNewRequest);
}

export function* infoBarWatcher() {
  yield actionInfoBarWatcher();
}
