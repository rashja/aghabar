import { put, takeEvery } from "redux-saga/effects";
import * as loginActions from "../actions/login";
import * as ordersListActions from "../actions/ordersList";
import { showToastify, showLoading, hideLoading } from "../actions/loading";
import {
  fecthMobileNumber,
  fetchSignUp,
  fecthBaseInfo,
  fecthLogin,
  fecthRules,
} from "../apis/login";

/*--------------------------- send mobile ---------------------- */
function* sendMobile({ mobileNumber, time, token }) {
  yield put(showLoading());
  const result = yield fecthMobileNumber(mobileNumber, time, token);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(loginActions.getLogId(result.data.log_id));
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*----------------------------- sign up ------------------------ */
function* signUp({
  time,
  tok,
  type_user,
  fname,
  lname,
  mobile,
  reagent_code,
  national_code,
  name_company,
  phone_company,
  area_code,
}) {
  yield put(showLoading());
  const result = yield fetchSignUp(
    time,
    tok,
    type_user,
    fname,
    lname,
    mobile,
    reagent_code,
    national_code,
    name_company,
    phone_company,
    area_code
  );

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(loginActions.registerSuccess());
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*----------------------------- pre code ------------------------ */
function* preCode({ time, tok }) {
  yield put(showLoading());

  const result = yield fecthBaseInfo(time, tok);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(ordersListActions.baseInfo(result.data));
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*----------------------------- otp code ------------------------ */
function* otpCode({ mobile, time, log_id, code_user, tok }) {
  yield put(showLoading());

  const result = yield fecthLogin(mobile, time, log_id, code_user, tok);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(loginActions.checkLoginInfo(result.data));
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(loginActions.errorOtpInvalid());
  }
}
/*----------------------------- get rules ----------------------- */
function* getRules({ time, token }) {
  const result = yield fecthRules(time, token);

  if (result.success && result.data.status !== "error") {
    yield put(loginActions.payloadGetRules(result.data));
  }

  if (result.data.status === "error") {
    yield put(loginActions.errorOtpInvalid());
  }
}
/*-------------------------------------------------------------- */
function* actionLoginWatcher() {
  yield takeEvery(loginActions.SEND_MOBILE, sendMobile);
  yield takeEvery(loginActions.SIGN_UP, signUp);
  yield takeEvery(loginActions.PRE_CODE, preCode);
  yield takeEvery(loginActions.OTP_CODE, otpCode);
  yield takeEvery(loginActions.GET_RULES, getRules);
}

export function* loginWatcher() {
  yield actionLoginWatcher();
}
