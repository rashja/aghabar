import { put, takeEvery } from "redux-saga/effects";
import * as ordersListActions from "../actions/ordersList";
import { showToastify, showLoading, hideLoading } from "../actions/loading";
import {
  fecthSuccessOrders,
  fecthCurrentOrders,
  fecthResendRequest,
  fecthPursuitRequest,
  fecthCancelOrders,
  fecthSetRate,
} from "../apis/ordersList";

/*--------------------------- success orders ---------------------- */
function* successOrders({ user_id, time, tok, page_num }) {
  yield put(showLoading());

  const result = yield fecthSuccessOrders(user_id, time, tok, page_num);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(ordersListActions.payloadSuccessOrders(result.data, page_num));
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*--------------------------- current orders ---------------------- */
function* currentOrders({ user_id, time, tok, page_num }) {
  yield put(showLoading());

  const result = yield fecthCurrentOrders(user_id, time, tok, page_num);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(ordersListActions.payloadCurrentOrders(result.data, page_num));
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*--------------------------- cancel orders ---------------------- */
function* cancelOrders({ user_id, time, tok, page_num }) {
  yield put(showLoading());

  const result = yield fecthCancelOrders(user_id, time, tok, page_num);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(ordersListActions.payloadCancelOrders(result.data, page_num));
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*--------------------------- resend request -------------------- */
function* resendRequest({ token, time, userId, trId }) {
  yield put(showLoading());

  const result = yield fecthResendRequest(token, time, userId, trId);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(ordersListActions.successResendRequest());
    yield put(ordersListActions.resendPayload(result.data));
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*--------------------------- pursuit request -------------------- */
function* pursuitRequest({ time, tok, user_id, tr_id }) {
  yield put(showLoading());

  const result = yield fecthPursuitRequest(time, tok, user_id, tr_id);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(ordersListActions.payloadPursuitRequest(result.data));
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*----------------------------- set rate ----------------------- */
function* setRate({ token, time, userId, rateDriver, trId, complaintsId }) {
  yield put(showLoading());

  const result = yield fecthSetRate(
    token,
    time,
    userId,
    rateDriver,
    trId,
    complaintsId
  );

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(ordersListActions.successSetRate());
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*----------------------------------------------------------------- */

function* actionOrdersListWatcher() {
  yield takeEvery(ordersListActions.REQUEST_SUCCESS_ORDERS, successOrders);
  yield takeEvery(ordersListActions.REQUEST_CURRENT_ORDERS, currentOrders);
  yield takeEvery(ordersListActions.REQUEST_CANCEL_ORDERS, cancelOrders);
  yield takeEvery(ordersListActions.SEND_PURSUIT_REQUEST, pursuitRequest);
  yield takeEvery(ordersListActions.RESEND_REQUEST, resendRequest);
  yield takeEvery(ordersListActions.REQUEST_SET_RATE, setRate);
}

export function* ordersListWatcher() {
  yield actionOrdersListWatcher();
}
