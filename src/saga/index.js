import { all } from "redux-saga/effects";
import { loginWatcher } from "./login";
import { infoBarWatcher } from "./infoBar";
import { profileWatcher } from "./profile";
import { ordersListWatcher } from "./ordersList";
import { newRequestsWatcher } from "./newRequests";
import { announcementsWatcher } from "./announcements";
import { pursuitRequestWatcher } from "./pursuitRequest";
import { criticsAndSuggestionWatcher } from "./criticsAndSuggestions";
import { financialManagementWatcher } from "./financialManagement";
import { externalTransactionsWatcher } from "./externalTransactions";

export default function* rootSaga() {
  yield all([
    loginWatcher(),
    profileWatcher(),
    infoBarWatcher(),
    ordersListWatcher(),
    newRequestsWatcher(),
    announcementsWatcher(),
    pursuitRequestWatcher(),
    financialManagementWatcher(),
    criticsAndSuggestionWatcher(),
    externalTransactionsWatcher(),
  ]);
}
