import { put, takeEvery } from "redux-saga/effects";
import * as profile from "../actions/profile";
import { showToastify, showLoading, hideLoading } from "../actions/loading";
import { fecthSetProfile, fecthGetProfile } from "../apis/profile";

/*--------------------------- set profile ---------------------- */
function* setProfile({
  time,
  tok,
  userId,
  firstName,
  lastName,
  profilePicture,
}) {
  yield put(showLoading());

  const result = yield fecthSetProfile(
    time,
    tok,
    userId,
    firstName,
    lastName,
    profilePicture
  );

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(profile.ProfileSuccess());
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*--------------------------- get profile ---------------------- */
function* getProfile({ time, token, userId }) {
  yield put(showLoading());

  const result = yield fecthGetProfile(time, token, userId);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(profile.payloadGetProfile(result.data.user));
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*------------------------------------------------------------- */

function* actionProfileWatcher() {
  yield takeEvery(profile.REQUEST_SET_PROFILE, setProfile);
  yield takeEvery(profile.REQUEST_GET_PROFILE, getProfile);
}

export function* profileWatcher() {
  yield actionProfileWatcher();
}
