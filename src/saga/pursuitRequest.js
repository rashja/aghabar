import { put, takeEvery } from "redux-saga/effects";
import * as pursuitRequest from "../actions/pursuitRequest";
import { showToastify, showLoading, hideLoading } from "../actions/loading";
import { fecthPickedDriver } from "../apis/pursuitRequest";

/*--------------------------- pick driver ---------------------- */
function* getPickedDriver({ time, tok, userId, trId, driverId }) {
  yield put(showLoading());
  const result = yield fecthPickedDriver(time, tok, userId, trId, driverId);

  if (result.success && result.data.status !== "error") {
    yield put(hideLoading());
    yield put(pursuitRequest.pickedDriverSuccess());
  }

  if (result.data.status === "error") {
    yield put(hideLoading());
    yield put(showToastify(result.data.message, "error"));
  }
}
/*------------------------------------------------------------- */

function* actionPursuitRequestWatcher() {
  yield takeEvery(pursuitRequest.REQUEST_PICKED_DRIVER, getPickedDriver);
}

export function* pursuitRequestWatcher() {
  yield actionPursuitRequestWatcher();
}
