import { BASE_URL } from "./constants";
import ApiService from "./apiServiceManager";
import axios from "axios";

const apiService = new ApiService();
const formData = new FormData();

/*------------------------------ announcments ----------------------- */
export async function fecthAnnouncements(time, tok, user_id, pageNum) {
  try {
    apiService.setMethod("POST");
    formData.append("time", `${time}`);
    formData.append("tok", `${tok}`);
    formData.append("user_id", `${user_id}`);
    formData.append("page_num", `${pageNum}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI36`, formData)
    );
    formData.delete("time");
    formData.delete("tok");
    formData.delete("user_id");
    formData.delete("page_num");

    if (response.status !== 200) {
      return {
        success: false,
        data: null
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error
    };
  }
}
