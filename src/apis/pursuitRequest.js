import { BASE_URL } from "./constants";
import ApiService from "./apiServiceManager";
import axios from "axios";

const apiService = new ApiService();
const formData = new FormData();

/*------------------------------ picked driver ----------------------- */
export async function fecthPickedDriver(time, tok, user_id, tr_id, driver_id) {
  try {
    apiService.setMethod("POST");
    formData.append("time", `${time}`);
    formData.append("tok", `${tok}`);
    formData.append("user_id", `${user_id}`);
    formData.append("tr_id", `${tr_id}`);
    formData.append("driver_id", `${driver_id}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI18`, formData)
    );
    formData.delete("time");
    formData.delete("tok");
    formData.delete("user_id");
    formData.delete("tr_id");
    formData.delete("driver_id");

    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
