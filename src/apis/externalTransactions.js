import { BASE_URL } from "./constants";
import ApiService from "./apiServiceManager";
import axios from "axios";

const apiService = new ApiService();
const formData = new FormData();

/*------------------------------ send suggestions ----------------------- */
export async function fetchExternalTransactions(time, tok, user_id, page_num) {
  try {
    apiService.setMethod("POST");
    formData.append("time", `${time}`);
    formData.append("tok", `${tok}`);
    formData.append("user_id", `${user_id}`);
    formData.append("page_num", `${page_num}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI26`, formData)
    );
    formData.delete("time");
    formData.delete("tok");
    formData.delete("user_id");
    formData.delete("page_num");

    if (response.status !== 200) {
      return {
        success: false,
        data: null
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error
    };
  }
}
/*------------------------------ finish work ------------------------ */
export async function fetchFinishWork(tok, time, user_id, tr_id) {
  try {
    apiService.setMethod("POST");
    formData.append("tok", `${tok}`);
    formData.append("time", `${time}`);
    formData.append("user_id", `${user_id}`);
    formData.append("tr_id", `${tr_id}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI27`, formData)
    );
    formData.delete("tok");
    formData.delete("time");
    formData.delete("user_id");
    formData.delete("tr_id");

    if (response.status !== 200) {
      return {
        success: false,
        data: null
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error
    };
  }
}
