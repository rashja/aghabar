import { BASE_URL } from "./constants";
import ApiService from "./apiServiceManager";
import axios from "axios";

const apiService = new ApiService();
const formData = new FormData();

/*------------------------------ send suggestions ----------------------- */
export async function fecthSetComments(time, tok, user_id, title, text) {
  try {
    apiService.setMethod("POST");
    formData.append("time", `${time}`);
    formData.append("tok", `${tok}`);
    formData.append("user_id", `${user_id}`);
    formData.append("title", `${title}`);
    formData.append("text", `${text}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI35`, formData)
    );
    formData.delete("time");
    formData.delete("tok");
    formData.delete("user_id");
    formData.delete("title");
    formData.delete("text");

    if (response.status !== 200) {
      return {
        success: false,
        data: null
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error
    };
  }
}
