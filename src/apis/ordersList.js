import { BASE_URL } from "./constants";
import ApiService from "./apiServiceManager";
import axios from "axios";

const apiService = new ApiService();
const formData = new FormData();

/*------------------------------ success orders ----------------------- */
export async function fecthSuccessOrders(user_id, time, tok, page_num) {
  try {
    apiService.setMethod("POST");
    formData.append("user_id", `${user_id}`);
    formData.append("time", `${time}`);
    formData.append("tok", `${tok}`);
    formData.append("page_num", `${page_num}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI32`, formData)
    );
    formData.delete("user_id");
    formData.delete("time");
    formData.delete("tok");
    formData.delete("page_num");

    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
/*------------------------------ current orders ----------------------- */
export async function fecthCurrentOrders(user_id, time, tok, page_num) {
  try {
    apiService.setMethod("POST");
    formData.append("user_id", `${user_id}`);
    formData.append("time", `${time}`);
    formData.append("tok", `${tok}`);
    formData.append("page_num", `${page_num}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI15`, formData)
    );
    formData.delete("user_id");
    formData.delete("time");
    formData.delete("tok");
    formData.delete("page_num");

    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
/*------------------------------- cancel orders ----------------------- */
export async function fecthCancelOrders(user_id, time, tok, page_num) {
  formData.delete("user_id");
  formData.delete("time");
  formData.delete("tok");
  formData.delete("page_num");
  try {
    apiService.setMethod("POST");
    formData.append("user_id", `${user_id}`);
    formData.append("time", `${time}`);
    formData.append("tok", `${tok}`);
    formData.append("page_num", `${page_num}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI33`, formData)
    );
    formData.delete("user_id");
    formData.delete("time");
    formData.delete("tok");
    formData.delete("page_num");

    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
/*------------------------------- new request ----------------------- */
export async function fecthResendRequest(tok, time, user_id, tr_id) {
  try {
    apiService.setMethod("POST");
    formData.append("user_id", `${user_id}`);
    formData.append("time", `${time}`);
    formData.append("tok", `${tok}`);
    formData.append("tr_id", `${tr_id}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI21`, formData)
    );
    formData.delete("user_id");
    formData.delete("time");
    formData.delete("tok");
    formData.delete("tr_id");

    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
/*------------------------------- pursuit request ----------------------- */
export async function fecthPursuitRequest(time, tok, user_id, tr_id) {
  try {
    apiService.setMethod("POST");
    formData.append("user_id", `${user_id}`);
    formData.append("time", `${time}`);
    formData.append("tok", `${tok}`);
    formData.append("tr_id", `${tr_id}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI16`, formData)
    );
    formData.delete("user_id");
    formData.delete("time");
    formData.delete("tok");
    formData.delete("tr_id");

    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
/*------------------------------- set rate ----------------------- */
export async function fecthSetRate(tok, time, user_id, rate, tr_id, options) {
  try {
    apiService.setMethod("POST");
    formData.append("user_id", `${user_id}`);
    formData.append("time", `${time}`);
    formData.append("tok", `${tok}`);
    formData.append("tr_id", `${tr_id}`);
    formData.append("rate", `${rate}`);
    formData.append("options", `${options}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI25`, formData)
    );
    formData.delete("user_id");
    formData.delete("time");
    formData.delete("tok");
    formData.delete("tr_id");
    formData.delete("rate");
    formData.delete("options");

    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
