const formData = new FormData();

class ApiService {
  _method = "GET";
  _headers = {
    Accept: "application/json",
    "Content-Type": "application/json"
  };

  getMethod() {
    return this._method;
  }
  setMethod(newMethod) {
    this._method = newMethod;
  }
  getHeaders() {
    return this._headers;
  }
  setHeaders(newHeaders) {
    this._headers = newHeaders;
  }

  getRequest(reqUrl, reBody) {
    if (reBody) {
      return {
        url: reqUrl,
        method: this._method,
        headers: this._headers,
        data: reBody
      };
    }
    if (reBody === undefined) {
      return {
        url: reqUrl,
        method: this._method,
        headers: this._headers,
        data: null
      };
    }
  }
}
export default ApiService;
