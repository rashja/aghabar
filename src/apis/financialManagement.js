import { BASE_URL } from "./constants";
import ApiService from "./apiServiceManager";
import axios from "axios";

const apiService = new ApiService();
const formData = new FormData();

/*------------------------------ payment list ----------------------- */
export async function fecthPaymentList(tok, time, user_id) {
  try {
    apiService.setMethod("POST");
    formData.append("tok", `${tok}`);
    formData.append("time", `${time}`);
    formData.append("user_id", `${user_id}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI34`, formData)
    );
    formData.delete("tok");
    formData.delete("time");
    formData.delete("user_id");

    if (response.status !== 200) {
      return {
        success: false,
        data: null
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error
    };
  }
}
