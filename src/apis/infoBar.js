import { BASE_URL } from "./constants";
import ApiService from "./apiServiceManager";
import axios from "axios";

const apiService = new ApiService();
const formData = new FormData();

/*------------------------------ payment list ----------------------- */
export async function fecthBarClass(tok, time, user_id, bar_class_id) {
  try {
    apiService.setMethod("POST");
    formData.append("tok", `${tok}`);
    formData.append("time", `${time}`);
    formData.append("user_id", `${user_id}`);
    formData.append("bar_class_id", `${bar_class_id}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI7`, formData)
    );
    formData.delete("tok");
    formData.delete("time");
    formData.delete("user_id");
    formData.delete("bar_class_id");

    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
/*------------------------------ new request ------------------------- */
export async function fecthNewRequest(
  time,
  tok,
  user_id,
  title_bar,
  class_bar,
  loading_mode,
  des_insurance,
  ispackaged,
  dimensions,
  date_sender,
  clock_sender,
  trucks_id,
  type_pay,
  weight,
  addresses,
  imageFiles
) {
  try {
    apiService.setMethod("POST");
    formData.append("tok", `${tok}`);
    formData.append("time", `${time}`);
    formData.append("user_id", `${user_id}`);
    formData.append("title_bar", `${title_bar}`);
    formData.append("class_bar", `${class_bar}`);
    formData.append("loading_mode", `${loading_mode}`);
    formData.append("des_insurance", `${des_insurance}`);
    formData.append("ispackaged", `${ispackaged}`);
    formData.append("dimensions", `${dimensions}`);
    formData.append("date_sender", `${date_sender}`);
    formData.append("clock_sender", `${clock_sender}`);
    formData.append("trucks_id", `${trucks_id}`);
    formData.append("type_pay", `${type_pay}`);
    formData.append("weight", `${weight}`);
    formData.append("addresses", `${JSON.stringify(addresses)}`);
    imageFiles[0]
      ? formData.append("img_one", imageFiles[0], imageFiles[0].name)
      : formData.append("img_one", "");
    imageFiles[1]
      ? formData.append("img_two", imageFiles[1], imageFiles[1].name)
      : formData.append("img_two", "");
    imageFiles[2]
      ? formData.append("img_three", imageFiles[2], imageFiles[2].name)
      : formData.append("img_three", "");

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI14`, formData)
    );

    formData.delete("tok");
    formData.delete("time");
    formData.delete("user_id");
    formData.delete("title_bar");
    formData.delete("class_bar");
    formData.delete("loading_mode");
    formData.delete("des_insurance");
    formData.delete("ispackaged");
    formData.delete("dimensions");
    formData.delete("date_sender");
    formData.delete("clock_sender");
    formData.delete("trucks_id");
    formData.delete("type_pay");
    formData.delete("weight");
    formData.delete("addresses");
    formData.delete("img_one");
    formData.delete("img_two");
    formData.delete("img_three");

    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
