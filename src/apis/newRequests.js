import { mapIrUrl, mapIrToken } from "./constants";
import ApiService from "./apiServiceManager";
import axios from "axios";

const apiService = new ApiService();

export async function fecthProvinceCoordinates(province) {
  try {
    apiService.setMethod("POST");
    apiService.setHeaders({
      "Content-Type": "application/json",
      "x-api-key": mapIrToken,
    });
    let body = JSON.stringify({
      text: `${province}`,
      $select: "city",
    });
    const response = await axios(apiService.getRequest(mapIrUrl, body));

    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
