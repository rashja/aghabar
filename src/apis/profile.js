import { BASE_URL } from "./constants";
import ApiService from "./apiServiceManager";
import axios from "axios";

const apiService = new ApiService();
const formData = new FormData();

/*------------------------------ set profile ----------------------- */
export async function fecthSetProfile(
  time,
  tok,
  user_id,
  fname,
  lname,
  picture
) {
  try {
    apiService.setMethod("POST");
    formData.append("time", `${time}`);
    formData.append("tok", `${tok}`);
    formData.append("user_id", `${user_id}`);
    formData.append("fname", `${fname}`);
    formData.append("lname", `${lname}`);
    picture
      ? formData.append("picture", picture, picture.name)
      : formData.append("picture", "");

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI6`, formData)
    );
    formData.delete("time");
    formData.delete("tok");
    formData.delete("user_id");
    formData.delete("fname");
    formData.delete("lname");
    formData.delete("picture");

    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
/*------------------------------ get profile ----------------------- */
export async function fecthGetProfile(time, tok, user_id) {
  try {
    apiService.setMethod("POST");
    formData.append("time", `${time}`);
    formData.append("tok", `${tok}`);
    formData.append("user_id", `${user_id}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI5`, formData)
    );
    formData.delete("time");
    formData.delete("tok");
    formData.delete("user_id");

    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
