import { BASE_URL } from "./constants";
import ApiService from "./apiServiceManager";
import axios from "axios";

const apiService = new ApiService();
const formData = new FormData();

/*-------------------------------- sign up --------------------------- */
export async function fetchSignUp(
  time,
  tok,
  type_user,
  fname,
  lname,
  mobile,
  reagent_code,
  national_code,
  name_company,
  phone_company,
  area_code
) {
  try {
    apiService.setMethod("POST");
    formData.append("time", `${time}`);
    formData.append("tok", `${tok}`);
    formData.append("type_user", `${type_user}`);
    formData.append("fname", `${fname}`);
    formData.append("lname", `${lname}`);
    formData.append("mobile", `${mobile}`);
    formData.append("reagent_code", `${reagent_code}`);
    formData.append("national_code", `${national_code}`);
    formData.append("name_company", `${name_company}`);
    formData.append("phone_company", `${phone_company}`);
    formData.append("area_code", `${area_code}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI1`, formData)
    );
    formData.delete("time");
    formData.delete("tok");
    formData.delete("type_user");
    formData.delete("fname");
    formData.delete("lname");
    formData.delete("mobile");
    formData.delete("reagent_code");
    formData.delete("national_code");
    formData.delete("name_company");
    formData.delete("phone_company");
    formData.delete("area_code");

    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
/*------------------------------ mobile number ----------------------- */
export async function fecthMobileNumber(mobile, time, tok) {
  try {
    apiService.setMethod("POST");
    formData.append("mobile", `${mobile}`);
    formData.append("time", `${time}`);
    formData.append("tok", `${tok}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI3`, formData)
    );
    formData.delete("mobile");
    formData.delete("time");
    formData.delete("tok");
    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
/*-------------------------------- pre code -------------------------- */
export async function fecthBaseInfo(time, tok) {
  try {
    apiService.setMethod("POST");
    formData.append("time", `${time}`);
    formData.append("tok", `${tok}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI8`, formData)
    );

    formData.delete("time");
    formData.delete("tok");

    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
/*-------------------------------- sign in -------------------------- */
export async function fecthLogin(mobile, time, log_id, code_user, tok) {
  try {
    apiService.setMethod("POST");
    formData.append("mobile", `${mobile}`);
    formData.append("time", `${time}`);
    formData.append("log_id", `${log_id}`);
    formData.append("code_user", `${code_user}`);
    formData.append("tok", `${tok}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI4`, formData)
    );
    formData.delete("mobile");
    formData.delete("time");
    formData.delete("log_id");
    formData.delete("code_user");
    formData.delete("tok");

    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
/*-------------------------------- fetch rules -------------------------- */
export async function fecthRules(time, tok) {
  try {
    apiService.setMethod("POST");
    formData.append("time", `${time}`);
    formData.append("tok", `${tok}`);

    const response = await axios(
      apiService.getRequest(`${BASE_URL}UAPI12`, formData)
    );

    formData.delete("time");
    formData.delete("tok");

    if (response.status !== 200) {
      return {
        success: false,
        data: null,
      };
    }

    if (response.status === 200) {
      return {
        success: true,
        data: response.data,
      };
    }
  } catch (error) {
    return {
      success: false,
      data: null,
      error,
    };
  }
}
