/*---------------------------------- get coordinate ------------------------------ */
export const GET_COORDINATES = "newRequestsActions.GET_COORDINATE";

export const getCoordinate = (payload) => ({
  type: GET_COORDINATES,
  payload,
});

/*----------------------------- delete location from map ------------------------- */
export const DELETE_LOCATION_FROM_MAP =
  "newRequestsActions.DELETE_LOCATION_FROM_MAP";

export const deleteLocationFromMap = (payload) => ({
  type: DELETE_LOCATION_FROM_MAP,
  payload,
});
/*-------------------------- get city and province coordinate -------------------- */
export const GET_CITY_COORDINATE = "newRequestsActions.GET_CITY_COORDINATE";
export const GET_CITY_COORDINATE_PAYLOAD =
  "newRequestsActions.GET_CITY_COORDINATE_PAYLOAD";

export const getCityCoordinate = (province, city) => ({
  type: GET_CITY_COORDINATE,
  province,
  city,
});

export const getCityCoordinatePayload = (payload, city) => ({
  type: GET_CITY_COORDINATE_PAYLOAD,
  payload: payload.filter((p) => p.city === city.trim()),
});
/*--------------------- send multiple coordinates to next step ------------------- */
export const SEND_MULTIPLE_COORDINATES =
  "newRequestsActions.SEND_MULTIPLE_COORDINATES";

export const sendMultipleCoordinates = (multCoordinates) => ({
  type: SEND_MULTIPLE_COORDINATES,
  payload: multCoordinates,
});
/*-------------------------- send extra infoes to next step ----------------------- */
export const SEND_EXTRA_INFORMATIONS_ADDRESSES =
  "newRequestsActions.SEND_EXTRA_INFORMATIONS_ADDRESSES";

export const sendExtraAddressesInfo = (info) => ({
  type: SEND_EXTRA_INFORMATIONS_ADDRESSES,
  payload: info,
});
/*---------------------------------- set area code -------------------------------- */
export const SET_AREA_CODE = "newRequestsActions.SET_AREA_CODE";

export const setAreaCoordinate = (payload) => ({
  type: SET_AREA_CODE,
  payload,
});
/*------------------------------ set multiple refresh ----------------------------- */
export const SET_MULTIPLE_REFRESH = "newRequestsActions.SET_MULTIPLE_REFRESH";

export const setMultipleCoordinatesForRefresh = (payload) => ({
  type: SET_MULTIPLE_REFRESH,
  payload,
});
