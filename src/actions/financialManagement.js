/*-------------------------- payment list -------------------- */
export const REQUEST_PAYMRNT_LIST =
  "FinancialManagementActions.REQUEST_PAYMRNT_LIST";
export const PAYLOAD_PAYMRNT_LIST =
  "FinancialManagementActions.PAYLOAD_PAYMRNT_LIST";

export const requestPaymentList = (tok, time, user_id) => ({
  type: REQUEST_PAYMRNT_LIST,
  tok,
  time,
  user_id
});

export const payloadPaymentList = payload => ({
  type: PAYLOAD_PAYMRNT_LIST,
  payload
});
