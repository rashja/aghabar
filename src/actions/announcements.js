/*-------------------------- announcment -------------------- */
export const REQUEST_ANNOUNCEMENT = "AnnouncementsActions.REQUEST_ANNOUNCEMENT";
export const PAYLOAD_EXTERNAL_TRANSACTIONS =
  "AnnouncementsActions.PAYLOAD_ANNOUNCEMENT";

export const requestAnnouncements = (time, tok, userId, pageNum) => ({
  type: REQUEST_ANNOUNCEMENT,
  time,
  tok,
  userId,
  pageNum
});

export const payloadAnnouncements = payload => ({
  type: PAYLOAD_EXTERNAL_TRANSACTIONS,
  payload
});
