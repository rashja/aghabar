/*------------------------- Base infoes ------------------------ */
export const BASE_INFO = "OrdersListActions.BASE_INFO";

export const baseInfo = (info) => {
  return {
    type: BASE_INFO,
    payload: info,
  };
};
/*--------------------------- success orders -------------------- */
export const REQUEST_SUCCESS_ORDERS =
  "OrdersListActions.REQUEST_SUCCESS_ORDERS";
export const PAYLOAD_SUCCESS_ORDERS =
  "OrdersListActions.PAYLOAD_SUCCESS_ORDERS";

export const requestSuccessOrders = (user_id, time, tok, page_num) => ({
  type: REQUEST_SUCCESS_ORDERS,
  user_id,
  time,
  tok,
  page_num,
});

export const payloadSuccessOrders = (payload, page) => ({
  type: PAYLOAD_SUCCESS_ORDERS,
  payload,
  page,
});
/*-------------------------- current orders -------------------- */
export const REQUEST_CURRENT_ORDERS =
  "OrdersListActions.REQUEST_CURRENT_ORDERS";
export const PAYLOAD_CURRENT_ORDERS =
  "OrdersListActions.PAYLOAD_CURRENT_ORDERS";

export const requestCurrentOrders = (user_id, time, tok, page_num) => ({
  type: REQUEST_CURRENT_ORDERS,
  tok,
  time,
  user_id,
  page_num,
});

export const payloadCurrentOrders = (payload, page) => ({
  type: PAYLOAD_CURRENT_ORDERS,
  payload,
  page,
});
/*-------------------------- cancel orders -------------------- */
export const REQUEST_CANCEL_ORDERS = "OrdersListActions.REQUEST_CANCEL_ORDERS";
export const PAYLOAD_CANCEL_ORDERS = "OrdersListActions.PAYLOAD_CANCEL_ORDERS";

export const requestCancelOrders = (user_id, time, tok, page_num) => ({
  type: REQUEST_CANCEL_ORDERS,
  tok,
  time,
  user_id,
  page_num,
});

export const payloadCancelOrders = (payload, page) => ({
  type: PAYLOAD_CANCEL_ORDERS,
  payload,
  page,
});

/*-------------------------- pursuit request -------------------- */
export const SEND_PURSUIT_REQUEST = "OrdersListActions.SEND_PURSUIT_REQUEST";
export const PAYLOAD_SEND_PURSUIT_REQUEST =
  "OrdersListActions.PAYLOAD_SEND_PURSUIT_REQUEST";
export const RESET_PURSUIT_MODE = "OrdersListActions.RESET_PURSUIT_MODE";

export const sendPursuitRequest = (time, tok, user_id, tr_id) => ({
  type: SEND_PURSUIT_REQUEST,
  tok,
  time,
  user_id,
  tr_id,
});

export const payloadPursuitRequest = (payload) => ({
  type: PAYLOAD_SEND_PURSUIT_REQUEST,
  payload,
});

export const resetPursuitRequestMode = () => ({
  type: RESET_PURSUIT_MODE,
});
/*-------------------------- resend requests -------------------- */
export const RESEND_REQUEST = "OrdersListActions.RESEND_REQUEST";
export const SUCCESS_RESEND_REQUEST =
  "OrdersListActions.SUCCESS_RESEND_REQUEST";
export const RESET_RESEND_REQUEST = "OrdersListActions.RESET_RESEND_REQUEST";
export const RESEND_REQUEST_PAYLOAD =
  "OrdersListActions.RESEND_REQUEST_PAYLOAD";
export const CLEAR_RESEND_REQUESTS_DATA =
  "OrdersListActions.CLEAR_RESEND_REQUESTS_DATA";

export const resendRequest = (token, time, userId, trId) => ({
  type: RESEND_REQUEST,
  token,
  time,
  userId,
  trId,
});

export const resendPayload = (data) => ({
  type: RESEND_REQUEST_PAYLOAD,
  payload: data.resend_tr,
});

export const successResendRequest = () => ({
  type: SUCCESS_RESEND_REQUEST,
});

export const resetResendRequest = () => ({
  type: RESET_RESEND_REQUEST,
});

export const clearResendRequestsData = () => ({
  type: CLEAR_RESEND_REQUESTS_DATA,
});
/*------------------------- handle menu ------------------------ */
export const OPEN_MENU = "OPEN_MENU";
export const CLOSE_MENU = "CLOSE_MENU";

export const openMenu = () => ({
  type: OPEN_MENU,
});

export const closeMenu = () => ({
  type: CLOSE_MENU,
});
/*---------------------------- set mode ------------------------ */
export const SET_TAB = "OrdersListActions.SET_TAB";

export const setTab = (mode) => ({
  type: SET_TAB,
  payload: mode,
});
/*-------------------------- set rate driver-------------------- */
export const REQUEST_SET_RATE = "OrdersListActions.REQUEST_SET_RATE";
export const SUCCESS_SET_RATE = "OrdersListActions.SUCCESS_SET_RATE";
export const RESET_SET_RATE = "OrdersListActions.RESET_SET_RATE";

export const setRate = (
  token,
  time,
  userId,
  rateDriver,
  trId,
  complaintsId
) => ({
  type: REQUEST_SET_RATE,
  token,
  time,
  userId,
  rateDriver,
  trId,
  complaintsId,
});

export const successSetRate = () => ({
  type: SUCCESS_SET_RATE,
});

export const resetSetRate = () => ({
  type: RESET_SET_RATE,
});
/*-------------------------------------------------------------- */
