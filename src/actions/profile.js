/*-------------------------- profile -------------------- */
export const REQUEST_SET_PROFILE = "Profile.REQUEST_SET_PROFILE";
export const PROFILE_SUCCESS = "Profile.PROFILE_SUCCESS";
export const REQUEST_GET_PROFILE = "Profile.REQUEST_GET_PROFILE";
export const PAYLOAD_GET_PROFILE = "Profile.GET_PROFILE";
export const CLEAR_PROFILE_MODE = "Profile.CLEAR_PROFILE_MODE";
export const CLEAR_GET_PROFILE_MODE = "Profile.CLEAR_GET_PROFILE_MODE";

export const requestSetProfile = (
  time,
  tok,
  userId,
  firstName,
  lastName,
  profilePicture
) => ({
  type: REQUEST_SET_PROFILE,
  time,
  tok,
  userId,
  firstName,
  lastName,
  profilePicture
});

export const requestGetProfile = (time, token, userId) => ({
  type: REQUEST_GET_PROFILE,
  time,
  token,
  userId
});

export const payloadGetProfile = payload => ({
  type: PAYLOAD_GET_PROFILE,
  payload
});

export const ClearProfileMode = () => ({
  type: CLEAR_PROFILE_MODE
});

export const ProfileSuccess = () => ({
  type: PROFILE_SUCCESS
});

export const ClearGetProfileMode = () => ({
  type: CLEAR_GET_PROFILE_MODE
});
