/*------------------------- send mobile -------------------- */
export const SEND_MOBILE = "LoginActions.SEND_MOBILE";

export const sendMobile = (mobileNumber, time, token) => ({
  type: SEND_MOBILE,
  mobileNumber,
  time,
  token,
});
/*------------------------- sign up ------------------------ */
export const SIGN_UP = "LoginActions.SIGN_UP";

export const signUp = (
  time,
  tok,
  type_user,
  fname,
  lname,
  mobile,
  reagent_code,
  national_code,
  name_company,
  phone_company,
  area_code
) => ({
  type: SIGN_UP,
  time,
  tok,
  type_user,
  fname,
  lname,
  mobile,
  reagent_code,
  national_code,
  name_company,
  phone_company,
  area_code,
});
/*------------------------- pre code ---------------------- */
export const PRE_CODE = "LoginActions.PRE_CODE";

export const getPreCode = (time, tok) => ({
  type: PRE_CODE,
  time,
  tok,
});
/*--------------------- register success ------------------ */
export const REGISTER_SUCCESS = "LoginActions.REGISTER_SUCCESS";

export const registerSuccess = () => ({
  type: REGISTER_SUCCESS,
});
/*------------------------ get log id --------------------- */
export const LOG_ID = "LoginActions.LOG_ID";

export const getLogId = (logId) => ({
  type: LOG_ID,
  payload: logId,
});
/*----------------------- send otp code ------------------- */
export const OTP_CODE = "LoginActions.OTP_CODE";

export const sendOtpCode = (mobile, time, log_id, code_user, tok) => ({
  type: OTP_CODE,
  mobile,
  time,
  log_id,
  code_user,
  tok,
});
/*----------------------- check login --------------------- */
export const CHECK_LOGIN = "LoginActions.CHECK_LOGIN";

export const checkLoginInfo = (info) => ({
  type: CHECK_LOGIN,
  payload: info,
});
/*------------------ reset send mobile mode --------------- */
export const RESET_MOBILE_MODE = "LoginActions.RESET_MOBILE_MODE";

export const resetsendMobileMode = () => ({
  type: RESET_MOBILE_MODE,
});
/*------------------------ invalid Otp -------------------- */
export const OTP_INVALID = "LoginActions.OTP_INVALID";

export const errorOtpInvalid = () => ({
  type: OTP_INVALID,
});
/*-------------------- reset invalid Otp ------------------ */
export const RESET_INVALID_OTP = "LoginActions.RESET_INVALID_OTP";

export const resetInvalidOtp = () => ({
  type: RESET_INVALID_OTP,
});
/*--------------------------- get rules -------------------- */
export const GET_RULES = "LoginActions.GET_RULES";
export const PAYLOAD_GET_RULES = "LoginActions.PAYLOAD_GET_RULES";

export const getRules = (time, token) => ({
  type: GET_RULES,
  time,
  token,
});

export const payloadGetRules = (payload) => ({
  type: PAYLOAD_GET_RULES,
  payload,
});
/*---------------------------------------------------------------------------------------------- */
