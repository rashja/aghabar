/*-------------------------- external transactions -------------------- */
export const REQUEST_EXTERNAL_TRANSACTIONS =
  "ExtrenalTransactionsActions.REQUEST_EXTERNAL_TRANSACTIONS";
export const PAYLOAD_EXTERNAL_TRANSACTIONS =
  "ExtrenalTransactionsActions.PAYLOAD_EXTERNAL_TRANSACTIONS";

export const requestExtrenalTransactions = (time, tok, userId, pageNum) => ({
  type: REQUEST_EXTERNAL_TRANSACTIONS,
  tok,
  time,
  userId,
  pageNum
});

export const payloadExtrenalTransactions = payload => ({
  type: PAYLOAD_EXTERNAL_TRANSACTIONS,
  payload
});
/*------------------------------ finish work ------------------------- */
export const REQUEST_FINISH_WORK =
  "ExtrenalTransactionsActions.REQUEST_FINISH_WORK";
export const FINISH_WORK_SUCCESS_ON =
  "ExtrenalTransactionsActions.FINISH_WORK_SUCCESS_ON";
export const FINISH_WORK_SUCCESS_OF =
  "ExtrenalTransactionsActions.FINISH_WORK_SUCCESS_OF";

export const getFinishWork = (token, time, user_id, trId) => ({
  type: REQUEST_FINISH_WORK,
  token,
  time,
  user_id,
  trId
});

export const finishWorkSuccessOn = () => ({
  type: FINISH_WORK_SUCCESS_ON
});

export const finishWorkSuccessOf = () => ({
  type: FINISH_WORK_SUCCESS_OF
});
