/*-------------------------- pick driver -------------------- */
export const REQUEST_PICKED_DRIVER = "PursuitRequest.REQUEST_PICKED_DRIVER";
export const PICKED_DRIVER_SUCCESS = "PursuitRequest.PICKED_DRIVER_SUCCESS";
export const RESET_DRIVER_PICKED_MODE =
  "PursuitRequest.RESET_DRIVER_PICKED_MODE";

export const getPickedDriver = (time, tok, userId, trId, driverId) => ({
  type: REQUEST_PICKED_DRIVER,
  time,
  tok,
  userId,
  trId,
  driverId,
});

export const pickedDriverSuccess = () => ({
  type: PICKED_DRIVER_SUCCESS,
});

export const resetDriverPickedMode = () => ({
  type: RESET_DRIVER_PICKED_MODE,
});
