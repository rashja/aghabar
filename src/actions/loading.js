import { toast } from "react-toastify";

/*--------------------------- toastify ---------------------- */
export const SHOW_TOASTIFY = "SHOW_TOASTIFY";

export const showToastify = (text, mode) => {
  if (mode === "success") {
    toast.success(text, {
      position: toast.POSITION.BOTTOM_RIGHT,
      className: "toastify"
    });
  }
  if (mode === "error") {
    toast.error(text, {
      position: toast.POSITION.BOTTOM_RIGHT,
      className: "toastify"
    });
  }
  if (mode === "warning") {
    toast.warn(text, {
      position: toast.POSITION.BOTTOM_RIGHT,
      className: "toastify"
    });
  }
  if (mode === "info") {
    toast.info(text, {
      position: toast.POSITION.BOTTOM_RIGHT,
      className: "toastify"
    });
  }
  return {
    type: SHOW_TOASTIFY
  };
};
/*--------------------------- show loading ------------------ */
export const SHOW_LOADING = "loadingActions_SHOW_LOADING";

export const showLoading = () => ({
  type: SHOW_LOADING
});
/*--------------------------- hide loading ------------------ */
export const HIDE_LOADING = "loadingActions_HIDE_LOADING";

export const hideLoading = () => ({
  type: HIDE_LOADING
});
/*---------------------------- sign out --------------------- */
export const SIGN_OUT = "SIGNOUT_REQUEST";

export const signOut = () => ({
  type: SIGN_OUT
});
