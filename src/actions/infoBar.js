/*---------------------------- class Bar ----------------------- */
export const REQUEST_CLASS_BAR = "InfoBarActions.REQUEST_CLASS_BAR";
export const PAYLOAD_CLASS_BAR = "InfoBarActions.PAYLOAD_CLASS_BAR";

export const requestClassBar = (tok, time, userId, classBarId) => ({
  type: REQUEST_CLASS_BAR,
  tok,
  time,
  userId,
  classBarId,
});

export const payloadRequestClassBar = (payload) => ({
  type: PAYLOAD_CLASS_BAR,
  payload,
});
/*---------------------------- new request ---------------------- */
export const SEND_NEW_REQUEST = "InfoBarActions.SEND_NEW_REQUEST";
export const NEW_REQUEST_SUCCESS = "InfoBarActions.NEW_REQUEST_SUCCESS";
export const RESET_NEW_REQUEST_MODE = "InfoBarActions.RESET_NEW_REQUEST_MODE";

export const sendNewRequest = (
  time,
  tok,
  user_id,
  title_bar,
  class_bar,
  loading_mode,
  des_insurance,
  ispackaged,
  dimensions,
  date_sender,
  clock_sender,
  trucks_id,
  type_pay,
  weight,
  addresses,
  imageFiles
) => ({
  type: SEND_NEW_REQUEST,
  time,
  tok,
  user_id,
  title_bar,
  class_bar,
  loading_mode,
  des_insurance,
  ispackaged,
  dimensions,
  date_sender,
  clock_sender,
  trucks_id,
  type_pay,
  weight,
  addresses,
  imageFiles,
});

export const newRequestSuccess = () => ({
  type: NEW_REQUEST_SUCCESS,
});

export const resetNewRequestMode = () => ({
  type: RESET_NEW_REQUEST_MODE,
});
/*----------------------- back address to map ------------------- */
export const BACK_TO_MAP_AREA = "InfoBarActions.BACK_TO_MAP_AREA";

export const backToMapArea = (addresses, infoBarExtraInfo) => ({
  type: BACK_TO_MAP_AREA,
  payload: { addresses, infoBarExtraInfo },
});
/*--------------------- clear address for back ------------------ */
export const CLEAR_ADDRESSES = "InfoBarActions.CLEAR_ADDRESSES";

export const clearAddrresses = () => ({
  type: CLEAR_ADDRESSES,
});
