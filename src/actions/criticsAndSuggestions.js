/*-------------------------- set comments -------------------- */
export const REQUEST_SET_COMMENTS =
  "CriticsAndSuggestions.REQUEST_SET_COMMENTS";

export const requestSetComments = (time, tok, user_id, title, text) => ({
  type: REQUEST_SET_COMMENTS,
  time,
  tok,
  user_id,
  title,
  text
});
