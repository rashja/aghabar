import * as announcementsActions from "../actions/announcements";
import baseReducer from "./baseReducer";

export const initialState = {
  announcmentsList: []
};

const announcements = baseReducer(initialState, {
  [announcementsActions.PAYLOAD_EXTERNAL_TRANSACTIONS](state, action) {
    return {
      ...state,
      announcmentsList: [...state.announcmentsList, ...action.payload]
    };
  }
});

export default announcements;
