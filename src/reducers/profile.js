import * as profileActions from "../actions/profile";
import baseReducer from "./baseReducer";

export const initialState = {
  setProfileMode: false,
  getProfileMode: false
};

const profile = baseReducer(initialState, {
  [profileActions.PROFILE_SUCCESS](state, action) {
    return {
      ...state,
      setProfileMode: true
    };
  },
  [profileActions.CLEAR_PROFILE_MODE](state, action) {
    return {
      ...state,
      setProfileMode: false
    };
  },
  [profileActions.PAYLOAD_GET_PROFILE](state, action) {
    return {
      ...state,
      getProfileMode: true
    };
  },
  [profileActions.CLEAR_GET_PROFILE_MODE](state, action) {
    return {
      ...state,
      getProfileMode: false
    };
  }
});

export default profile;
