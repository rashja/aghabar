import * as pursuitRequestActions from "../actions/pursuitRequest";
import baseReducer from "./baseReducer";

export const initialState = {
  driverPickedMode: false,
};

const pursuitRequest = baseReducer(initialState, {
  [pursuitRequestActions.PICKED_DRIVER_SUCCESS](state, action) {
    return {
      ...state,
      driverPickedMode: true,
    };
  },
  [pursuitRequestActions.RESET_DRIVER_PICKED_MODE](state, action) {
    return {
      ...state,
      driverPickedMode: false,
    };
  },
});

export default pursuitRequest;
