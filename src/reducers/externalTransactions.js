import * as externalTransctionsActions from "../actions/externalTransactions";
import baseReducer from "./baseReducer";

export const initialState = {
  externalTransactionsList: [],
  finishWorkMode: false
};

const externalTransactions = baseReducer(initialState, {
  [externalTransctionsActions.PAYLOAD_EXTERNAL_TRANSACTIONS](state, action) {
    return {
      ...state,
      externalTransactionsList: action.payload.trs
    };
  },
  [externalTransctionsActions.FINISH_WORK_SUCCESS_ON](state, action) {
    return {
      ...state,
      finishWorkMode: true
    };
  },
  [externalTransctionsActions.FINISH_WORK_SUCCESS_OF](state, action) {
    return {
      ...state,
      finishWorkMode: false
    };
  }
});

export default externalTransactions;
