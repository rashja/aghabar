import * as ordersListActions from "../actions/ordersList";
import * as loginActions from "../actions/login";
import * as profileActions from "../actions/profile";
import baseReducer from "./baseReducer";

export const initialState = {
  baseInfo: null,
  rules: null,
  userInfo: null,
  showMenu: false,
  successList: [],
  currentList: [],
  cancelList: [],
  tabStatus: "DOING",
  resendRequestMode: "",
  resendRequestData: null,
  pursuitRequestMode: "",
  pursuitRequest: null,
  setRateMode: false,
};

const ordersList = baseReducer(initialState, {
  /*--------------------------- basic information ----------------------- */
  [ordersListActions.BASE_INFO](state, action) {
    return {
      ...state,
      baseInfo: action.payload,
    };
  },
  /*---------------------------- get profile ------------------------- */
  [profileActions.PAYLOAD_GET_PROFILE](state, action) {
    return {
      ...state,
      userInfo: action.payload,
    };
  },
  /*------------------------------- check login ------------------------- */
  [loginActions.CHECK_LOGIN](state, action) {
    return {
      ...state,
      userInfo: action.payload,
    };
  },
  /*------------------------------ handle menu -------------------------- */
  [ordersListActions.OPEN_MENU](state) {
    return {
      ...state,
      showMenu: !state.showMenu,
    };
  },
  [ordersListActions.CLOSE_MENU](state) {
    return {
      ...state,
      showMenu: false,
    };
  },
  /*------------------------------- set tabs ---------------------------- */
  [ordersListActions.SET_TAB](state, action) {
    return {
      ...state,
      tabStatus: action.payload,
    };
  },
  /*---------------------------- success orders ------------------------- */
  [ordersListActions.PAYLOAD_SUCCESS_ORDERS](state, action) {
    if (action.page === 1) {
      return {
        ...state,
        successList: action.payload.old_tr,
      };
    } else if (action.page > 1) {
      return {
        ...state,
        successList: [...state.successList, ...action.payload.old_tr],
        tabStatus: "SUCCESS",
      };
    }
  },
  /*---------------------------- current orders ------------------------- */
  [ordersListActions.PAYLOAD_CURRENT_ORDERS](state, action) {
    if (action.page === 1) {
      return {
        ...state,
        currentList: action.payload.current_tr,
      };
    } else if (action.page > 1) {
      return {
        ...state,
        currentList: [...state.currentList, ...action.payload.current_tr],
        tabStatus: "DOING",
      };
    }
  },
  /*---------------------------- cancel orders ------------------------- */
  [ordersListActions.PAYLOAD_CANCEL_ORDERS](state, action) {
    if (action.page === 1) {
      return {
        ...state,
        cancelList: action.payload.cancel_tr,
      };
    } else if (action.page > 1) {
      return {
        ...state,
        cancelList: [...state.cancelList, ...action.payload.cancel_tr],
        tabStatus: "FAILED",
      };
    }
  },
  /*---------------------------- resend request ------------------------- */
  [ordersListActions.SUCCESS_RESEND_REQUEST](state) {
    return {
      ...state,
      resendRequestMode: "success",
    };
  },
  [ordersListActions.RESET_RESEND_REQUEST](state) {
    return {
      ...state,
      resendRequestMode: "",
      pursuitRequestMode: "",
    };
  },
  [ordersListActions.RESEND_REQUEST_PAYLOAD](state, action) {
    return {
      ...state,
      resendRequestData: action.payload,
    };
  },
  [ordersListActions.CLEAR_RESEND_REQUESTS_DATA](state) {
    return {
      ...state,
      resendRequestData: null,
    };
  },
  [ordersListActions.PAYLOAD_SEND_PURSUIT_REQUEST](state, action) {
    return {
      ...state,
      pursuitRequest: action.payload,
      pursuitRequestMode: "success",
    };
  },
  [ordersListActions.RESET_PURSUIT_MODE](state) {
    return {
      ...state,
      pursuitRequestMode: "",
    };
  },
  /*------------------------------- set rate --------------------------- */
  [ordersListActions.SUCCESS_SET_RATE](state) {
    return {
      ...state,
      setRateMode: true,
    };
  },
  [ordersListActions.RESET_SET_RATE](state) {
    return {
      ...state,
      setRateMode: false,
      pursuitRequest: null,
    };
  },
  /*------------------------------- get rules --------------------------- */
  [loginActions.PAYLOAD_GET_RULES](state, action) {
    return {
      ...state,
      rules: action.payload,
    };
  },
  /*-------------------------------------------------------------------- */
});

export default ordersList;
