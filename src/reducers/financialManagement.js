import * as financialManagementActions from "../actions/financialManagement";
import baseReducer from "./baseReducer";

export const initialState = {
  finance: "",
  paymentList: []
};

const financialManagement = baseReducer(initialState, {
  [financialManagementActions.PAYLOAD_PAYMRNT_LIST](state, action) {
    return {
      ...state,
      finance: action.payload.payments.finance,
      paymentList: action.payload.payments.payments_list
    };
  }
});

export default financialManagement;
