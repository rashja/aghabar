import { combineReducers } from "redux";
import ordersList from "./ordersList";
import loading from "./loading";
import login from "./login";
import financialManagement from "./financialManagement";
import externalTransactions from "./externalTransactions";
import announcements from "./announcements";
import profile from "./profile";
import newRequests from "./newRequests";
import pursuitRequest from "./pursuitRequest";
import infoBar from "./infoBar";

const appReducer = combineReducers({
  externalTransactions,
  financialManagement,
  pursuitRequest,
  announcements,
  newRequests,
  ordersList,
  profile,
  infoBar,
  loading,
  login,
});
const SIGNOUT_REQUEST = "SIGNOUT_REQUEST";

const reducer = (state, action) => {
  if (action.type === SIGNOUT_REQUEST) {
    state = undefined;
  }
  return appReducer(state, action);
};

export default reducer;
