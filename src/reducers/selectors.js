import { createSelector } from "reselect";

/*----------------------------- success orders --------------------------- */
const selectSuccessOrders = (state) => state.ordersList.successList;

export const successList = createSelector(
  selectSuccessOrders,
  (allSuccessList) => allSuccessList
);
/*----------------------------- current orders --------------------------- */
const selectCurrentOrders = (state) => state.ordersList.currentList;

export const currentList = createSelector(
  selectCurrentOrders,
  (allCurrentList) => allCurrentList
);
/*------------------------------ cancel orders --------------------------- */
const selectCancelOrders = (state) => state.ordersList.cancelList;

export const cancelList = createSelector(
  selectCancelOrders,
  (allCancelList) => allCancelList
);
/*-------------------------- external transactions ----------------------- */
const selectExternalTransactions = (state) =>
  state.externalTransactions.externalTransactionsList;

export const externalTransactionsList = createSelector(
  selectExternalTransactions,
  (allExternalTransactions) => allExternalTransactions
);
/*------------------------------ announcements --------------------------- */
const selectAnnouncements = (state) => state.announcements.announcmentsList;

export const announcementsList = createSelector(
  selectAnnouncements,
  (allAnnouncements) => allAnnouncements
);
/*---------------------------- pursuist request -------------------------- */
const selectPursuitRequest = (state) => state.ordersList.pursuitRequest;

export const pursuitRequestList = createSelector(
  selectPursuitRequest,
  (allPursuitRequest) => allPursuitRequest
);
