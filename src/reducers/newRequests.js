import * as newRequestsActions from "../actions/newRequests";
import * as infoBarActions from "../actions/infoBar";
import * as ordersListActions from "../actions/ordersList";
import baseReducer from "./baseReducer";

export const initialState = {
  multipleCoordinates: [],
  areaCoordinate: null,
  newRequestsExtraInfoes: null,
};

const newRequests = baseReducer(initialState, {
  [newRequestsActions.GET_COORDINATES](state, action) {
    return {
      ...state,
      multipleCoordinates: [...state.multipleCoordinates, action.payload],
    };
  },
  [newRequestsActions.DELETE_LOCATION_FROM_MAP](state, action) {
    return {
      ...state,
      multipleCoordinates: state.multipleCoordinates.filter((coordinate) => {
        if (coordinate.text) {
          return coordinate.text !== action.payload;
        } else {
          return coordinate.type === action.payload.type
            ? coordinate.priority !== action.payload.priority
            : coordinate;
        }
      }),
    };
  },
  [newRequestsActions.GET_CITY_COORDINATE_PAYLOAD](state, action) {
    return {
      ...state,
      areaCoordinate: action.payload[0].geom.coordinates,
    };
  },
  [infoBarActions.BACK_TO_MAP_AREA](state, action) {
    return {
      ...state,
      multipleCoordinates: action.payload.addresses,
      newRequestsExtraInfoes: action.payload.infoBarExtraInfo,
    };
  },
  [ordersListActions.RESEND_REQUEST_PAYLOAD](state, action) {
    return {
      ...state,
      multipleCoordinates: action.payload.addresses,
      areaCoordinate: [
        action.payload.addresses[action.payload.addresses.length - 1].y,
        action.payload.addresses[action.payload.addresses.length - 1].x,
      ],
    };
  },
  [newRequestsActions.SET_AREA_CODE](state, action) {
    return {
      ...state,
      areaCoordinate: action.payload,
    };
  },
  [infoBarActions.RESET_NEW_REQUEST_MODE](state, action) {
    return initialState;
  },
});

export default newRequests;
