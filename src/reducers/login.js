import * as loginActions from "../actions/login";
import baseReducer from "./baseReducer";

export const initialState = {
  registerSuccessMode: false,
  logId: null,
  sendMobileMode: false,
  invalidOtp: false
};

const login = baseReducer(initialState, {
  [loginActions.REGISTER_SUCCESS](state, action) {
    return {
      ...state,
      registerSuccessMode: true
    };
  },
  [loginActions.LOG_ID](state, action) {
    return {
      ...state,
      logId: action.payload,
      sendMobileMode: true
    };
  },
  [loginActions.RESET_MOBILE_MODE](state, action) {
    return {
      ...state,
      sendMobileMode: false
    };
  },
  [loginActions.OTP_INVALID](state, action) {
    return {
      ...state,
      invalidOtp: true
    };
  },
  [loginActions.RESET_INVALID_OTP](state, action) {
    return {
      ...state,
      invalidOtp: false
    };
  }
});

export default login;
