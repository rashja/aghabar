import * as infoBarActions from "../actions/infoBar";
import * as newRequestsActions from "../actions/newRequests";
import baseReducer from "./baseReducer";

export const initialState = {
  truck: [],
  packageType: [],
  addresses: [],
  infoBarExtraInfo: null,
  newRequestMode: "",
};

const infoBar = baseReducer(initialState, {
  [infoBarActions.PAYLOAD_CLASS_BAR](state, action) {
    return {
      ...state,
      truck: action.payload.trucks,
      packageType: action.payload.package_type,
    };
  },
  [newRequestsActions.SEND_MULTIPLE_COORDINATES](state, action) {
    return {
      ...state,
      addresses: action.payload,
    };
  },
  [infoBarActions.CLEAR_ADDRESSES](state, action) {
    return {
      ...state,
      addresses: [],
      infoBarExtraInfo: [],
    };
  },
  [newRequestsActions.SEND_EXTRA_INFORMATIONS_ADDRESSES](state, action) {
    return {
      ...state,
      infoBarExtraInfo: action.payload,
    };
  },
  [infoBarActions.NEW_REQUEST_SUCCESS](state, action) {
    return {
      ...state,
      newRequestMode: "success",
    };
  },
  [infoBarActions.RESET_NEW_REQUEST_MODE](state, action) {
    return initialState;
  },
});

export default infoBar;
