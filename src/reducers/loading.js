import * as loadingActions from '../actions/loading';
import baseReducer from './baseReducer';

export const initialState = {
  loader:false
};

const loading = baseReducer(initialState, {
  [loadingActions.SHOW_LOADING](state, action) {
    return {loader:true}
  },

  [loadingActions.HIDE_LOADING](state, action) {
    return {loader:false}
  },

});

export default loading;
