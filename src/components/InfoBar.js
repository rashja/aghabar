import React, { useState, useRef, useEffect } from "react";
import ComponentWrapper from "../HOC/ComponentCover";
import { useSelector, useDispatch } from "react-redux";
import downArrow from "../assets/images/down-arrow.png";
import MultipleDropZone from "./MultipleDropZone";
import addImage from "../assets/images/add-image.svg";
import trash from "../assets/images/trash.svg";
import CustomButton from "./CustomButton";
import Modal from "./Modal";
import moment from "jalali-moment";
import { showToastify } from "../actions/loading";
import {
  requestClassBar,
  sendNewRequest,
  backToMapArea,
  clearAddrresses,
  resetNewRequestMode,
} from "../actions/infoBar";
import LoadingMode from "./LoadingMode";
import useGenerator from "../customHooks/useGenerator";
import TruckBar from "./TruckBar";
import Back from "./Back";

function InfoBar({ history }) {
  /*-------------------------------- generator ----------------------------- */
  const generator = useGenerator();
  /*----------------------------------- refs ------------------------------- */
  const inputWidthRef = useRef(null);
  const inputLengthRef = useRef(null);
  const inputNamerBarRef = useRef(null);
  const inputInsuranceRef = useRef(null);
  /*------------------------------------ states ------------------------------- */
  const initialStates = {
    imagesFiles: [],
    insurance: false,
    normalMode: false,
    transportModetitles: [],
    pickedHour: "",
    pickedHourId: "",
    classBarId: null,
    classBarTitle: "",
    packageTypeId: null,
    packageTypeTitle: "",
    pickedTruckId: [],
    pickedTruckTitle: [],
    transportModeId: [],
    maxNumber: [],
    maxWeight: [],
    weightPicked: "",
    weightPickedId: "",
    pickDate: "",
    pickDateID: "",
    typePay: "",
  };
  const [state, setState] = useState(initialStates);
  /*---------------------------- selector and dispatch ------------------------ */
  const dispatch = useDispatch();
  const stateSelector = useSelector(
    ({
      ordersList: { baseInfo, resendRequestData },
      infoBar: {
        truck,
        packageType,
        addresses,
        infoBarExtraInfo,
        newRequestMode,
      },
    }) => ({
      baseInfo,
      truck,
      packageType,
      addresses,
      infoBarExtraInfo,
      newRequestMode,
      resendRequestData,
    })
  );
  const { bars_class, complaints } = stateSelector.baseInfo;
  const { resendRequestData } = stateSelector;
  const {
    truck,
    packageType,
    addresses,
    infoBarExtraInfo,
    newRequestMode,
  } = stateSelector;
  /*---------------------------------- side effects --------------------------- */
  useEffect(() => {
    if (resendRequestData !== null) {
      const {
        width,
        bar_title,
        height,
        img_one,
        img_two,
        img_three,
        des_insurance,
        loading_mode,
        class_bar,
        class_bar_title,
        package_title,
        truck_title,
        loading_mode_id,
        type_pay,
        weight,
        package_type,
        truck_id,
      } = resendRequestData;
      inputWidthRef.current.value = width;
      inputNamerBarRef.current.value = bar_title;
      inputLengthRef.current.value = height;
      setState({
        ...state,
        imagesFiles: [img_one, img_two, img_three].filter((item) => item),
        insurance: des_insurance.length > 0 ? true : false,
        transportModetitles: loading_mode,
        classBarId: class_bar,
        classBarTitle: class_bar_title,
        packageTypeId: [package_type],
        packageTypeTitle: package_title,
        pickedTruckId: [truck_id],
        pickedTruckTitle: [truck_title],
        transportModeId: loading_mode_id,
        // maxNumber: [],
        // maxWeight: [],
        weightPicked: weight.substring(0, 1),
        // weightPickedId: "",
        typePay: type_pay,
      });
      dispatch(
        requestClassBar(
          generator.token("getListTruckAndPackageByClassBarId"),
          generator.time,
          generator.userId,
          class_bar
        )
      );
    }
  }, []);

  useEffect(() => {
    if (resendRequestData !== null && state.insurance) {
      inputInsuranceRef.current.value = resendRequestData.des_insurance;
    }
  }, [state.insurance]);

  useEffect(() => {
    if (newRequestMode === "success") {
      history.push("/");
      dispatch(resetNewRequestMode());
    }
  }, [newRequestMode]);
  /*----------------------------- information bar area ------------------------ */
  //handle trans click
  const handleTransNormalPick = (picked) => {
    if (state.normalMode) {
      setState({
        ...state,
        normalMode: !state.normalMode,
        transportModetitles: [],
        transportModeId: [],
      });
    } else {
      setState({
        ...state,
        normalMode: !state.normalMode,
        transportModetitles: [picked],
        transportModeId: ["11"],
      });
    }
  };
  //handle diff mode
  const handleDiffMode = (otherMode) => {
    let transportTitle;
    let transportId;
    if (state.transportModetitles.indexOf(otherMode.title) === -1) {
      transportTitle = [...state.transportModetitles, otherMode.title];
      transportId = [...state.transportModeId, otherMode.id];
    } else {
      transportTitle = state.transportModetitles.filter(
        (tm) => tm !== otherMode.title
      );
      transportId = state.transportModeId.filter((ti) => ti !== otherMode.id);
    }
    setState({
      ...state,
      transportModetitles: transportTitle,
      transportModeId: transportId,
    });
  };
  //render information bar
  const rendertransportationKindModal = () => {
    return (
      <div className="trans-kind-wrapper">
        <label className="checkbox-input-container d-flex">
          <input
            onClick={() => handleTransNormalPick("عادی")}
            type="checkbox"
            checked={state.normalMode}
          />
          <span className="check-mark"></span>
          <span className="pr-2">عادی</span>
        </label>
        {!state.normalMode && (
          <>
            {complaints.map(
              (comp) =>
                comp.type === "3" && (
                  <LoadingMode
                    handleOthersMode={(otherMode) => handleDiffMode(otherMode)}
                    item={comp}
                  />
                )
            )}
          </>
        )}
      </div>
    );
  };
  //handle pick class bar
  const hanldePickClassBar = (bar) => {
    setState({
      ...state,
      classBarId: bar.id,
      classBarTitle: bar.title,
    });
    document.getElementById(`${bar.id}Class`).style.color = "#e52e71";
    if (state.classBarId !== "") {
      if (document.getElementById(`${state.classBarId}Class`)) {
        document.getElementById(`${state.classBarId}Class`).style.color = "";
      }
    }
  };
  //render bar classes
  const renderBarClasses = () => {
    return (
      <div className="class-bar-wrapper">
        {bars_class.map((bar) => (
          <div
            id={`${bar.id}Class`}
            onClick={() => hanldePickClassBar(bar)}
            className="class-bar-item"
            key={bar.id}
          >
            {bar.title}
          </div>
        ))}
      </div>
    );
  };
  //handdlepackage type
  const handlePackageType = (pack) => {
    setState({
      ...state,
      packageTypeId: pack.id,
      packageTypeTitle: pack.title,
    });
    document.getElementById(`${pack.id}Package`).style.color = "#e52e71";
    if (state.classBarId !== "") {
      if (document.getElementById(`${state.packageTypeId}Package`)) {
        document.getElementById(`${state.packageTypeId}Package`).style.color =
          "";
      }
    }
  };
  //render package types
  const renderpackageTypes = () => {
    return (
      <div className="package-type-wrapper">
        {packageType.length > 0 &&
          packageType.map((pack) => (
            <div
              onClick={() => handlePackageType(pack)}
              className="package-type-item"
              key={pack.id}
              id={`${pack.id}Package`}
            >
              {pack.title}
            </div>
          ))}
      </div>
    );
  };
  //bar class
  const handleConfirmBarClass = () => {
    if (state.classBarId !== null) {
      dispatch(
        requestClassBar(
          generator.token("getListTruckAndPackageByClassBarId"),
          generator.time,
          generator.userId,
          state.classBarId
        )
      );
      setState({
        ...state,
        pickedTruckTitle: [],
        pickedTruckId: [],
        weightPicked: "",
      });
    }
  };
  //handle pick package type
  const handlePickPackageType = () => {
    if (state.classBarTitle === "") {
      dispatch(
        showToastify("ابتدا کلاس باربری مورد نظر را انتخاب کنید", "warning")
      );
    }
  };
  //render inforamion bar
  const renderInformationBar = () => {
    return (
      <div className="d-column info-bar-wrapper">
        <h5>اطلاعات بار</h5>
        <input
          ref={inputNamerBarRef}
          placeholder="نام محموله"
          className="input pr-2"
        />
        <div
          data-toggle="modal"
          data-target="#classBar"
          data-backdrop="static"
          className={[
            "input",
            "info-bar-div",
            "pr-2",
            state.classBarTitle !== "" ? "text-dark" : "text-secondary",
          ].join(" ")}
        >
          {state.classBarTitle !== "" ? state.classBarTitle : "کلاسه باری"}
          <img className="info-bar-div-arrow" alt="arrow" src={downArrow} />
        </div>
        <div className="info-bar-input-wrapper">
          <div
            onClick={() =>
              resendRequestData !== null &&
              setState({
                ...state,
                transportModetitles: [],
                transportModeId: [],
              })
            }
            data-toggle="modal"
            data-target="#transportationKind"
            data-backdrop="static"
            className={[
              "input",
              "info-bar-div",
              "pr-2",
              state.transportModetitles.length > 0
                ? "text-dark"
                : "text-secondary",
            ].join(" ")}
          >
            {state.transportModetitles.length > 0
              ? state.transportModetitles &&
                state.transportModetitles.join("/").length > 15
                ? state.transportModetitles.join("/").substring(0, 15) + "..."
                : state.transportModetitles
              : "حالت های بارگیری"}
            <img
              className="info-bar-div-arrow text-secondary"
              alt="arrow"
              src={downArrow}
            />
          </div>
          <div
            onClick={handlePickPackageType}
            data-toggle="modal"
            data-target={state.classBarTitle === "" ? "" : "#packageType"}
            className={[
              "input",
              "info-bar-div",
              "pr-2",
              state.packageTypeTitle !== "" ? "text-dark" : "text-secondary",
            ].join(" ")}
          >
            {state.packageTypeTitle !== ""
              ? state.packageTypeTitle
              : "انواع بسته بندی"}
            <img className="info-bar-div-arrow" alt="arrow" src={downArrow} />
          </div>
        </div>
        <div className="info-bar-input-wrapper">
          <input
            ref={inputLengthRef}
            placeholder="طول بار (متر)"
            className="input pr-2"
          />
          <input
            ref={inputWidthRef}
            placeholder="عرض بار (متر)"
            className="input pr-2"
          />
        </div>
        <Modal
          confirmColorBtnClass="btn-warning active"
          Id="transportationKind"
          confirmBtnName="ثبت"
          noCloseTag
          noClose
          //  handleConfirm={handleConfirmNewAddress}
          title="حالت های بارگیری"
          CustomWidth="375px"
        >
          {rendertransportationKindModal()}
        </Modal>
        <Modal
          confirmColorBtnClass="btn-warning active"
          Id="classBar"
          confirmBtnName="ثبت"
          noCloseTag
          noClose
          handleConfirm={handleConfirmBarClass}
          title="کلاس های باربری"
          CustomWidth="340px"
        >
          {renderBarClasses()}
        </Modal>
        <Modal
          confirmColorBtnClass="btn-warning active"
          Id="packageType"
          confirmBtnName="ثبت"
          noCloseTag
          noClose
          // handleConfirm={handleConfirmBarClass}
          title="انواع بسته بندی"
          CustomWidth="340px"
        >
          {renderpackageTypes()}
        </Modal>
      </div>
    );
  };
  /*----------------------------- render add photos --------------------------- */
  //Delete images
  const handleDeleteImages = () => {
    setState({
      ...state,
      imagesFiles: [],
    });
  };
  const renderAddPhotos = () => {
    return (
      <div className="d-column add-photo-wrapper">
        <span className="d-flex align-items-start">
          <h5>افزودن عکس</h5>
          {state.imagesFiles.length !== 0 && (
            <img
              onClick={handleDeleteImages}
              alt="delet"
              className="trash-images"
              src={trash}
            />
          )}
        </span>
        <div className="add-photo">
          <MultipleDropZone
            getFile={(files) =>
              setState({ ...state, imagesFiles: files.slice(0, 3) })
            }
            deleteImages={state.imagesFiles}
            defaultPhoto={addImage}
          />
        </div>
      </div>
    );
  };
  /*----------------------------- render loading area ------------------------- */
  //pick hour
  const handlePickHour = (hour, index) => {
    setState({
      ...state,
      pickedHour: hour,
      pickedHourId: index,
    });
    document.getElementById(`${index}Hour`).style.color = "#e52e71";
    if (state.pickedHourId !== "") {
      if (document.getElementById(`${state.pickedHourId}Hour`)) {
        document.getElementById(`${state.pickedHourId}Hour`).style.color = "";
      }
    }
  };
  //rendr hours modal
  const rendertransportationHoursModal = () => {
    const hours = [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
    ];
    return (
      <div className="hours-wrapper">
        {hours.map((hour, index) => (
          <div
            id={`${String(index)}Hour`}
            onClick={() => handlePickHour(hour, index)}
            className="hour-item"
            key={index}
          >
            {hour}
          </div>
        ))}
      </div>
    );
  };
  //rendewr handle Get Truck title
  const handleGetTruckBarTitle = (item) => {
    if (state.pickedTruckTitle.indexOf(item.title) === -1) {
      setState({
        ...state,
        pickedTruckTitle: [...state.pickedTruckTitle, item.title],
        maxNumber: [...state.maxNumber, item.max_weight],
        pickedTruckId: [...state.pickedTruckId, item.id],
      });
    } else {
      setState({
        ...state,
        pickedTruckTitle: state.pickedTruckTitle.filter(
          (pt) => pt !== item.title
        ),
        maxNumber: state.maxNumber.filter((mn) => mn !== item.max_weight),
        pickedTruckId: state.pickedTruckId.filter((pti) => pti !== item.id),
      });
    }
  };
  //render truck bar modal
  const renderTruckBarModal = () => {
    return (
      <div className="truck-wrapper">
        {truck.length > 0 &&
          truck.map((t) => (
            <TruckBar
              getTruckBarTitle={(title) => handleGetTruckBarTitle(title)}
              item={t}
            />
          ))}
      </div>
    );
  };
  //click on item weight tab
  const handlePickItemWeight = (item, index) => {
    setState({
      ...state,
      weightPicked: item,
      weightPickedId: index,
    });

    document.getElementById(`${index}Weight`).style.color = "#e52e71";
    if (state.weightPickedId !== "") {
      if (document.getElementById(`${state.weightPickedId}Weight`)) {
        document.getElementById(`${state.weightPickedId}Weight`).style.color =
          "";
      }
    }
  };
  //render transportation Weights Modal
  const rendertransportationWeightsModal = () => {
    const weightNumber = [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
      25,
      26,
      27,
      28,
      29,
      30,
    ];
    return (
      <div className="weight-wrapper">
        {weightNumber.map(
          (num, index) =>
            num <= Math.max(...state.maxNumber) && (
              <div
                key={index}
                id={`${index}Weight`}
                className="weight-item"
                onClick={() => handlePickItemWeight(num, index)}
              >
                {num}
              </div>
            )
        )}
      </div>
    );
  };
  //click tab truck bar
  const handleClicktruckBarTab = () => {
    if (resendRequestData !== null) {
      setState({
        ...state,
        pickedTruckTitle: [],
        pickedTruckId: [],
        weightPicked: "",
      });
    }
    if (state.classBarTitle === "") {
      dispatch(
        showToastify("ابتدا کلاس باربری مورد نظر را انتخاب کنید", "warning")
      );
    }
  };
  //click on weight tab
  const handleClickOnWeightTab = () => {
    if (state.maxNumber.length === 0) {
      dispatch(
        showToastify("ابتدا نوع کامیون مورد نظر را انتخاب کنید.", "warning")
      );
    }
  };
  //click on dates item
  const clickOnDatesItem = (d, nD) => {
    setState({
      ...state,
      pickDate: nD,
      pickDateID: d,
    });
    document.getElementById(d).style.color = "#e52e71";
    if (state.pickDateID !== "") {
      if (document.getElementById(`${state.pickDateID}`)) {
        document.getElementById(`${state.pickDateID}`).style.color = "";
      }
    }
  };
  //render dates modal
  const rendertransportationDatesModal = () => {
    const datesNum = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    return (
      <div className="dates-wrapper">
        {datesNum.map((dn) => (
          <div
            key={dn}
            id={dn}
            onClick={() =>
              clickOnDatesItem(
                dn,
                moment().add(dn, "day").locale("fa").format("YYYY/MM/DD")
              )
            }
            className="dates-item"
          >
            {moment().add(dn, "day").locale("fa").format("YYYY/MM/DD")}
          </div>
        ))}
      </div>
    );
  };
  //render loading area
  const renderLoadingArea = () => {
    return (
      <div className="d-column info-bar-wrapper">
        <h5>اطلاعات بارگیری</h5>
        <div className="info-bar-input-wrapper">
          <div
            data-toggle="modal"
            data-target={state.classBarTitle === "" ? "" : "#truckBarKind"}
            data-backdrop="static"
            className={[
              "input",
              "info-bar-div",
              "pr-2",
              state.pickedTruckTitle.length > 0
                ? "text-dark"
                : "text-secondary",
            ].join(" ")}
            onClick={handleClicktruckBarTab}
          >
            {state.pickedTruckTitle.length > 0
              ? state.pickedTruckTitle &&
                state.pickedTruckTitle.join("/").length > 15
                ? state.pickedTruckTitle.join("/").substring(0, 15) + "..."
                : state.pickedTruckTitle.join("/")
              : "انتخاب نوع کامیون"}
            <img className="info-bar-div-arrow" alt="arrow" src={downArrow} />
          </div>
          <div
            onClick={handleClickOnWeightTab}
            data-toggle="modal"
            data-target={
              state.maxNumber.length === 0 ? "" : "#transportationWeights"
            }
            className={[
              "input",
              "info-bar-div",
              "pr-2",
              state.weightPicked !== "" ? "text-dark pr-5" : "text-secondary",
            ].join(" ")}
          >
            {state.weightPicked !== "" ? state.weightPicked : "وزن تقریبی"}
            <img
              className="info-bar-div-arrow text-secondary"
              alt="arrow"
              src={downArrow}
            />
          </div>
        </div>
        <div className="info-bar-input-wrapper">
          <div
            data-toggle="modal"
            data-target="#transportationDates"
            className={[
              "input",
              "info-bar-div",
              "pr-2",
              state.pickDate !== "" ? "text-dark pr-5" : "text-secondary",
            ].join(" ")}
          >
            {state.pickDate !== "" ? state.pickDate : "تاریخ بارگیری"}
            <img
              className="info-bar-div-arrow text-secondary"
              alt="arrow"
              src={downArrow}
            />
          </div>
          <div
            data-toggle="modal"
            data-target="#transportationHours"
            className={[
              "input",
              "info-bar-div",
              "pr-2",
              state.pickedHour !== "" ? "text-dark pr-5" : "text-secondary",
            ].join(" ")}
          >
            {state.pickedHour !== "" ? state.pickedHour : "ساعت بارگیری"}
            <img className="info-bar-div-arrow" alt="arrow" src={downArrow} />
          </div>
        </div>
        <Modal
          confirmColorBtnClass="btn-warning active"
          Id="transportationHours"
          confirmBtnName="ثبت"
          noCloseTag
          noClose
          title="زمان بارگیری"
          CustomWidth="250px"
        >
          {rendertransportationHoursModal()}
        </Modal>
        <Modal
          confirmColorBtnClass="btn-warning active"
          Id="truckBarKind"
          confirmBtnName="ثبت"
          noCloseTag
          noClose
          title="انتخاب نوع کامیون"
          CustomWidth="300px"
        >
          {renderTruckBarModal()}
        </Modal>
        <Modal
          confirmColorBtnClass="btn-warning active"
          Id="transportationWeights"
          confirmBtnName="ثبت"
          noCloseTag
          noClose
          title="وزن تقریبی"
          CustomWidth="250px"
        >
          {rendertransportationWeightsModal()}
        </Modal>
        <Modal
          confirmColorBtnClass="btn-warning active"
          Id="transportationDates"
          confirmBtnName="ثبت"
          noCloseTag
          noClose
          title="تاریخ بارگیری"
          CustomWidth="250px"
        >
          {rendertransportationDatesModal()}
        </Modal>
      </div>
    );
  };
  /*----------------------------- render payment area ------------------------- */
  //handle Insurance
  const handleInsurance = () => {
    setState({
      ...state,
      insurance: !state.insurance,
    });
  };
  //handle type pay
  const handleTypePay = (type) => {
    setState({
      ...state,
      typePay: type,
    });
  };
  //render payment
  const renderPaymentArea = () => {
    return (
      <div className="d-column payment-area-wrapper">
        <h5>نوع پرداخت</h5>
        <div className="payment-area-radios-wrapper">
          <label
            onClick={() => handleTypePay("1")}
            className="radio-input-container border-btm d-flex"
          >
            <input
              type="radio"
              checked={state.typePay === "1" ? "checked" : ""}
              name="radio"
            />
            <span className="checkmark"></span>
            <span className="mr-2">پیش کرایه</span>
          </label>
          <label
            onClick={() => handleTypePay("2")}
            className="radio-input-container border-btm d-flex"
          >
            <input
              type="radio"
              checked={state.typePay === "2" ? "checked" : ""}
              name="radio"
            />
            <span className="checkmark"></span>
            <span className="mr-2">پس کرایه</span>
          </label>
        </div>
        <div
          className={[
            "d-flex",
            state.insurance ? "radio-active-wrapper" : "radio-wrapper",
          ].join(" ")}
        >
          <label className="switch">
            <input
              type="checkbox"
              checked={state.insurance}
              onChange={handleInsurance}
            />
            <div className="slider"></div>
          </label>
          <span className="mr-2">
            <b>بیمه</b>
          </span>
        </div>
        {state.insurance && (
          <div>
            <input
              ref={inputInsuranceRef}
              placeholder="مبلغ بیمه (تومان)"
              type="text"
              className="input pr-2 text-secondary mt-3"
            />
          </div>
        )}
      </div>
    );
  };
  /*--------------------------- handleBack to map area ------------------------ */
  const handleBack = () => {
    dispatch(backToMapArea(addresses, infoBarExtraInfo));
    dispatch(clearAddrresses());
  };
  /*----------------------------- confirm new request ------------------------- */
  const confirmNewRequest = () => {
    if (
      inputNamerBarRef.current.value.length > 0 &&
      state.classBarId !== null &&
      state.transportModeId.length > 0 &&
      state.packageTypeId !== null &&
      state.pickDate !== "" &&
      state.pickedHour !== "" &&
      state.pickedTruckId.length > 0 &&
      state.typePay !== "" &&
      state.weightPicked !== ""
    ) {
      dispatch(
        sendNewRequest(
          generator.time,
          generator.token("newRequest"),
          generator.userId,
          inputNamerBarRef.current.value, // title bar
          state.classBarId, // class bar
          state.transportModeId.join("-"), // transport mode
          inputInsuranceRef.current && inputInsuranceRef.current.value !== ""
            ? inputInsuranceRef.current.value
            : null, // insurance
          state.packageTypeId, // package type
          `${inputLengthRef.current.value}-${inputWidthRef.current.value}`, // size
          state.pickDate, // date
          state.pickedHour, // hour
          state.pickedTruckId.join("-"), //trucks id
          state.typePay, // type pay
          state.weightPicked, //weight
          addresses,
          state.imagesFiles
        )
      );
    } else {
      dispatch(
        showToastify("لطفا تمامی فیلد های اجباری را پر کنید", "warning")
      );
    }
  };
  /*--------------------------------------------------------------------------- */
  return (
    <div className="info-bar-container">
      <div onClick={handleBack} className="info-bar-back">
        <Back />
      </div>
      {renderInformationBar()}
      {renderAddPhotos()}
      {renderLoadingArea()}
      {renderPaymentArea()}
      <div className="confirm-new-request-wrapper">
        <CustomButton onClick={confirmNewRequest} btnClass="yellow-btn">
          ثبت درخواست
        </CustomButton>
      </div>
    </div>
  );
}

export default ComponentWrapper(InfoBar);
