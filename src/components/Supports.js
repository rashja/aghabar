import React from "react";
import ComponentWrapper from "../HOC/ComponentCover";
import phone from "../assets/images/phone.png";
import mail from "../assets/images/mail.svg";
import mapLoacation from "../assets/images/map-pin.svg";
import GoogleMap from "./GoogleMap";
import aparat from "../assets/images/aparat.png";
import telegram from "../assets/images/telegram.png";
import instagram from "../assets/images/instagram.png";
import { useSelector } from "react-redux";
import Modal from "./Modal";
import CustomButton from "./CustomButton";

function Supports() {
  /*-------------------------------- selectors ----------------------------- */
  const stateSelector = useSelector(({ ordersList: { baseInfo, rules } }) => ({
    baseInfo,
    rules,
  }));
  const { company_information, social_networks } = stateSelector.baseInfo;
  const { rules_user } = stateSelector.rules;
  /*---------------------------- render suport infoes ---------------------- */
  const renderSupportInformations = () => {
    return (
      <div className="support-info-container">
        <h5 className="support-info-container-title">
          از طریق راه‌های زیر میتوانید با ما در ارتباط باشید.
        </h5>
        <div className="support-info-container-each-info">
          <img style={{ width: "24px", height: "24px" }} src={phone} />
          <h5>{company_information.phone}</h5>
        </div>
        <div className="support-info-container-each-info">
          <img src={mail} />
          <h5>{company_information.mail}</h5>
        </div>
        <div className="support-info-container-each-info">
          <img src={mapLoacation} />
          <h5>{company_information.address}</h5>
        </div>
      </div>
    );
  };
  /*------------------------------------------------------------------------ */
  return (
    <div className="supports-container">
      <h3 className="title-header">پشتیبانی</h3>
      {renderSupportInformations()}
      <div className="g-map">
        <GoogleMap
          disableClick
          newMapStyle={{ height: "200px", borderRadius: "20px" }}
          lat={38.4237}
          lng={27.1428}
        />
      </div>
      <div className="social-media-rules-wrappers">
        <div className="btn-rules-wrapper">
          <CustomButton
            data-toggle="modal"
            data-target="#support-rules"
            btnClass="yellow-btn"
          >
            قوانین و مقررات
          </CustomButton>
        </div>
        <div className="social-media-wrapper">
          <a href={social_networks.aparat}>
            <img style={{ width: "60px" }} alt="aparat" src={aparat} />
          </a>
          <a href={social_networks.instagram}>
            <img
              style={{ width: "50px", height: "50px" }}
              alt="instagram"
              src={instagram}
            />
          </a>
          <a href={social_networks.telegram}>
            <img
              style={{ width: "50px", height: "50px" }}
              alt="telegram"
              src={telegram}
            />
          </a>
        </div>
      </div>
      <Modal
        title="قوانین سایت"
        noClose
        confirmBtnName="مطالعه شد"
        noCloseTag
        confirmColorBtnClass="btn-info active"
        Id="support-rules"
      >
        <b>{rules_user}</b>
      </Modal>
    </div>
  );
}

export default ComponentWrapper(Supports);
