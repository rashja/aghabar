import React, { useRef, useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import { persistor } from "./../store/index";
import { useSelector, useDispatch } from "react-redux";
import { signOut } from "../actions/loading";
import add from "../assets/images/menu/add.png";
import bell from "../assets/images/menu/bell.png";
import clipBoard from "../assets/images/menu/clipboard.png";
import exit from "../assets/images/menu/exit.png";
import headset from "../assets/images/menu/headset.png";
import sms from "../assets/images/menu/sms.png";
import payCard from "../assets/images/menu/pay-card.png";
import transaction from "../assets/images/menu/transaction.png";
import profile from "../assets/images/profile.png";
import Modal from "./Modal";
import {
  requestSetProfile,
  requestGetProfile,
  ClearProfileMode,
} from "../actions/profile";
import { ImageUrl } from "../apis/constants";
import { closeMenu } from "../actions/ordersList";
import Dropzone from "./Dropzone";
import useGenerator from "../customHooks/useGenerator";

function Menu(props) {
  /*---------------------------- generator ------------------------ */
  const generator = useGenerator();
  /*------------------------------- refs -------------------------- */
  const firstNameInputRef = useRef();
  const lastNameInputRef = useRef();
  /*------------------------------ states ------------------------- */
  const [profilePicture, setProfilePicture] = useState(null);
  /*----------------------- dispatch & selectos ------------------- */
  const dispatch = useDispatch();
  const stateSelector = useSelector(
    ({ ordersList: { userInfo }, profile: { setProfileMode } }) => ({
      userInfo,
      setProfileMode,
    })
  );
  const { userInfo, setProfileMode } = stateSelector;
  /*--------------------------- side effects ---------------------- */
  useEffect(() => {
    if (setProfileMode) {
      dispatch(
        requestGetProfile(
          generator.time,
          generator.token("getProfile"),
          generator.userId
        )
      );
      dispatch(ClearProfileMode());
    }
  }, [setProfileMode]);
  /*----------------------------- sign put ------------------------ */
  const hanldeSignOut = () => {
    dispatch(signOut());
    persistor.purge();
    dispatch(signOut());
  };
  /*--------------------------- close menu ------------------------ */
  const handleCloseMenu = () => dispatch(closeMenu());
  /*----------------------- confirm edit profile ------------------ */
  const handleConfirmEditProfile = () => {
    if (firstNameInputRef.current && lastNameInputRef.current) {
      const firstName = firstNameInputRef.current.value;
      const lastName = lastNameInputRef.current.value;
      dispatch(
        requestSetProfile(
          generator.time,
          generator.token("setProfile"),
          generator.userId,
          firstName,
          lastName,
          profilePicture
        )
      );
    }
  };
  /*--------------------------- edit profile ---------------------- */
  const renderEditProfile = () => {
    return (
      <div className="d-column">
        <Dropzone
          justOne
          getFile={(file) => setProfilePicture(file[0])}
          updatePic={`${ImageUrl}users/${userInfo.picture}`}
          defaultPhoto={profile}
          checkUpdatePic={userInfo.picture}
        />
        <input
          ref={firstNameInputRef}
          defaultValue={userInfo.fname}
          className="input pr-3 m-2 h6"
          placeholder="نام"
          type="text"
        />
        <input
          ref={lastNameInputRef}
          defaultValue={userInfo.lname}
          className="input pr-3 m-2 h6"
          placeholder="نام خانوادگی"
          type="text"
        />
      </div>
    );
  };
  /*--------------------------- profile area ---------------------- */
  const renderProfileArea = () => {
    return (
      <>
        <div className="menu-profile-wrapper">
          <div className="menu-profile-image ml-3">
            <img
              style={{ width: "50px" }}
              alt="profile"
              src={
                userInfo.picture !== ""
                  ? `${ImageUrl}users/${userInfo.picture}`
                  : profile
              }
            />
          </div>
          <div className="menu-profile-info">
            <span className="mb-2 pl-5">{`${userInfo.fname} ${userInfo.lname}`}</span>
            <div style={{ marginBottom: 0 }} className="d-flex">
              <b>{userInfo.mobile}</b>
              <div data-toggle="modal" data-target="#edit-profile">
                <p style={{ margin: "10px 18px 0 0", cursor: "pointer" }}>
                  ویرایش
                </p>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };
  /*--------------------------------------------------------------- */
  return (
    <>
      <Modal
        handleConfirm={handleConfirmEditProfile}
        confirmColorBtnClass="btn-warning active"
        Id="edit-profile"
        closeBtnName="بستن"
        confirmBtnName="تایید"
        noCloseTag
        title="ویرایش پروفایل"
      >
        {renderEditProfile()}
      </Modal>
      <div
        style={props.showMenu ? { width: "250px" } : { width: "0" }}
        className="sidenav"
      >
        {renderProfileArea()}
        <NavLink
          exact
          to="/"
          activeClassName="nav-active"
          onClick={handleCloseMenu}
        >
          <div className="d-flex">
            <img className="ml-2" alt="clipBoard" src={clipBoard} />
            <span className="mt-1">خانه</span>
          </div>
        </NavLink>
        <NavLink
          activeClassName="nav-active"
          onClick={handleCloseMenu}
          to="/addRequest"
        >
          <div className="d-flex">
            <img className="ml-2" alt="add" src={add} />
            <span className="mt-1">ثبت درخواست حمل</span>
          </div>
        </NavLink>
        <NavLink
          activeClassName="nav-active"
          onClick={handleCloseMenu}
          to="/externalTransactions"
        >
          <div className="d-flex">
            <img className="ml-2" alt="transaction" src={transaction} />
            <span className="mt-1">تراکنش های خارج از سیستم</span>
          </div>
        </NavLink>
        <NavLink
          activeClassName="nav-active"
          onClick={handleCloseMenu}
          to="/financialManagment"
        >
          <div className="d-flex">
            <img className="ml-2" alt="payCard" src={payCard} />
            <span className="mt-1">مدیریت مالی</span>
          </div>
        </NavLink>
        <NavLink
          activeClassName="nav-active"
          onClick={handleCloseMenu}
          to="/announcements"
        >
          <div className="d-flex">
            <img className="ml-2" alt="bell" src={bell} />
            <span className="mt-1">اعلانات</span>
          </div>
        </NavLink>
        <NavLink
          activeClassName="nav-active"
          onClick={handleCloseMenu}
          to="/supports"
        >
          <div className="d-flex">
            <img className="ml-2" alt="headset" src={headset} />
            <span className="mt-1">پشتیبانی</span>
          </div>
        </NavLink>
        <NavLink
          activeClassName="nav-active"
          onClick={handleCloseMenu}
          to="/criticsAndSuggestions"
        >
          <div className="d-flex">
            <img className="ml-2" alt="sms" src={sms} />
            <span className="mt-1">ارسال نظرات و پیشنهادات</span>
          </div>
        </NavLink>
        <NavLink
          activeClassName="nav-active"
          onClick={hanldeSignOut}
          to="/login"
        >
          <div className="d-flex">
            <img className="ml-2" alt="exit" src={exit} />
            <span className="mt-1">خروج از حساب کاربری</span>
          </div>
        </NavLink>
      </div>
    </>
  );
}

export default Menu;
