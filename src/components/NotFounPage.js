import React from "react";
import truck from "../assets/images/loading.gif";

function NotFounPage() {
  return (
    <div className="not-found-page-wrapper">
      <h1 className="not-found-error">404 !</h1>
      <h2>Not Found</h2>
      <h3 className="mt-3">
        {" "}
        <b>We can`t seem to find the page you`re looking for</b>
      </h3>
      <div>
        <img alt="truck-404" style={{ width: "500px" }} src={truck} />
      </div>
    </div>
  );
}

export default NotFounPage;
