import React, { useState, useMemo, useEffect, useRef } from "react";
import moment from "jalali-moment";
import { useDispatch, useSelector, batch } from "react-redux";
import ComponentWrapper from "../HOC/ComponentCover";
import { getPreCode } from "../actions/login";
import Rating from "./RatingStar";
import profile from "../assets/images/profile.png";
import truck from "../assets/images/truck.png";
import { showToastify } from "../actions/loading";
import {
  requestSuccessOrders,
  requestCurrentOrders,
  requestCancelOrders,
  setTab,
  setRate,
  resetSetRate,
  resetResendRequest,
  clearResendRequestsData,
  resetPursuitRequestMode,
} from "../actions/ordersList";
import SuccessOrderItem from "./SuccessOrderItem";
import CancelOrderItem from "./CancelOrderItem";
import CurrentOrderItem from "./CurrentOrderItem";
import CustomButton from "./CustomButton";
import {
  successList,
  currentList,
  cancelList,
  pursuitRequestList,
} from "../reducers/selectors";
import useGenerator from "../customHooks/useGenerator";
import { resetNewRequestMode } from "../actions/infoBar";
import Modal from "./Modal";
var md5 = require("md5");

const OrdersList = ({ history }) => {
  /*------------------------------ generator --------------------------- */
  const generator = useGenerator();
  /*--------------------------------- ref ------------------------------- */
  const rateDriverBtnRef = useRef(null);
  /*------------------------- dispatch & selectors ---------------------- */
  const dispatch = useDispatch();
  const stateSelector = useSelector(({ ordersList }) => ({
    ordersList,
  }));
  const successOrders = useSelector(successList);
  const currentOrders = useSelector(currentList);
  const cancelOrders = useSelector(cancelList);
  const pursuitRequest = useSelector(pursuitRequestList);
  const {
    baseInfo,
    tabStatus,
    setRateMode,
    resendRequestMode,
    pursuitRequestMode,
  } = stateSelector.ordersList;
  /*-------------------------------- states ----------------------------- */
  const initialStates = {
    page: 1,
    rateDriver: 0,
    complaintsId: [],
  };
  const [state, setState] = useState(initialStates);
  /*--------------------------------- memorize ------------------------------- */
  const memoizedSuccessOrders = useMemo(() => successOrders, [successOrders]);
  const memoizedCurrentOrders = useMemo(() => currentOrders, [currentOrders]);
  const memoizedCancelOrders = useMemo(() => cancelOrders, [cancelOrders]);
  const memoizedPursuitRequest = useMemo(() => pursuitRequest, [
    pursuitRequest,
  ]);
  /*----------------------------- side effects -------------------------- */
  useEffect(() => {
    const time = moment().locale("").format("YYYY/MM/DD");
    const tokenNoUserId = md5(`${time}checkVersionOfSystem`);
    batch(() => {
      dispatch(getPreCode(generator.time, tokenNoUserId));
      dispatch(
        requestSuccessOrders(
          generator.userId,
          generator.time,
          generator.token("oldTransactions"),
          state.page
        )
      );
      dispatch(
        requestCurrentOrders(
          generator.userId,
          generator.time,
          generator.token("currentTransactions"),
          state.page
        )
      );
      dispatch(
        requestCancelOrders(
          generator.userId,
          generator.time,
          generator.token("cancelingTransactions"),
          state.page
        )
      );
      dispatch(clearResendRequestsData());
      dispatch(resetNewRequestMode());
    });
    if (memoizedPursuitRequest !== null) {
      if (
        memoizedPursuitRequest.flag_rate === "0" &&
        memoizedPursuitRequest.current_tr.finished === "1"
      ) {
        if (rateDriverBtnRef.current) {
          rateDriverBtnRef.current.click();
        }
      }
    }
  }, []);

  useEffect(() => {
    if (memoizedPursuitRequest !== null) {
      if (
        memoizedPursuitRequest.current_tr &&
        (memoizedPursuitRequest.current_tr.step_id === "22" ||
          memoizedPursuitRequest.current_tr.step_id === "21" ||
          memoizedPursuitRequest.current_tr.step_id === "20")
      ) {
        dispatch(resetSetRate());
        dispatch(showToastify("این بار رد شد.", "error"));
      }
    }
  }, [
    memoizedPursuitRequest &&
      memoizedPursuitRequest.current_tr &&
      memoizedPursuitRequest.current_tr.step_id,
  ]);

  useEffect(() => {
    if (setRateMode) {
      dispatch(resetSetRate());
    }
  }, [setRateMode]);

  useEffect(() => {
    if (pursuitRequestMode === "success") {
      history.push("/pursuitRequest");
      dispatch(resetPursuitRequestMode());
    }
  }, [pursuitRequestMode]);

  useEffect(() => {
    if (resendRequestMode === "success") {
      history.push("/addRequest");
      dispatch(resetResendRequest());
    }
  }, [resendRequestMode]);

  useEffect(() => {
    document.addEventListener("scroll", function (event) {
      if (
        (tabStatus === "SUCCESS" && memoizedSuccessOrders.length > 0) ||
        (tabStatus === "FAILED" && memoizedCancelOrders.length > 0) ||
        (tabStatus === "DOING" && memoizedCurrentOrders.length > 0)
      ) {
        if (
          window.innerHeight + window.scrollY >=
          document.getElementById("HTML").offsetHeight
        ) {
          setState({
            ...state,
            page: state.page++,
          });
        }
      }
    });
  }, ["pagination"]);

  useEffect(() => {
    if (state.page > 1) {
      if (tabStatus === "SUCCESS") {
        dispatch(
          requestSuccessOrders(
            generator.userId,
            generator.time,
            generator.token("oldTransactions"),
            state.page
          )
        );
      }
      if (tabStatus === "FAILED") {
        dispatch(
          requestCancelOrders(
            generator.userId,
            generator.time,
            generator.token("cancelingTransactions"),
            state.page
          )
        );
      }
      if (tabStatus === "DOING") {
        dispatch(
          requestCurrentOrders(
            generator.userId,
            generator.time,
            generator.token("currentTransactions"),
            state.page
          )
        );
      }
    }
  }, [state.page]);
  /*--------------------------------- tabs ----------------------------- */
  //handle tabs
  const handleTab = (mode) => {
    dispatch(setTab(mode));
  };
  //render tabs
  const renderTabs = () => {
    return (
      <div className="tabs-wrapper">
        <div
          onClick={() => handleTab("DOING")}
          className={["tab", tabStatus === "DOING" && "tab-active"].join(" ")}
        >
          در حال انجام
        </div>
        <div
          onClick={() => handleTab("SUCCESS")}
          className={["tab", tabStatus === "SUCCESS" && "tab-active"].join(" ")}
        >
          موفقیت آمیز
        </div>
        <div
          onClick={() => handleTab("FAILED")}
          className={["tab", tabStatus === "FAILED" && "tab-active"].join(" ")}
        >
          ناموفق
        </div>
      </div>
    );
  };
  /*----------------------------- doing orders -------------------------- */
  const renderDoingOrders = () => {
    return (
      <div className="container">
        <div
          className={`row ${
            memoizedCurrentOrders && memoizedCurrentOrders.length === 0
              ? "d-column"
              : ""
          }`}
        >
          {memoizedCurrentOrders && memoizedCurrentOrders.length > 0 ? (
            memoizedCurrentOrders.map((item, index) => (
              <CurrentOrderItem key={index} item={item} />
            ))
          ) : (
            <div className="truck-wrapper">
              <div>
                <img className="truck-img" alt="truck" src={truck} />
              </div>
              <div className="h5 mt-5">
                تاکنون سفارش بار در حال انجام ثبت نشده است.
              </div>
              <div className="btn-request-wrapper">
                <CustomButton
                  onClick={() => history.push("/addRequest")}
                  btnClass="yellow-btn"
                >
                  <b>افزودن بار جدید</b>
                </CustomButton>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  };
  /*----------------------------- success orders ------------------------ */
  const renderSuccessOrders = () => {
    return (
      <div className="container">
        <div
          className={`row ${
            memoizedCurrentOrders && memoizedCurrentOrders.length === 0
              ? "d-column"
              : ""
          }`}
        >
          {memoizedSuccessOrders && memoizedSuccessOrders.length > 0 ? (
            memoizedSuccessOrders.map((item, index) => (
              <SuccessOrderItem key={index} item={item} />
            ))
          ) : (
            <div className="truck-wrapper">
              <div>
                <img className="truck-img" alt="truck" src={truck} />
              </div>
              <div className="h5 mt-5">
                تاکنون سفارش بار موفقی ثبت نشده است.
              </div>
              <div className="btn-request-wrapper">
                <CustomButton
                  onClick={() => history.push("/addRequest")}
                  btnClass="yellow-btn"
                >
                  <b>افزودن بار جدید</b>
                </CustomButton>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  };
  /*----------------------------- failed orders ------------------------- */
  const renderFailedOrders = () => {
    return (
      <div className="container">
        <div
          className={`row ${
            memoizedCurrentOrders && memoizedCurrentOrders.length === 0
              ? "d-column"
              : ""
          }`}
        >
          {memoizedCancelOrders && memoizedCancelOrders.length > 0 ? (
            memoizedCancelOrders.map((item, index) => (
              <CancelOrderItem key={index} item={item} />
            ))
          ) : (
            <div className="truck-wrapper">
              <div>
                <img className="truck-img" alt="truck" src={truck} />
              </div>
              <div className="h5 mt-5">
                تاکنون سفارش بار ناموفقی ثبت نشده است.
              </div>
              <div className="btn-request-wrapper">
                <CustomButton
                  onClick={() => history.push("/addRequest")}
                  btnClass="yellow-btn"
                >
                  <b>افزودن بار جدید</b>
                </CustomButton>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  };
  /*----------------------------- driving area ----------------------- */
  //handle set rate
  const handleSetRate = () => {
    dispatch(
      setRate(
        generator.token("setRate"),
        generator.time,
        generator.userId,
        state.rateDriver,
        memoizedPursuitRequest.current_tr.tr_id,
        state.complaintsId
      )
    );
  };
  //pick complaints
  const handlePickComplaints = (com) => {
    if (state.complaintsId.indexOf(com.id) === -1) {
      setState({
        ...state,
        complaintsId: [...state.complaintsId, com.id],
      });
    } else {
      setState({
        ...state,
        complaintsId: state.complaintsId.filter((it) => it !== com.id),
      });
    }
  };
  //rating items
  const renderRatingItems = () => {
    let firstName =
      memoizedPursuitRequest && memoizedPursuitRequest.step_2.fname;
    let lastName =
      memoizedPursuitRequest && memoizedPursuitRequest.step_2.lname;
    return (
      <div>
        <div className="d-column">
          <div className="d-flex justify-content-center">
            <img alt="profile" style={{ width: "100px" }} src={profile} />
          </div>
          <div>
            <div className="h4">{`${firstName} ${lastName}`}</div>
          </div>
          <div className="rating mb-3">
            <Rating
              choosedRate={(rate) => setState({ ...state, rateDriver: rate })}
            />
          </div>
        </div>
        <div>
          {baseInfo &&
            baseInfo.complaints.map((com, index) => {
              if (com.type === "1") {
                return (
                  <div key={index}>
                    <label className="checkbox-input-container d-flex">
                      <input
                        onClick={() => handlePickComplaints(com)}
                        type="checkbox"
                      />
                      <span className="check-mark"></span>
                      <span className="pr-2">{com.title}</span>
                    </label>
                  </div>
                );
              }
            })}
        </div>
      </div>
    );
  };
  //driver modal
  const renderDriverRateModal = () => {
    return (
      <>
        <button
          ref={rateDriverBtnRef}
          style={{ display: "none" }}
          data-toggle="modal"
          data-target="#rateDriver"
        ></button>
        <Modal
          handleConfirm={handleSetRate}
          closeBtnName="بستن"
          confirmColorBtnClass="btn-warning active"
          Id="rateDriver"
          confirmBtnName="ثبت"
          noCloseTag
          title="امتیاز دهی"
          CustomWidth="450px"
        >
          {renderRatingItems()}
        </Modal>
      </>
    );
  };
  /*--------------------------------------------------------------------- */
  return (
    <div className="orders-list-container">
      <div id="showScroll" className="title-header">
        لیست سفارش ها
      </div>
      {renderTabs()}
      {renderDriverRateModal()}
      {tabStatus === "DOING" && renderDoingOrders()}
      {tabStatus === "SUCCESS" && renderSuccessOrders()}
      {tabStatus === "FAILED" && renderFailedOrders()}
    </div>
  );
};

export default ComponentWrapper(OrdersList);
