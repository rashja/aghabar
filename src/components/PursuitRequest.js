import React, { useEffect, useMemo, useRef } from "react";
import ComponentWrapper from "../HOC/ComponentCover";
import Expantion from "./Expantion";
import CustomButton from "./CustomButton";
import mapWatch from "../assets/images/map-watch.png";
import refresh from "../assets/images/refresh.svg";
import { sendPursuitRequest, resetResendRequest } from "../actions/ordersList";
import { useDispatch, useSelector } from "react-redux";
import { resetDriverPickedMode } from "../actions/pursuitRequest";
import Modal from "./Modal";
import GoogleMap from "./GoogleMap";
import ConfirmNewRequest from "./ConfirmRequestPursuit";
import BeginningLocationsPursuit from "./BeginningLocationsPursuit";
import LoadingPursuit from "./LoadingPursuit";
import { pursuitRequestList } from "../reducers/selectors";
import DestinationLocationsPusrsuit from "./DestinationLocationsPusrsuit";
import OperatorsPursuitRequest from "./OperatorsPursuitRequest";
import PorteragePursuitRequest from "./PorteragePursuitRequest";
import DriverChoosesPursuitRequest from "./DriverChoosesPursuitRequest";
import DeliveresPayloads from "./DeliveresPayloads";
import useInterval from "../customHooks/useInterval";
import useGenerator from "../customHooks/useGenerator";

const PursuitRequest = ({ history }) => {
  /*------------------------------------ refrence ----------------------------------- */
  const pursuitRequsetRef = useRef();
  /*------------------------------------ generator ---------------------------------- */
  const generator = useGenerator();
  /* ------------------------------ dispatch & selector ----------------------------- */
  const dispatch = useDispatch();
  const stateSelector = useSelector(
    ({
      ordersList: { pursuitRequestMode },
      pursuitRequest: { driverPickedMode },
    }) => ({
      pursuitRequestMode,
      driverPickedMode,
    })
  );
  const pursuitRequest = useSelector(pursuitRequestList);
  const { pursuitRequestMode, driverPickedMode } = stateSelector;
  /*------------------------------------ memorize ---------------------------------- */
  const memoizedPursuitRequest = useMemo(() => pursuitRequest, [
    pursuitRequest,
  ]);
  /* --------------------------------- side effects -------------------------------- */
  useEffect(() => {
    if (memoizedPursuitRequest === null) {
      history.push("/");
    } else {
      dispatch(
        sendPursuitRequest(
          generator.time,
          generator.token("getCurrentItemTrOfUsersByUserId"),
          generator.userId,
          memoizedPursuitRequest.current_tr &&
            memoizedPursuitRequest.current_tr.tr_id
        )
      );
    }
  }, []);

  useInterval(() => {
    // Your custom logic here
    dispatch(
      sendPursuitRequest(
        generator.time,
        generator.token("getCurrentItemTrOfUsersByUserId"),
        generator.userId,
        memoizedPursuitRequest.current_tr &&
          memoizedPursuitRequest.current_tr.tr_id
      )
    );
  }, 60000);

  useEffect(() => {
    if (
      memoizedPursuitRequest &&
      memoizedPursuitRequest.current_tr &&
      (memoizedPursuitRequest.current_tr.step_id === "22" ||
        memoizedPursuitRequest.current_tr.step_id === "21" ||
        memoizedPursuitRequest.current_tr.step_id === "20")
    ) {
      history.push("/");
    }
  }, [
    memoizedPursuitRequest &&
      memoizedPursuitRequest.current_tr &&
      memoizedPursuitRequest.current_tr.step_id,
  ]);

  useEffect(() => {
    if (pursuitRequestMode === "success") {
      dispatch(resetResendRequest());
    }
  }, [pursuitRequestMode]);

  useEffect(() => {
    if (driverPickedMode) {
      dispatch(
        sendPursuitRequest(
          generator.time,
          generator.token("getCurrentItemTrOfUsersByUserId"),
          generator.userId,
          memoizedPursuitRequest &&
            memoizedPursuitRequest.current_tr &&
            memoizedPursuitRequest.current_tr.tr_id
        )
      );
      dispatch(resetDriverPickedMode());
    }
  }, [driverPickedMode]);

  useEffect(() => {
    if (
      memoizedPursuitRequest &&
      memoizedPursuitRequest.current_tr &&
      memoizedPursuitRequest.current_tr.finished === "1"
    ) {
      history.push("/");
    }
  }, [
    memoizedPursuitRequest &&
      memoizedPursuitRequest.current_tr &&
      memoizedPursuitRequest.current_tr.finished,
  ]);
  /* ------------------------------ render map and reload -------------------------- */
  // handle refresh api
  const handleRefreshApi = () => {
    dispatch(
      sendPursuitRequest(
        generator.time,
        generator.token("getCurrentItemTrOfUsersByUserId"),
        generator.userId,
        memoizedPursuitRequest.current_tr &&
          memoizedPursuitRequest.current_tr.tr_id
      )
    );
  };
  //handle resize map
  const handleResizeMap = () => {
    window.addEventListener("resize", () => {
      if (
        pursuitRequsetRef.current &&
        pursuitRequsetRef.current.offsetWidth < 550
      ) {
        return { heigth: "200px" };
      }
    });
  };
  //render map and reload
  const renderMapAndReload = () => {
    return (
      <div className="map-reload-wrapper">
        <div className="btn-wrapper">
          <CustomButton
            data-toggle="modal"
            data-target="#mapDriver"
            btnClass="white-btn"
            disabled={
              memoizedPursuitRequest.current_tr.x_driver === "" ||
              memoizedPursuitRequest.current_tr.x_driver === null
                ? true
                : false
            }
          >
            <div className="d-flex justify-content-around">
              <div>
                <img style={{ width: "18px" }} alt="map" src={mapWatch} />
              </div>
              <div>مشاهده نقشه</div>
            </div>
          </CustomButton>
        </div>
        <div className="btn-wrapper">
          <CustomButton onClick={handleRefreshApi} btnClass="white-btn">
            <div className="d-flex justify-content-around">
              <div>
                <img style={{ width: "18px" }} alt="refresh" src={refresh} />
              </div>
              <div>بازسازی صفحه</div>
            </div>
          </CustomButton>
        </div>
        <Modal
          closeBtnName="بستن"
          confirmColorBtnClass="btn-warning active"
          Id="mapDriver"
          confirmBtnName="ثبت"
          noCloseTag
          noConfirm
          title="مشاهده راننده"
          CustomWidth="800px"
        >
          <div className="g-map">
            <GoogleMap newMapStyle={handleResizeMap} />
          </div>
        </Modal>
      </div>
    );
  };
  /* -------------------------------------------------------------------------------- */
  if (memoizedPursuitRequest !== null) {
    const {
      current_tr,
      step_0,
      step_1,
      step_2,
      step_3To4,
      step_5,
      step_6To10,
      step_11,
      step_12To16,
    } = memoizedPursuitRequest;
    return (
      <div ref={pursuitRequsetRef} className="pursuit-requests-wrapper">
        <h5>پیگیری درخواست</h5>
        {/*-------------------------- confirm new request --------------------------*/}
        <Expantion
          step={[0]}
          successMode={
            [0].includes(Number(current_tr.step_id)) ||
            Number(current_tr.step_id) > 0
          }
          waitingMode={[0].includes(Number(current_tr.step_id_wait))}
          title={step_0.header}
        >
          <ConfirmNewRequest pursuitRequest={memoizedPursuitRequest} />
        </Expantion>
        {/*-------------------------------- Operators ------------------------------*/}
        <Expantion
          step={[1]}
          lock={Number(current_tr.step_id) === 0}
          successMode={
            [1].includes(Number(current_tr.step_id)) ||
            Number(current_tr.step_id) > 1
          }
          driverMode={current_tr.step_id === "30" ? true : false}
          waitingMode={[1].includes(Number(current_tr.step_id_wait))}
          title={step_1.header}
        >
          <OperatorsPursuitRequest pursuitRequest={memoizedPursuitRequest} />
        </Expantion>
        {/* ------------------------------ choose driver -------------------------- */}
        <Expantion
          successMode={
            ([2].includes(Number(current_tr.step_id)) ||
              Number(current_tr.step_id) > 2) &&
            current_tr.step_id !== "30"
          }
          lock={
            Number(current_tr.step_id) < 2 || Number(current_tr.step_id) === 30
          }
          step={[2]}
          waitingMode={[2].includes(Number(current_tr.step_id_wait))}
          title={step_2 && step_2.header}
        >
          <DriverChoosesPursuitRequest
            pursuitRequest={memoizedPursuitRequest}
          />
        </Expantion>
        {/* --------------------------- waiting Loading permit --------------------- */}
        <Expantion
          successMode={
            ([3, 4].includes(Number(current_tr.step_id)) ||
              Number(current_tr.step_id) > 4) &&
            current_tr.step_id !== "30"
          }
          lock={
            Number(current_tr.step_id) < 4 || Number(current_tr.step_id) === 30
          }
          step={[3, 4]}
          waitingMode={[3, 4].includes(Number(current_tr.step_id_wait))}
          title={step_3To4.header}
        >
          <PorteragePursuitRequest pursuitRequest={memoizedPursuitRequest} />
        </Expantion>
        {/*--------------------------- beginning locations ------------------------- */}
        <Expantion
          successMode={
            ([5].includes(Number(current_tr.step_id)) ||
              Number(current_tr.step_id) > 5) &&
            current_tr.step_id !== "30"
          }
          step={[5]}
          waitingMode={[5].includes(Number(current_tr.step_id_wait))}
          title={step_5.header}
        >
          <BeginningLocationsPursuit pursuitRequest={memoizedPursuitRequest} />
        </Expantion>
        {/*------------------------------- Loading Bar ------------------------------ */}
        <Expantion
          successMode={
            ([6, 7, 8, 9, 10].includes(Number(current_tr.step_id)) ||
              Number(current_tr.step_id) > 10) &&
            current_tr.step_id !== "30"
          }
          step={[6, 7, 8, 9, 10]}
          waitingMode={[6, 7, 8, 9, 10].includes(
            Number(current_tr.step_id_wait)
          )}
          title={step_6To10.header}
        >
          <LoadingPursuit pursuitRequest={memoizedPursuitRequest} />
        </Expantion>
        {/*------------------------------ destinations bar ---------------------------*/}
        <Expantion
          step={[11]}
          successMode={
            ([11].includes(Number(current_tr.step_id)) ||
              Number(current_tr.step_id) > 11) &&
            current_tr.step_id !== "30"
          }
          waitingMode={[11].includes(Number(current_tr.step_id_wait))}
          title={step_11.header}
        >
          <DestinationLocationsPusrsuit
            pursuitRequest={memoizedPursuitRequest}
          />
        </Expantion>
        {/* ----------------------------- deliver payload ---------------------------- */}
        <Expantion
          successMode={
            ([12, 13, 14, 15, 16].includes(Number(current_tr.step_id)) ||
              Number(current_tr.step_id) > 16) &&
            current_tr.step_id !== "30"
          }
          step={[
            12,
            13,
            14,
            15,
            16,
            Number(current_tr.finished) === 0 ? 100 : 11,
          ]}
          waitingMode={[12, 13, 14, 15, 16].includes(
            Number(current_tr.step_id_wait)
          )}
          title={step_12To16.header}
        >
          <DeliveresPayloads pursuitRequest={memoizedPursuitRequest} />
        </Expantion>
        {renderMapAndReload()}
      </div>
    );
  } else {
    return "";
  }
};

export default ComponentWrapper(PursuitRequest);
