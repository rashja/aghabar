import React, { Component } from "react";

const stars = [
  {
    id: "1",
    empty: require("../assets/images/star_0.png"),
    full: require("../assets/images/star_5.png"),
  },
  {
    id: "2",
    empty: require("../assets/images/star_0.png"),
    full: require("../assets/images/star_5.png"),
  },
  {
    id: "3",
    empty: require("../assets/images/star_0.png"),
    full: require("../assets/images/star_5.png"),
  },
  {
    id: "4",
    empty: require("../assets/images/star_0.png"),
    full: require("../assets/images/star_5.png"),
  },
  {
    id: "5",
    empty: require("../assets/images/star_0.png"),
    full: require("../assets/images/star_5.png"),
  },
];
class Rating extends Component {
  state = {
    rateIndex: this.props.count !== undefined ? String(this.props.count) : "0",
  };
  componentDidUpdate(prevProps, prevState) {
    if (prevState.rateIndex !== this.state.rateIndex) {
      this.props.choosedRate(this.state.rateIndex);
    }
  }

  render() {
    return (
      <div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            margin: "0 auto",
            direction: "ltr",
          }}
        >
          {stars.map((star) => (
            <div
              key={star.id}
              onClick={
                this.props.disable
                  ? null
                  : () => this.setState({ rateIndex: star.id })
              }
            >
              <img
                className="rating-star"
                src={star.id <= this.state.rateIndex ? star.full : star.empty}
              />
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default Rating;
