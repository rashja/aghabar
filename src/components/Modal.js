import React from "react";

const closeBtnStyle = {
  width: "145px",
  height: "44px",
  margin: "5px",
  borderRadius: "20px",
  boxShadow: "0 3px 6px 0 rgba(0, 0, 0, 0.3)",
  color: "black",
  border: "0",
  cursor: "pointer",
};
const Modal = (props) => {
  //props
  //title ===> set modal title
  //noClose ===> no close button in footer
  //closeBtnName ===> name for close button
  //noConfirm ===> no confirm button in footer
  //confirmBtnName ===> name for confirm button
  // noCloseTag ===> no close tag  for modal
  //confirmColorBtnClass ===> class for close button
  //id ===> for specific modal
  return (
    <>
      <div
        className="modal fade"
        id={props.Id}
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div
          className="modal-dialog modal-dialog-centered"
          style={props.CustomWidth ? { maxWidth: props.CustomWidth } : null}
          role="document"
        >
          <div
            className={[
              "modal-content",
              props.customClass ? props.customClass : "",
            ].join(" ")}
          >
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                {props.title}
              </h5>
              {props.noCloseTag ? (
                ""
              ) : (
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              )}
            </div>
            <div className="modal-body">{props.children}</div>
            <div className="modal-footer">
              {props.noClose ? (
                ""
              ) : (
                <button
                  onClick={props.handleClose}
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                  className="btn btn-def bg-secondary"
                  style={closeBtnStyle}
                >
                  {props.closeBtnName}
                </button>
              )}
              {props.noConfirm ? (
                ""
              ) : (
                <button
                  onClick={props.handleConfirm}
                  type="button"
                  data-dismiss="modal"
                  className={[
                    "btn",
                    "btn-df",
                    props.confirmColorBtnClass
                      ? props.confirmColorBtnClass
                      : "btn-primary",
                  ].join(" ")}
                >
                  {props.confirmBtnName}
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Modal;
