import React from "react";
import arrow from "../assets/images/left-arrow.png";
import warning from "../assets/images/warning.png";
import CustomButton from "./CustomButton";
import { sendPursuitRequest } from "../actions/ordersList";
import { useDispatch } from "react-redux";
import useGenerator from "../customHooks/useGenerator";

const CurrentOrderItem = ({ item }) => {
  /*------------------------------ generator --------------------------- */
  const generator = useGenerator();
  /*----------------------------- dispatch & selector ------------------------- */
  const dispatch = useDispatch();
  const {
    tr_id,
    title_bar,
    province_title_s,
    province_title_r,
    city_title_s,
    truck_title,
    header,
    step_id,
    city_title_r,
  } = item;
  /*----------------------------- pursuit request -------------------------- */
  const pursuitRequest = () => {
    dispatch(
      sendPursuitRequest(
        generator.time,
        generator.token(),
        generator.userId,
        tr_id
      )
    );
  };
  /*------------------------------------------------------------------------ */
  return (
    <div className="col-12 col-lg-6">
      <div className="current-order-item-wrapper">
        <div className="current-order-item-title">{title_bar}</div>
        <div className="d-column">
          <div className="current-order-item-location-wrapper">
            <div className="d-column">
              <div className="h6 mb-2">از</div>
              <div className="current-order-item-location">{`${province_title_s} ,${city_title_s}`}</div>
            </div>
            <div>
              <img style={{ width: "30px" }} alt="arrow" src={arrow} />
            </div>
            <div>
              <div className="h6 mb-2 d-column">به</div>
              <div className="current-order-item-location">{`${province_title_r} ,${city_title_r}`}</div>
            </div>
          </div>
          <div className="d-flex">
            <div className="h6">نوع خودرو :</div>
            <div className="h6">{truck_title}</div>
          </div>
          <div className="current-order-item-warning">
            <div>
              <img
                style={{ width: "20px", height: "20px" }}
                alt="warning"
                src={warning}
              />
            </div>
            <div>{header}</div>
          </div>
          <div className="btns-wrapper">
            <div
              className={step_id === "30" ? "btn-wrapper" : "btn-wrapper w-75"}
            >
              <CustomButton onClick={pursuitRequest} btnClass="black-btn">
                پیگیری درخواست
              </CustomButton>
            </div>
            {step_id === "30" && (
              <div className="btn-wrapper">
                <CustomButton btnClass="black-btn">لیست رانندگان</CustomButton>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default CurrentOrderItem;
