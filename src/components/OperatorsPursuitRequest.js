import React, { useRef, useEffect } from "react";
import CustomButton from "./CustomButton";
import { useSelector } from "react-redux";
import DriverInfo from "./DriverInfo";
import Modal from "./Modal";

export default function OperatorsPursuitRequest({ pursuitRequest }) {
  const { drivers_list, date, clock } = pursuitRequest.step_1;
  /* --------------------------------- refs ----------------------------- */
  const driverModalBtn = useRef(null);
  /* ------------------------- dispatch & selector ---------------------- */
  const stateSelector = useSelector(
    ({ pursuitRequest: { driverPickedMode } }) => ({
      driverPickedMode,
    })
  );
  const { driverPickedMode } = stateSelector;
  /* ---------------------------- side eefects -------------------------- */
  useEffect(() => {
    if (driverPickedMode) {
      if (driverModalBtn.current) {
        driverModalBtn.current.click();
      }
    }
  }, [driverPickedMode]);
  /* -------------------------- render driver list ---------------------- */
  const renderDriversList = () => {
    return (
      <div>
        <h6 className="title-header">
          لطفا راننده مورد نظر خود را انتخاب کنید
        </h6>
        <div className="render-driver-list-wrapper">
          {drivers_list.length > 0 &&
            drivers_list.map((driver) => <DriverInfo driver={driver} />)}
        </div>
      </div>
    );
  };
  /* ------------------------------------------------------------------- */
  return (
    <div className="render-confirm-request-wrapper">
      <div className="render-confirm-request-wrapper-item">
        <b>تاریخ تایید سامانه :</b>
        <b className="pr-1">{date}</b>
      </div>
      <div className="render-confirm-request-wrapper-item">
        <b>ساعت تایید سامانه :</b>
        <b className="pr-1">{clock}</b>
      </div>
      {pursuitRequest.current_tr.step_id === "30" && (
        <div className="render-confirm-request-driver-btn">
          <button
            ref={driverModalBtn}
            data-toggle="modal"
            data-target="#driversList"
          >
            انتخاب راننده
          </button>
        </div>
      )}
      <Modal
        closeBtnName="بستن"
        confirmColorBtnClass="btn-warning active"
        Id="driversList"
        confirmBtnName="ثبت"
        noCloseTag
        title="لیست رانندگان"
        CustomWidth="600px"
      >
        {renderDriversList()}
      </Modal>
    </div>
  );
}
