import React from "react";

export default function BeginningLocationsPursuit({ pursuitRequest }) {
  const { loading_mode, count_addresses } = pursuitRequest.step_5;
  return (
    <div className="render-beginning-locations-wrapper">
      <div className="render-beginning-locations-item">
        <b>حالت های بارگیری :</b>
        <b>{loading_mode.join("-")}</b>
      </div>
      <b className="render-beginning-locations-item">
        لیست آدرس مکان های بارگیری :
      </b>
      <div className="addresses-wrapper">
        {count_addresses.map((adresses, index) => {
          return (
            <div key={index} className="addresses-locations-item">
              <div>
                <div className="h6">{`مبدا شماره ${index + 1}`}</div>
                <b>نام :</b>
                <b className="pr-1">{adresses.name}</b>
              </div>
              <div>
                <b>موبایل :</b>
                <b className="pr-1">{adresses.mobile}</b>
              </div>
              <div>
                <b>توضیحات :</b>
                <b className="pr-1" style={{ wordBreak: "break-all" }}>
                  {adresses.des.length > 0 ? adresses.des : ". . . "}
                </b>
              </div>
              <div>
                <b>آدرس :</b>
                <b className="pr-1">{`${adresses.address} ${
                  adresses.unit.length > 0 ? ", واحد " + adresses.unit : ""
                } ${
                  adresses.plaque.length > 0 ? ", پلاک " + adresses.plaque : ""
                }`}</b>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
