import React from "react";
import CustomButton from "./CustomButton";
import { useDispatch } from "react-redux";
import { getFinishWork } from "../actions/externalTransactions";
import Modal from "./Modal";
import useGenerator from "../customHooks/useGenerator";

export default function ExternalTransaction({ exTransaction }) {
  /*------------------------------ generator --------------------------- */
  const generator = useGenerator();
  /*------------------------- dispatch & selector ----------------------- */
  const dispatch = useDispatch();
  const {
    tr_id,
    fname_driver,
    lname_driver,
    phone_driver,
    truck_title,
    title_bar,
    bar_class,
    cost,
    des_insurance,
    type_pay,
    weight,
    height,
    width,
    package_type,
    loading_mode,
    count_address_s,
    count_address_r,
    status_tr,
  } = exTransaction;
  /*---------------------------- finish work ---------------------------- */
  const handleFinishWork = () => {
    dispatch(
      getFinishWork(
        generator.token("setFinishedTrOutOfSystem"),
        generator.time,
        generator.userId,
        tr_id
      )
    );
  };
  /*----------------------------- driver Info ------------------------- */
  const renderDriverInfo = () => {
    return (
      <div>
        <div className="detail-title-item mb-2">
          <b>اطلاعات راننده</b>
        </div>
        <div className="d-flex">
          <p>نام راننده :</p>
          <p className="ml-1">{fname_driver}</p>
          <p>{lname_driver}</p>
        </div>
        <div className="d-flex">
          <p>شماره موبایل راننده :</p>
          <p>{phone_driver}</p>
        </div>
        <div className="d-flex">
          <p>نوع و برند کامیون :</p>
          <p>{truck_title}</p>
        </div>
      </div>
    );
  };
  /*----------------------------- bar Info ------------------------- */
  const renderBarInfo = () => {
    return (
      <div>
        <div className="detail-title-item mb-2">
          <b>اطلاعات بار</b>
        </div>
        <div className="d-flex">
          <p>نام محموله :</p>
          <p>{title_bar}</p>
        </div>
        <div className="d-flex">
          <p>کلاسه بار :</p>
          <p>{bar_class}</p>
        </div>
        <div className="d-flex">
          <p>قیمت حمل بار وهزینه صدور بارنامه :</p>
          <p>{cost}</p>
        </div>
        <div className="d-flex">
          <p>بیمه :</p>
          <p>{des_insurance}</p>
        </div>
        <div className="d-flex">
          <p>نوع پرداخت :</p>
          <p>{type_pay === "1" ? "پیش کرایه" : "پس کرایه"}</p>
        </div>
        <div className="d-flex">
          <p>وزن تقریبی بار :</p>
          <p>{weight}</p>
        </div>
        <div className="d-flex">
          <p>طول و عرض بار :</p>
          <p>{height * width}</p>
        </div>
        <div className="d-flex">
          <p>نوع بسته بندی :</p>
          <p>{package_type}</p>
        </div>
        <div className="d-flex">
          <p>حالت بارگیری :</p>
          {loading_mode.map((loading) => (
            <span className="ml-1">{loading}</span>
          ))}
        </div>
      </div>
    );
  };
  /*----------------------------- beginning Info ------------------------- */
  const renderBeginning = () => {
    return (
      <div className="detail-directions-wrapper mb-3">
        <div className="mb-2">
          <b>لیست آدرس های مبدا</b>
        </div>
        {count_address_s.map((add, index) => (
          <div className="detail-directions-item" key={index}>
            <div className="d-flex">
              <b>{`مبدا ${index + 1} :`}</b>
              <p>{`${add.province} ${add.city} ${add.address} ${add.plaque} ${add.unit}`}</p>
            </div>
            <div className="d-flex">
              <p>دریافت کننده :</p>
              <p>{add.name}</p>
            </div>
            <div className="d-flex">
              <p>شماره موبایل :</p>
              <p>{add.mobile}</p>
            </div>
          </div>
        ))}
      </div>
    );
  };
  /*----------------------------- destination Info ------------------------- */
  const renderDestination = () => {
    return (
      <div className="detail-directions-wrapper">
        <div className="mb-2">
          <b>لیست آدرس های مقصد</b>
        </div>
        {count_address_r.map((add, index) => (
          <div className="detail-directions-item" key={index}>
            <div className="d-flex">
              <b>{`مقصد ${index + 1} :`}</b>
              <p>{`${add.province} ${add.city} ${add.address} ${add.plaque} ${add.unit}`}</p>
            </div>
            <div className="d-flex">
              <p>دریافت کننده :</p>
              <p>{add.name}</p>
            </div>
            <div className="d-flex">
              <p>شماره موبایل :</p>
              <p>{add.mobile}</p>
            </div>
          </div>
        ))}
      </div>
    );
  };
  /*----------------------------- Information ------------------------- */
  const renderInformations = () => {
    return (
      <div className="details-order-wrapper">
        <div>
          {renderDriverInfo()}
          {renderBarInfo()}
        </div>
        <div className="directions-wrapper">
          {renderBeginning()}
          {renderDestination()}
        </div>
      </div>
    );
  };
  /*------------------------------------------------------------------- */
  return (
    <div className="col-12 col-lg-6 d-column">
      <div className="single-external-transaction-container">
        <div className="single-external-transaction-title">{bar_class}</div>
        <div className="single-external-transaction-cost">
          <div className="text-warning">{cost}</div>
          <div className="mr-2">تومان</div>
        </div>
        <div className="single-external-transaction-vehicle-type">
          <div>نوع خودرو :</div>
          <div>{truck_title}</div>
        </div>
        <div className="single-external-transaction-status">
          {(status_tr === 0 && (
            <div className="mb-2 mt-2 text-danger justify-content">حذف شده</div>
          )) ||
            (status_tr === 1 && (
              <CustomButton onClick={handleFinishWork} btnClass="yellow-btn">
                پایان کار
              </CustomButton>
            )) ||
            (status_tr === 2 && (
              <div className="mb-2 mt-2 text-success justify-content">
                به اتمام رسیده
              </div>
            ))}
        </div>
        <div className="single-external-transaction-btns-container">
          <CustomButton
            data-toggle="modal"
            data-target="#informations"
            btnClass="black-btn"
          >
            جزئیات سفارش
          </CustomButton>
        </div>
      </div>
      <Modal
        confirmColorBtnClass="btn-warning active"
        Id="informations"
        noClose
        confirmBtnName="بستن"
        noCloseTag
        title="جزئیات سفارش"
        customClass="modal-width"
      >
        {renderInformations()}
      </Modal>
    </div>
  );
}
