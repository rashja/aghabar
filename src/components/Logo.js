import React from "react";
import logo from "../assets/images/logo.png";

export default function Logo(props) {
  return (
    <div>
      <img
        style={{ width: `${props.width}`, height: `${props.height}` }}
        src={logo}
        alt="logo"
      />
    </div>
  );
}
