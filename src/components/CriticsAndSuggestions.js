import React, { useRef } from "react";
import { useDispatch } from "react-redux";
import ComponentCover from "../HOC/ComponentCover";
import CustomButton from "./CustomButton";
import { showToastify } from "../actions/loading";
import { requestSetComments } from "../actions/criticsAndSuggestions";
import useGenerator from "../customHooks/useGenerator";

function CriticsAndSuggestions() {
  /*------------------------------ generator --------------------------- */
  const generator = useGenerator();
  /*--------------------------------- refs ----------------------------- */
  const titleInputRef = useRef();
  const contextTextAreaRef = useRef();
  /*------------------------------- dispatch --------------------------- */
  const dispatch = useDispatch();
  /*---------------------------------- submit -------------------------- */
  const handleSubmit = (e) => {
    e.preventDefault();
    if (titleInputRef.current && contextTextAreaRef.current) {
      if (
        titleInputRef.current.value.length > 0 &&
        contextTextAreaRef.current.value.length > 0
      ) {
        dispatch(
          requestSetComments(
            generator.time,
            generator.token("setComments"),
            generator.userId,
            titleInputRef.current.value,
            contextTextAreaRef.current.value
          )
        );
      } else {
        dispatch(showToastify("لطفا تمامی فیلد ها را پر کنید.", "error"));
      }
    }
  };
  /*-------------------------------------------------------------------- */
  return (
    <div className="critics-suggestions-wrapper">
      <div className="h6 mb-4">
        نظرات و پیشنهادات خود را برای ما ارسال کنید.
      </div>
      <form className="critics-suggestions-form" onSubmit={handleSubmit}>
        <input
          placeholder="تیتر خود را وارد کنید "
          className="input"
          ref={titleInputRef}
        />
        <textarea placeholder="توضیحات ..." ref={contextTextAreaRef} />
        <div className="w-50">
          <CustomButton btnClass="black-btn" type="submit">
            ارسال
          </CustomButton>
        </div>
      </form>
    </div>
  );
}

export default ComponentCover(CriticsAndSuggestions);
