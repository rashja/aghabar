import React from 'react'
import loading from '../assets/images/loading.gif'


export default function Loading() {
    return (
        <div class="load-wrapp">
        <div class="load-3">
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
        </div>
      </div>
    )
}
