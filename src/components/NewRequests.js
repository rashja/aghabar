import React, { useRef, useState, useEffect, useCallback } from "react";
import ComponentWrapper from "../HOC/ComponentCover";
import GoogleMap from "./GoogleMap";
import Modal from "./Modal";
import { useSelector, useDispatch } from "react-redux";
import {
  getCoordinate,
  deleteLocationFromMap,
  getCityCoordinate,
  sendMultipleCoordinates,
  sendExtraAddressesInfo,
  setAreaCoordinate,
} from "../actions/newRequests";
import userAddress from "../assets/images/userAddress.png";
import phoneAddress from "../assets/images/phoneAddress.png";
import explainAddress from "../assets/images/explainAddress.png";
import editAddress from "../assets/images/editAddress.png";
import mapAddress from "../assets/images/mapAddress.png";
import { showToastify } from "../actions/loading";
import search from "../assets/images/search.png";
import CustomButton from "./CustomButton";

function NewRequests({ history }) {
  /*------------------------------------- refs ------------------------------ */
  const btnModalOpenRef = useRef(null);
  const inputNameRef = useRef(null);
  const inputMobileRef = useRef(null);
  const inputAddressRef = useRef(null);
  const inputUnitRef = useRef(null);
  const inputPlaqueRef = useRef(null);
  const inputExplanationRef = useRef(null);
  /*----------------------------- dispatch & selector ----------------------- */
  const dispatch = useDispatch();
  const stateSelector = useSelector(
    ({
      ordersList: { baseInfo, resendRequestData },
      newRequests: {
        multipleCoordinates,
        areaCoordinate,
        newRequestsExtraInfoes,
      },
    }) => ({
      baseInfo,
      multipleCoordinates,
      areaCoordinate,
      newRequestsExtraInfoes,
      resendRequestData,
    })
  );
  const { provinces, city } = stateSelector.baseInfo;
  const { resendRequestData } = stateSelector;
  const {
    multipleCoordinates,
    areaCoordinate,
    newRequestsExtraInfoes,
  } = stateSelector;
  /*----------------------------------- states ------------------------------ */
  const initialStates = {
    newCoordinate: null,
    beginning: true,
    destination: false,
    beginningCount: 0,
    destinationsCount: 0,
    beginningDelete: [],
    destinationDelete: [],
    beginningTargets: 0,
    destinationTargets: 0,
    searchProvince: "",
    pickedProvince: "",
    pickedProvinceId: "",
    searchCity: "",
    pickedCity: "",
    pickedCityId: "",
  };
  const [state, setState] = useState(initialStates);
  /*--------------------------------- side Effects -------------------------- */
  useEffect(() => {
    if (newRequestsExtraInfoes !== null) {
      setState({
        ...state,
        newCoordinate: newRequestsExtraInfoes.newCoordinate,
        beginning: newRequestsExtraInfoes.beginning,
        destination: newRequestsExtraInfoes.destination,
        beginningCount: newRequestsExtraInfoes.beginningCount,
        destinationsCount: newRequestsExtraInfoes.destinationsCount,
        beginningDelete: newRequestsExtraInfoes.beginningDelete,
        destinationDelete: newRequestsExtraInfoes.destinationDelete,
        beginningTargets: newRequestsExtraInfoes.beginningTargets,
        destinationTargets: newRequestsExtraInfoes.destinationTargets,
        searchProvince: newRequestsExtraInfoes.searchProvince,
        pickedProvince: newRequestsExtraInfoes.pickedProvince,
        pickedProvinceId: newRequestsExtraInfoes.pickedProvinceId,
        searchCity: newRequestsExtraInfoes.searchCity,
        pickedCity: newRequestsExtraInfoes.pickedCity,
        pickedCityId: newRequestsExtraInfoes.pickedCityId,
      });
    }
    if (resendRequestData !== null && multipleCoordinates.length === 0) {
      history.push("/");
    }
  }, []);

  useEffect(() => {
    if (resendRequestData !== null) {
      const beg = resendRequestData.addresses.filter(
        (item) => item.type === "1"
      );
      const dest = resendRequestData.addresses.filter(
        (item) => item.type === "2"
      );

      setState({
        ...state,
        pickedCityId:
          resendRequestData.addresses[resendRequestData.addresses.length - 1]
            .city_id,
        pickedCity:
          resendRequestData.addresses[resendRequestData.addresses.length - 1]
            .city_title,
        pickedProvince:
          resendRequestData.addresses[resendRequestData.addresses.length - 1]
            .province_title,
        pickedProvinceId:
          resendRequestData.addresses[resendRequestData.addresses.length - 1]
            .province_id,
        beginningCount: beg.length,
        beginningTargets: beg.length,
        destinationsCount: dest.length,
        destinationTargets: dest.length,
      });
      dispatch(
        getCityCoordinate(
          resendRequestData.addresses[resendRequestData.addresses.length - 1]
            .province_title,
          resendRequestData.addresses[resendRequestData.addresses.length - 1]
            .city_title
        )
      );
    }
  }, [resendRequestData]);
  /*----------------------------------- headers ----------------------------- */
  const renderHeaders = () => {
    return (
      <>
        <h3 className="title-header">ثبت بار جدید</h3>
        <h5 className="title-header">لطفا مبدأ و مقصد را انتخاب کنید</h5>
      </>
    );
  };
  /*--------------------- more detail of location marker -------------------- */
  //choose mode
  const handleChooseMode = (mode) => {
    if (mode === "B") {
      setState({
        ...state,
        beginning: true,
        destination: false,
      });
    } else if (mode === "D") {
      setState({
        ...state,
        beginning: false,
        destination: true,
      });
    }
  };
  //clear inputs
  const handleClearInputs = () => {
    inputNameRef.current.value = "";
    inputMobileRef.current.value = "";
    inputAddressRef.current.value = "";
    inputUnitRef.current.value = "";
    inputExplanationRef.current.value = "";
    inputPlaqueRef.current.value = "";
  };
  //new confirm address
  const handleConfirmNewAddress = () => {
    if (
      inputNameRef.current.value.length > 0 &&
      inputMobileRef.current.value.length > 0 &&
      inputPlaqueRef.current.value.length > 0 &&
      inputAddressRef.current.value.length > 0
    ) {
      if (state.beginning) {
        if (state.beginningTargets < 5) {
          if (state.beginningDelete.length !== 0) {
            const newBeginning = {
              x: state.newCoordinate.lat,
              y: state.newCoordinate.lng,
              text: `مبدا ${Math.min(...state.beginningDelete)}`,
              type: 1, //type 0 means beginning
              priority: Math.min(...state.beginningDelete),
              name: inputNameRef.current.value,
              mobile: inputMobileRef.current.value,
              address: inputAddressRef.current.value,
              unit: inputUnitRef.current.value,
              des: inputExplanationRef.current.value,
              plaque: inputPlaqueRef.current.value,
              city_id: state.pickedCityId,
              province_id: state.pickedProvinceId,
            };
            setState({
              ...state,
              beginningCount:
                state.beginningDelete.length === 1
                  ? state.beginningTargets + 1
                  : Math.min(...state.beginningDelete),
              beginningDelete: state.beginningDelete.filter(
                (i) => i !== String(Math.min(...state.beginningDelete))
              ),
              beginningTargets: state.beginningTargets + 1,
            });
            dispatch(getCoordinate(newBeginning));
            handleClearInputs();
          } else {
            const newBeginning = {
              x: state.newCoordinate.lat,
              y: state.newCoordinate.lng,
              text: `مبدا ${state.beginningCount + 1}`,
              type: 1, //type 0 means beginning,
              priority: state.beginningCount + 1,
              name: inputNameRef.current.value,
              mobile: inputMobileRef.current.value,
              address: inputAddressRef.current.value,
              unit: inputUnitRef.current.value,
              des: inputExplanationRef.current.value,
              plaque: inputPlaqueRef.current.value,
              city_id: state.pickedCityId,
              province_id: state.pickedProvinceId,
            };
            setState({
              ...state,
              beginningCount: state.beginningCount + 1,
              beginningTargets: state.beginningTargets + 1,
            });
            dispatch(getCoordinate(newBeginning));
            handleClearInputs();
          }
        } else {
          dispatch(
            showToastify("شما مجاز به اتخاب حداکثر 5 مبدا هستید", "warning")
          );
        }
      } else if (state.destination) {
        if (state.destinationTargets < 5) {
          if (state.destinationDelete.length !== 0) {
            const newDestination = {
              x: state.newCoordinate.lat,
              y: state.newCoordinate.lng,
              text: `مقصد ${Math.min(...state.destinationDelete)}`,
              type: 2, //type 1 means beginning,
              priority: Math.min(...state.destinationDelete),
              name: inputNameRef.current.value,
              mobile: inputMobileRef.current.value,
              address: inputAddressRef.current.value,
              unit: inputUnitRef.current.value,
              des: inputExplanationRef.current.value,
              plaque: inputPlaqueRef.current.value,
              city_id: state.pickedCityId,
              province_id: state.pickedProvinceId,
            };
            setState({
              ...state,
              destinationsCount:
                state.destinationDelete.length === 1
                  ? state.destinationTargets + 1
                  : Math.min(...state.destinationDelete),
              destinationDelete: state.destinationDelete.filter(
                (i) => i !== String(Math.min(...state.destinationDelete))
              ),
              destinationTargets: state.destinationTargets + 1,
            });
            dispatch(getCoordinate(newDestination));
            handleClearInputs();
          } else {
            const newDestination = {
              x: state.newCoordinate.lat,
              y: state.newCoordinate.lng,
              text: `مقصد ${state.destinationsCount + 1}`,
              type: 2, //type 1 means destinantion,
              priority: state.destinationsCount + 1,
              name: inputNameRef.current.value,
              mobile: inputMobileRef.current.value,
              address: inputAddressRef.current.value,
              unit: inputUnitRef.current.value,
              des: inputExplanationRef.current.value,
              plaque: inputPlaqueRef.current.value,
              city_id: state.pickedCityId,
              province_id: state.pickedProvinceId,
            };
            setState({
              ...state,
              destinationsCount: state.destinationsCount + 1,
              destinationTargets: state.destinationTargets + 1,
            });
            dispatch(getCoordinate(newDestination));
            handleClearInputs();
          }
        } else {
          dispatch(
            showToastify("شما مجاز به اتخاب حداکثر 5 مقصد هستید", "warning")
          );
        }
      }
    } else {
      dispatch(showToastify("لطفا فرم های اجباری را تکمیل کنید", "warning"));
      handleClearInputs();
    }
  };
  //delete locations
  const handleDeleteLocations = (deleteLocation) => {
    if (Number(deleteLocation.type) === 1) {
      setState({
        ...state,
        beginningDelete: [
          ...state.beginningDelete,
          deleteLocation.text
            ? deleteLocation.text.substring(deleteLocation.text.length - 1)
            : deleteLocation.priority,
        ],
        beginningTargets: state.beginningTargets - 1,
      });
      dispatch(
        deleteLocationFromMap(
          deleteLocation.text ? deleteLocation.text : deleteLocation
        )
      );
    } else if (Number(deleteLocation.type) === 2) {
      setState({
        ...state,
        destinationDelete: [
          ...state.destinationDelete,
          deleteLocation.text
            ? deleteLocation.text.substring(deleteLocation.text.length - 1)
            : deleteLocation.priority,
        ],
        destinationTargets: state.destinationTargets - 1,
      });
      dispatch(
        deleteLocationFromMap(
          deleteLocation.text ? deleteLocation.text : deleteLocation
        )
      );
    }
  };
  //render more details
  const renderMoreDetails = () => {
    return (
      <div>
        <div className="d-flex choose-mode-target">
          <div
            onClick={() => handleChooseMode("B")}
            className={[
              "choose-mode-target-item",
              state.beginning && "target-blue",
            ].join(" ")}
          >
            مبدا
          </div>
          <div
            onClick={() => handleChooseMode("D")}
            className={[
              "choose-mode-target-item",
              state.destination && "target-blue",
            ].join(" ")}
          >
            مقصد
          </div>
        </div>
        <div className="input-more-info-wrapper">
          <div className="input-more-info-icon">
            <img style={{ width: "22px" }} alt="user" src={userAddress} />
          </div>
          <input
            ref={inputNameRef}
            className="input padding-right-input box-black-shadow"
            placeholder="نام و نام خانوادگی *"
            type="text"
          />
        </div>
        <div className="input-more-info-wrapper">
          <div className="input-more-info-icon">
            <img style={{ width: "22px" }} alt="mobile" src={phoneAddress} />
          </div>
          <input
            ref={inputMobileRef}
            className="input padding-right-input box-blue-shadow"
            placeholder="موبایل *"
            type="text"
          />
        </div>
        <div className="input-more-info-wrapper">
          <div className="input-more-info-icon">
            <img style={{ width: "22px" }} alt="address" src={mapAddress} />
          </div>
          <input
            ref={inputAddressRef}
            className="input padding-right-input box-black-shadow"
            placeholder="آدرس *"
            type="text"
          />
        </div>
        <div className="input-more-info-wrapper">
          <div className="input-more-info-icon">
            <img style={{ width: "22px" }} alt="explain" src={explainAddress} />
          </div>
          <input
            ref={inputExplanationRef}
            className="input padding-right-input box-black-shadow"
            placeholder="توضیحات"
            type="text"
          />
        </div>
        <div className="d-flex justify-content-between">
          <div className="input-more-info-wrapper">
            <div className="input-more-info-icon">
              <img style={{ width: "22px" }} alt="plague" src={editAddress} />
            </div>
            <input
              ref={inputPlaqueRef}
              className="input padding-right-input box-black-shadow input-rest-address"
              placeholder="پلاک *"
              type="text"
            />
          </div>
          <div className="input-more-info-wrapper">
            <div className="input-more-info-icon">
              <img style={{ width: "22px" }} alt="unit" src={editAddress} />
            </div>
            <input
              ref={inputUnitRef}
              className="input padding-right-input box-black-shadow input-rest-address"
              placeholder="واحد"
              type="text"
            />
          </div>
        </div>
      </div>
    );
  };
  /*--------------------------------- more details -------------------------- */
  const renderMoreDetailsModal = () => {
    return (
      <>
        <button
          data-toggle="modal"
          data-target="#moreInformations"
          children=""
          data-backdrop="static"
          style={{ display: "none" }}
          ref={btnModalOpenRef}
        />
        <Modal
          confirmColorBtnClass="btn-warning active"
          Id="moreInformations"
          confirmDisable={
            inputNameRef.current &&
            inputNameRef.current.value.length === 0 &&
            inputMobileRef.current &&
            inputMobileRef.current.value.length === 0 &&
            inputPlaqueRef.current &&
            inputPlaqueRef.current.value.length === 0 &&
            inputAddressRef.current &&
            inputAddressRef.current.value.length === 0
          }
          confirmBtnName="ثبت"
          noCloseTag
          handleConfirm={handleConfirmNewAddress}
          handleClose={handleClearInputs}
          closeBtnName="لغو"
          title="جزئیات بیشتر"
          CustomWidth="375px"
        >
          {renderMoreDetails()}
        </Modal>
      </>
    );
  };
  /*---------------------------- city and province ------------------------ */
  //click on province item
  const handleClickOnProvince = (province) => {
    setState({
      ...state,
      pickedProvince: province.title,
      pickedProvinceId: province.id,
      pickedCity: "",
      pickedCityId: "",
    });
    document.getElementById(province.id).style.color = "#e52e71";
    if (state.pickedProvinceId !== "") {
      if (document.getElementById(state.pickedProvinceId)) {
        document.getElementById(state.pickedProvinceId).style.color = "";
      }
    }
  };
  //render province
  const renderProvince = () => {
    const provinceFilter = provinces.filter(
      (p) => p.title.indexOf(state.searchProvince) !== -1
    );
    return (
      <div>
        <div>
          <input
            onChange={(e) =>
              setState({ ...state, searchProvince: e.target.value })
            }
            className="input text-center mb-3"
            type="text"
            placeholder="استان"
          />
        </div>
        <div className="province-city-wrapper">
          {provinceFilter.map((province) => (
            <div
              onClick={() => handleClickOnProvince(province)}
              className="province-city-item justify-content-center pb-2"
              key={province.id}
              id={province.id}
            >
              {province.title}
            </div>
          ))}
        </div>
      </div>
    );
  };
  //handle click on city item
  const handleClickOnCity = (cit) => {
    setState({
      ...state,
      pickedCity: cit.city_title,
      pickedCityId: cit.city_id,
    });
    document.getElementById(cit.city_id).style.color = "#e52e71";
    if (state.pickedProvinceId !== "") {
      if (document.getElementById(state.pickedProvinceId)) {
        document.getElementById(state.pickedProvinceId).style.color = "";
      }
    }
  };
  //render city modal
  const renderCity = () => {
    const cityFilterById = city.filter(
      (c) => c.province_id === state.pickedProvinceId
    );
    const cityFilter =
      cityFilterById.length > 0 &&
      cityFilterById.filter(
        (p) => p.city_title.indexOf(state.searchProvince) !== -1
      );
    return (
      <div>
        <div>
          <input
            onChange={(e) => setState({ ...state, searchCity: e.target.value })}
            className="input text-center mb-3"
            type="text"
            placeholder="شهر"
          />
        </div>
        <div className="province-city-wrapper">
          {cityFilter &&
            cityFilter.map((cit) => (
              <div
                className={[
                  "province-city-item",
                  "justify-content-center",
                  "pb-2",
                  cit.flag === 0 && "text-secondary",
                ].join(" ")}
                key={cit.city_id}
                id={cit.city_id}
                onClick={() => cit.flag === 1 && handleClickOnCity(cit)}
              >
                {cit.city_title}
              </div>
            ))}
        </div>
      </div>
    );
  };
  //handle confirm city
  const handleConfirmCity = () => {
    if (state.pickedProvince !== "" && state.pickedCity !== "") {
      dispatch(getCityCoordinate(state.pickedProvince, state.pickedCity));
    }
  };
  //city and province modal
  const renderProvinceAndCityModal = () => {
    return (
      <>
        <Modal
          noClose
          confirmColorBtnClass="btn-warning active"
          Id="pickProvince"
          confirmBtnName="ثبت"
          noCloseTag
          // handleConfirm={handleConfirmProvince}
          title="انتخاب استان"
          CustomWidth="375px"
        >
          {renderProvince()}
        </Modal>
        <Modal
          noClose
          confirmColorBtnClass="btn-warning active"
          Id={state.pickedProvince === "" ? null : "pickCity"}
          confirmBtnName="ثبت"
          noCloseTag
          handleConfirm={handleConfirmCity}
          title="انتخاب شهر"
          CustomWidth="375px"
        >
          {renderCity()}
        </Modal>
      </>
    );
  };
  //hanlde click on city tab
  const handleClickOnCityTab = () => {
    if (state.pickedProvince === "") {
      dispatch(
        showToastify("ابتدا استان مورد نظر خود را امتحان کنید", "warning")
      );
    }
  };
  //render city and province
  const renderSearchProvinceAndCity = () => {
    return (
      <div className="province-city-wrapper">
        <div
          data-toggle="modal"
          data-target="#pickProvince"
          className="search-area-wrapper"
        >
          {state.pickedProvince === "" && (
            <img
              style={{ width: "16px", height: "16px" }}
              alt="search"
              src={search}
            />
          )}
          <div>
            {state.pickedProvince !== ""
              ? state.pickedProvince
              : "لطفا استان مورد نظر خود را انتخاب کنید"}
          </div>
        </div>
        <div
          onClick={handleClickOnCityTab}
          data-toggle="modal"
          data-target="#pickCity"
          className="search-area-wrapper"
        >
          {state.pickedCity === "" && (
            <img
              style={{ width: "16px", height: "16px" }}
              alt="search"
              src={search}
            />
          )}
          <div>
            {" "}
            {state.pickedCity !== ""
              ? state.pickedCity
              : "لطفا شهر مورد نظر خود را انتخاب کنید"}
          </div>
        </div>
      </div>
    );
  };
  /*---------------------------- confirm addresses ------------------------ */
  const handleConfirmAddresses = () => {
    const beginningNumberArray = multipleCoordinates.map((coordinate) => {
      if (Number(coordinate.type) === 1) {
        if (coordinate.text) {
          return Number(coordinate.text.substring(coordinate.text.length - 1));
        } else {
          return Number(coordinate.priority);
        }
      }
    });
    const destinantionNumberArray = multipleCoordinates.map((coordinate) => {
      if (Number(coordinate.type) === 2) {
        if (coordinate.text) {
          return Number(coordinate.text.substring(coordinate.text.length - 1));
        } else {
          return Number(coordinate.priority);
        }
      }
    });

    if (
      Math.max(...beginningNumberArray.filter(Number)) ===
        state.beginningTargets &&
      Math.max(...destinantionNumberArray.filter(Number)) ===
        state.destinationTargets
    ) {
      dispatch(sendMultipleCoordinates(multipleCoordinates));
      dispatch(sendExtraAddressesInfo(state));
      history.push("/infoBar");
    } else {
      dispatch(
        showToastify("لطفا مبدا و مقصد را به ترتیب کامل کنید", "warning")
      );
    }
  };
  /*----------------------------- click on map event ---------------------- */
  const handlClickOnMapEvent = (coordinate) => {
    if (state.pickedCity !== "" && state.pickedProvince !== "") {
      setState({ ...state, newCoordinate: coordinate });
      dispatch(setAreaCoordinate([coordinate.lng, coordinate.lat]));
      btnModalOpenRef.current && btnModalOpenRef.current.click();
    } else {
      dispatch(
        showToastify("ابتدا استان و شهر مورد نظر را انتخاب کنید.", "warning")
      );
    }
  };
  /*----------------------------------------------------------------------- */
  return (
    <div className="new-request-container">
      {renderHeaders()}
      {renderProvinceAndCityModal()}
      <div className="g-map">
        {renderSearchProvinceAndCity()}
        <GoogleMap
          lat={areaCoordinate !== null && areaCoordinate[1]}
          lng={areaCoordinate !== null && areaCoordinate[0]}
          newMapStyle={{
            height: "450px",
            borderRadius: "20px",
            position: "realative",
          }}
          multipleMarker
          clickOnMapEvent={(coordinate) => handlClickOnMapEvent(coordinate)}
          deleteLocations={(deleteLocation) =>
            handleDeleteLocations(deleteLocation)
          }
        />
        {renderMoreDetailsModal()}
        <div className="confirm-addresses-button-wrapper">
          <CustomButton onClick={handleConfirmAddresses} btnClass="black-btn">
            تأیید آدرس مبدأ و مقصد
          </CustomButton>
        </div>
      </div>
    </div>
  );
}

export default ComponentWrapper(NewRequests);
