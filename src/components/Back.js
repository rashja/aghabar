import React from "react";
import arrowLeft from "../assets/images/arrow-left.svg";
import { withRouter } from "react-router";

const backStyle = {
  display: "flex",
  flexDirection: "row-reverse",
  alignItems: "baseline",
  cursor: "pointer",
};

function Back({ history }) {
  return (
    <div style={backStyle} onClick={() => history.goBack()}>
      <div>
        <img style={{ width: "27px" }} alt="back" src={arrowLeft} />
      </div>
      <h6 className="pl-1">بازگشت</h6>
    </div>
  );
}

export default withRouter(Back);
