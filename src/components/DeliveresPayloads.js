import React from "react";
import moment from "jalali-moment";
import calendar from "../assets/images/calendar.svg";
import checkSquareGray from "../assets/images/check-square.svg";
import checkSquareGreen from "../assets/images/check-square-green.svg";

export default function DeliveresPayloads({ pursuitRequest }) {
  const { count_address, steps } = pursuitRequest.step_12To16;
  var destinationSteps = [];
  for (var i = 0; i < count_address; i++) {
    destinationSteps.push(i);
  }
  return (
    <div className="render-loading-bar-wrapper mb-3">
      {destinationSteps.map((s, index) => (
        <div key={index} className="render-loading-bar-deliver-wrapper">
          <div className="d-flex pr-2 pt-2">
            <img
              alt="square"
              src={steps[index] ? checkSquareGreen : checkSquareGray}
            />
            <div
              className={[
                "p-1",
                "pr-1",
                "font-weight-bold",
                steps[index] ? "" : "text-secondary",
              ].join(" ")}
            >
              دریافت بار
            </div>
          </div>
          <div className="p-1 pr-3">
            {steps[index] ? (
              <div>
                <b className="pl-1">زمان :</b>
                <b>{steps[index].clock}</b>
                <img
                  style={{ width: "20px", margin: "0 3px" }}
                  alt="calendar"
                  src={calendar}
                />
                <b>
                  {moment(steps[0].time_stamp.split(" ")[0], "YYYY/MM/DD")
                    .locale("fa")
                    .format("YYYY/MM/DD")}
                </b>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
      ))}
    </div>
  );
}
