import React, { useEffect, useState } from "react";
import { useDropzone } from "react-dropzone";

const dropzoneContainer = {
  backgroundColor: "#fdfdfd",
  padding: 30,
  border: "3px dotted #989898",
  borderRadius: 20,
  margin: "10px 15px 10px",
  cursor: "pointer",
};
const thumbsContainer = {
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  justifyContent: "center",
  marginTop: 16,
};
const thumb = {
  display: "inline-flex",
  borderRadius: 2,
  border: "1px solid #eaeaea",
  marginBottom: 8,
  marginRight: 8,
  width: 100,
  height: 100,
  padding: 4,
  boxSizing: "border-box",
};
const thumbInner = {
  display: "flex",
  minWidth: 0,
  overflow: "hidden",
};
const img = {
  display: "block",
  width: "auto",
  height: "100%",
};
async function converter(file) {
  return await new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      resolve(reader.result);
    };
  });
}
function getBase64(props, file) {
  return converter(file).then((value) => {
    // props.saveUploadedImages(value);
    return value;
  });
}

function MyDropzone(props) {
  const [files, setFiles] = useState(props.arrayOfUploadedImg || []);
  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/png, image/jpeg, image/jpg",
    onDrop: (acceptedFiles) => {
      const modifyFiles = acceptedFiles.map((file) =>
        Object.assign(file, {
          preview: URL.createObjectURL(file),
          data: getBase64(props, file),
        })
      );
      setFiles(files.concat(modifyFiles));
    },
  });

  useEffect(() => {
    if (files.length > 0) {
      if (props.justOne) {
        setFiles([]);
      }
      const img = document.getElementById("imageProfile");
      img.src = files[0].preview;
      props.getFile(files);
    }
  }, [files]);

  const thumbs = files.map((file) => (
    <div style={thumb} key={file.name}>
      <div style={thumbInner}>
        <img
          id="imageProfile"
          alt="Image preview..."
          src={file.preview}
          style={{ width: "100px", height: "100px", borderRadius: "50px" }}
        />
      </div>
    </div>
  ));
  return (
    <>
      <div {...getRootProps()}>
        <input
          {...getInputProps()}
          style={{ display: "none" }}
          id="inputFile"
          type="file"
        />
      </div>
      <label htmlFor="inputFile">
        <img
          id="imageProfile"
          src={
            props.checkUpdatePic !== "" ? props.updatePic : props.defaultPhoto
          }
          style={{ width: "100px", height: "100px", borderRadius: "50px" }}
          alt="Image preview..."
        />
      </label>
    </>
  );
}

export default MyDropzone;
