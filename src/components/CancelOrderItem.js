import React from "react";
import CustomButton from "./CustomButton";
import { useDispatch } from "react-redux";
import warning from "../assets/images/warning.png";
import { resendRequest } from "../actions/ordersList";
import useGenerator from "../customHooks/useGenerator";

const CancelOrderItem = ({ item }) => {
  /*------------------------------ generator --------------------------- */
  const generator = useGenerator();
  /*----------------------------- dispatch & selector ------------------------- */
  const dispatch = useDispatch();
  const { tr_id, bar_class, weight, title_bar, truck_title, massage } = item;
  /*-------------------------------- resen request ---------------------------- */
  const handleResendRequest = () => {
    dispatch(
      resendRequest(
        generator.token("resendRequest"),
        generator.time,
        generator.userId,
        tr_id
      )
    );
  };
  /*-------------------------------- resen request ---------------------------- */
  return (
    <div className="col-12 col-lg-6">
      <div className="cancel-order-item-wrapper">
        <div className="cancel-order-item-title">{bar_class}</div>
        <div className="d-column">
          <div className="cancel-order-item-bar-info">
            <div>{weight}</div>
            <div>{title_bar}</div>
          </div>
          <div className="cancel-order-item-truck-info">
            <div>نوع خودرو :</div>
            <div>{truck_title}</div>
          </div>
          <div className="cancel-order-item-failed-warning">
            <div>
              <img
                style={{ width: "20px", height: "20px" }}
                src={warning}
                alt="warning"
              />
            </div>
            <div>{massage}</div>
          </div>
          <div className="cancel-order-item-btn-wrapper">
            <CustomButton onClick={handleResendRequest} btnClass="black-btn">
              درخواست مجدد
            </CustomButton>
          </div>
        </div>
      </div>
    </div>
  );
};
export default CancelOrderItem;
