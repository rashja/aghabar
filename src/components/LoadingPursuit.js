import React from "react";
import moment from "jalali-moment";
import calendar from "../assets/images/calendar.svg";
import checkSquareGray from "../assets/images/check-square.svg";
import checkSquareGreen from "../assets/images/check-square-green.svg";

export default function LoadingPursuit({ pursuitRequest }) {
  const {
    weight,
    package_type,
    width,
    length,
    steps,
    count_address,
  } = pursuitRequest.step_6To10;

  var beginningSteps = [];
  for (var i = 0; i < count_address; i++) {
    beginningSteps.push(i);
  }
  return (
    <div className="render-loading-bar-wrapper mb-3">
      <div className="render-loading-bar-item">
        <b>وزن تقریبی بار :</b>
        <b className="pr-1">{weight}</b>
      </div>
      <div className="render-loading-bar-item">
        <b>نوع بسته بندی :</b>
        <b className="pr-1">{package_type}</b>
      </div>
      <div className="render-loading-bar-item">
        <b>طول و عرض بار :</b>
        <b className="pr-1">{width.length > 0 ? `${width} * ${length}` : ""}</b>
      </div>
      {beginningSteps.map((s, index) => (
        <div key={index} className="render-loading-bar-deliver-wrapper">
          <div className="d-flex pr-2 pt-2">
            <img src={steps[index] ? checkSquareGreen : checkSquareGray} />
            <div
              className={[
                "p-1",
                "pr-1",
                "font-weight-bold",
                steps[index] ? "" : "text-secondary",
              ].join(" ")}
            >
              دریافت بار
            </div>
          </div>
          <div className="p-1 pr-3">
            {steps[index] ? (
              <div>
                <b className="pl-1">زمان :</b>
                <b>{steps[index].clock}</b>
                <img
                  style={{ width: "20px", margin: "0 3px" }}
                  alt="calendar"
                  src={calendar}
                />
                <b>
                  {moment(steps[0].time_stamp.split(" ")[0], "YYYY/MM/DD")
                    .locale("fa")
                    .format("YYYY/MM/DD")}
                </b>
              </div>
            ) : (
              ""
            )}
          </div>
        </div>
      ))}
    </div>
  );
}
