import React from "react";
import CustomButton from "./CustomButton";
import profile from "../assets/images/profile.png";
import Rating from "./RatingStar";
import { getPickedDriver } from "../actions/pursuitRequest";
import { useDispatch, useSelector } from "react-redux";
import useGenerator from "../customHooks/useGenerator";

export default function DriverInfo({ driver }) {
  /*------------------------------ generator --------------------------- */
  const generator = useGenerator();
  /* ------------------------ dispatch & selector ---------------------- */
  const dispatch = useDispatch();
  const stateSelector = useSelector(({ ordersList: { pursuitRequest } }) => ({
    pursuitRequest,
  }));
  const { pursuitRequest } = stateSelector;
  /* ----------------------------- pick driver ------------------------- */
  const pickDriver = () => {
    dispatch(
      getPickedDriver(
        generator.time,
        generator.token("confirmDriverFromList"),
        generator.userId,
        pursuitRequest.current_tr.tr_id,
        driver.id
      )
    );
  };
  /* ------------------------------------------------------------------- */
  return (
    <div className="render-confirm-request-wrapper">
      <div className="driver-info-wrapper">
        <div className="driver-info-seprated">
          <div>
            <div>
              <b>نام :</b>
              <b className="pr-1">{driver.fname + " " + driver.lname}</b>
            </div>
            <div>
              <b>برند کامیون :</b>
              <b className="pr-1">{driver.brand}</b>
            </div>
            <div>
              <b>مبلغ کرایه قابل پرداخت :</b>
              <b className="pr-1">{driver.price + " " + "تومان"}</b>
            </div>
          </div>
          <div className="driver-info-rating-image-wrapper">
            <img
              style={{ width: "65px" }}
              src={driver.picture === null ? profile : driver.picture}
            />
            <div className="rating mb-3">
              <Rating disable count={driver.rate} />
            </div>
          </div>
        </div>
        <div className="driver-info-wrapper-btn">
          <CustomButton onClick={pickDriver} btnClass="black-btn">
            انتخاب راننده
          </CustomButton>
        </div>
      </div>
    </div>
  );
}
