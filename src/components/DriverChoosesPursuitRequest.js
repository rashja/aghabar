import React from "react";

export default function DriverChoosesPursuitRequest({ pursuitRequest }) {
  const {
    fname,
    lname,
    phone,
    class_bar,
    truck_title,
    brand_title,
    plaque_4,
    plaque_3,
    plaque_2,
    plaque_1,
  } = pursuitRequest.step_2;
  return (
    <div className="render-confirm-request-wrapper">
      <div className="render-confirm-request-wrapper-item">
        <b>نام راننده :</b>
        <b className="pr-1">{fname + " " + lname}</b>
      </div>
      <div className="render-confirm-request-wrapper-item">
        <b>شماره موبایل راننده :</b>
        <b className="pr-1">{phone}</b>
      </div>
      <div className="render-confirm-request-wrapper-item">
        <b>کلاسه بار :</b>
        <b className="pr-1">{class_bar}</b>
      </div>
      <div className="render-confirm-request-wrapper-item">
        <b>نوع و برند کامیون :</b>
        <b className="pr-1">{truck_title + "/" + brand_title}</b>
      </div>
      <div className="render-confirm-request-wrapper-item">
        <b>شماره پلاک :</b>
        <b className="pr-1">
          {plaque_3 + "-" + plaque_4 + " " + plaque_2 + " " + plaque_1}
        </b>
      </div>
    </div>
  );
}
