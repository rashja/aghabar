import React from "react";
import { ImageUrl } from "../apis/constants";
import Modal from "./Modal";

export default function PorteragePursuitRequest({ pursuitRequest }) {
  const {
    porterage_pic,
    porterage_number,
    porterage_title,
    porterage_phone,
    porterage_address,
  } = pursuitRequest.step_3To4;
  return (
    <div className="render-confirm-request-wrapper">
      <div
        data-toggle="modal"
        data-target="#portrageImg"
        className="render-confirm-request-wrapper-item pointer"
      >
        <img
          style={{ width: "150px" }}
          alt="barname"
          src={`${ImageUrl}porterage/${porterage_pic}`}
        />
      </div>
      <div className="render-confirm-request-wrapper-item">
        <b>شماره بارنامه :</b>
        <b className="pr-1">{porterage_number}</b>
      </div>
      <div className="render-confirm-request-wrapper-item">
        <b>نام باربری :</b>
        <b className="pr-1">{porterage_title}</b>
      </div>
      <div className="render-confirm-request-wrapper-item">
        <b>تلفن باربری :</b>
        <b className="pr-1">{porterage_phone}</b>
      </div>
      <div className="render-confirm-request-wrapper-item">
        <b>آدرس باربری :</b>
        <b className="pr-1">{porterage_address}</b>
      </div>
      <Modal
        closeBtnName="بستن"
        confirmColorBtnClass="btn-warning active"
        Id="portrageImg"
        confirmBtnName="ثبت"
        noCloseTag
        noConfirm
        title="مشاهده بارنامه"
      >
        <img
          style={{ width: "100%" }}
          alt="barname"
          src={`${ImageUrl}porterage/${porterage_pic}`}
        />
      </Modal>
    </div>
  );
}
