import React from "react";
import calendar from "../assets/images/calendar.png";
import clock from "../assets/images/clock.png";
import creditCard from "../assets/images/credit-card.png";
import upBar from "../assets/images/upbar.png";
import downBar from "../assets/images/downbar.png";
import warning from "../assets/images/warning.png";

const FinancialTransactions = ({ item }) => {
  return (
    <div className="transactions-wrapper">
      <div className="transactions-date-price">
        <div className="d-flex align-items-end">
          <div className="p-1">
            <img
              style={{ width: "20px" }}
              alt="bar"
              src={item.stats_payment === 1 ? downBar : upBar}
            />
          </div>
          <div className="text-danger ml-1">{item.price}</div>
          <span className="text-danger ml-1">تومان</span>
        </div>
        <div className="d-flex align-items-end ">
          <div className="pl-2">{item.date}</div>
          <div className="pl-2">
            <img style={{ width: "20px" }} alt="calender" src={calendar} />
          </div>
        </div>
      </div>
      <div className="transactions-price-type-clock">
        <div className="d-flex p-1 align-items-end">
          <div className="ml-2">
            <img style={{ width: "20px" }} alt="credit" src={creditCard} />
          </div>
          <div className="ml-1">نوع پرداخت :</div>
          <div>{item.type_pay}</div>
        </div>
        <div className="d-flex align-items-end">
          <div>{item.clock}</div>
          <div className="mr-2 pl-2">
            <img style={{ width: "20px" }} alt="clock" src={clock} />
          </div>
        </div>
      </div>
      <div className="d-flex justify-content-center p-3">
        <div className="ml-1">
          <img
            style={{ width: "18px", height: "18px" }}
            alt="warning"
            src={warning}
          />
        </div>
        <div className="text-warning h6">{item.description}</div>
      </div>
    </div>
  );
};

export default FinancialTransactions;
