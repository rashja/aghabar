import React, { useMemo, useEffect } from "react";
import ComponentWrapper from "../HOC/ComponentCover";
import { useSelector, useDispatch } from "react-redux";
import { requestExtrenalTransactions } from "../actions/externalTransactions";
import ExternalTransaction from "./ExternalTransaction";
import truck from "../assets/images/truck.png";
import { externalTransactionsList } from "../reducers/selectors";
import useGenerator from "../customHooks/useGenerator";

function ExternalTransactions({ history }) {
  /*------------------------------ generator --------------------------- */
  const generator = useGenerator();
  /*---------------------------- dispatch & selectors ------------------------- */
  const dispatch = useDispatch();
  const stateSelector = useSelector(
    ({ externalTransactions: { finishWorkMode } }) => ({
      finishWorkMode,
    })
  );
  const { finishWorkMode } = stateSelector;
  const exTransList = useSelector(externalTransactionsList);
  /*--------------------------------- memorize ------------------------------- */
  const memoizedExTransList = useMemo(() => exTransList, [exTransList]);
  /*------------------------------- side effects ----------------------------- */
  useEffect(() => {
    dispatch(
      requestExtrenalTransactions(
        generator.time,
        generator.token("getTrsOutOfSystem"),
        generator.userId,
        "0"
      )
    );
  }, []);

  useEffect(() => {
    if (finishWorkMode) {
      dispatch(
        requestExtrenalTransactions(
          generator.time,
          generator.token("getTrsOutOfSystem"),
          generator.userId,
          "0"
        )
      );
    }
  }, [finishWorkMode]);
  /*------------------------------ render truck ---------------------------- */
  const renderTruck = () => {
    return (
      <div className="truck-wrapper d-column">
        <div>
          <img
            style={{ width: "300px" }}
            className="truck-img"
            alt="truck"
            src={truck}
          />
        </div>
        <div className="h5 mt-5">تاکنون سفارش خارج از سیستمی ثبت نشده است.</div>
      </div>
    );
  };
  /*------------------------------------------------------------------------ */
  return (
    <div className="external-transactions-container">
      <h4>تراکنش های خارج از سیستم</h4>
      {memoizedExTransList.length > 0 ? (
        <div className="container">
          <div className="row">
            {memoizedExTransList.map((exTrs, index) => (
              <ExternalTransaction exTransaction={exTrs} key={index} />
            ))}
          </div>
        </div>
      ) : (
        renderTruck()
      )}
    </div>
  );
}

export default ComponentWrapper(ExternalTransactions);
