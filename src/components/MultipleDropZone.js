import React, { useEffect, useState } from "react";
import { useDropzone } from "react-dropzone";

const dropzoneContainer = {
  backgroundColor: "#fdfdfd",
  padding: 30,
  border: "3px dotted #989898",
  borderRadius: 20,
  margin: "10px 15px 10px",
  cursor: "pointer",
};
const thumbsContainer = {
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  justifyContent: "center",
  marginTop: 16,
};
const thumb = {
  display: "inline-flex",
  borderRadius: 2,
  border: "1px solid #eaeaea",
  marginBottom: 8,
  marginRight: 8,
  width: 100,
  height: 100,
  padding: 4,
  boxSizing: "border-box",
};
const thumbInner = {
  display: "flex",
  minWidth: 0,
  overflow: "hidden",
};
const img = {
  display: "block",
  width: "auto",
  height: "100%",
};
async function converter(file) {
  return await new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      resolve(reader.result);
    };
  });
}
function getBase64(props, file) {
  return converter(file).then((value) => {
    // props.saveUploadedImages(value);
    return value;
  });
}

function MultipleDropZone(props) {
  const [files, setFiles] = useState(props.arrayOfUploadedImg || []);
  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/png, image/jpeg, image/jpg",
    onDrop: (acceptedFiles) => {
      const modifyFiles = acceptedFiles.map((file) =>
        Object.assign(file, {
          preview: URL.createObjectURL(file),
          data: getBase64(props, file),
        })
      );
      const lastFiles = files.concat(modifyFiles);
      if (lastFiles.length > 3) {
        setFiles(lastFiles.slice(lastFiles.length - 3));
      } else {
        setFiles(lastFiles);
      }
    },
  });

  useEffect(() => {
    if (files.length > 0) {
      //   const img = document.getElementById("imagesDrop");
      //   img.src = files[0].preview;
      props.getFile(files);
    }
  }, [files]);

  useEffect(() => {
    if (props.deleteImages.length === 0) {
      setFiles([]);
    }
  }, [props.deleteImages]);

  const thumbs = files.slice(0, 3).map((file) => (
    <div style={thumb} key={file.name}>
      <div style={thumbInner}>
        <img
          id="imagesDrop"
          alt="Image preview..."
          src={file.preview}
          style={{ width: "100px", height: "100px" }}
        />
      </div>
    </div>
  ));

  return (
    <>
      <div {...getRootProps()}>
        <input
          {...getInputProps()}
          style={{ display: "none" }}
          id="inputFileImages"
          type="file"
        />
      </div>
      <label
        style={
          files.length === 3
            ? {
                position: "absolute",
                opacity: 0,
                width: "100%",
                cursor: "pointer",
              }
            : { cursor: "pointer" }
        }
        htmlFor="inputFileImages"
      >
        <img
          id="imagesDrop"
          src={props.defaultPhoto}
          style={{ width: "100px", height: "100px" }}
          alt="Image preview..."
        />
      </label>
      {thumbs}
    </>
  );
}

export default MultipleDropZone;
