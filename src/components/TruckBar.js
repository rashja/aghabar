import React from "react";

export default function TruckBar({ item, getTruckBarTitle }) {
  const handleAgreement = () => {
    getTruckBarTitle(item);
  };
  return (
    <div className="d-flex">
      <label className="checkbox-input-container">
        <input onClick={handleAgreement} type="checkbox" />
        <span className="check-mark"></span>
      </label>
      <span className="pr-2">{item.title}</span>
    </div>
  );
}
