import React from "react";

function CustomButton({ children, btnClass, ...rest }) {
  return (
    <div className="custom-btn-wrapper">
      <button
        {...rest}
        className={
          (btnClass === "black-btn" && "black-btn") ||
          (btnClass === "yellow-btn" && "yellow-btn") ||
          (btnClass === "white-btn" && "white-btn")
        }
      >
        {children}
      </button>
    </div>
  );
}

export default CustomButton;
