import React from "react";

export default function DestinationLocationsPusrsuit({ pursuitRequest }) {
  const { count_addresses } = pursuitRequest.step_11;
  return (
    <div className="destination-location-wrapper">
      <b className="destination-location-item">لیست آدرس مکان های تخلیه :</b>
      <div className="addresses-wrapper">
        {count_addresses.map((adresses, index) => {
          return (
            <div key={index} className="addresses-locations-item">
              <div>
                <div className="h6">{`مقصد شماره ${index + 1}`}</div>
                <b>نام :</b>
                <b className="pr-1">{adresses.name}</b>
              </div>
              <div>
                <b>موبایل :</b>
                <b className="pr-1">{adresses.mobile}</b>
              </div>
              <div>
                <b>توضیحات :</b>
                <b className="pr-1">
                  {adresses.des.length > 0 ? adresses.des : ". . . "}
                </b>
              </div>
              <div>
                <b>آدرس :</b>
                <b className="pr-1">{`${adresses.address} ${
                  adresses.unit.length > 0 ? ", واحد " + adresses.unit : ""
                } ${
                  adresses.plaque.length > 0 ? ", پلاک " + adresses.plaque : ""
                }`}</b>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
