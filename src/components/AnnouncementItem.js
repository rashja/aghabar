import React from "react";

export default function AnnouncementItem({ item }) {
  return (
    <div className="announce-item-container">
      <h6>{item.body}</h6>
    </div>
  );
}
