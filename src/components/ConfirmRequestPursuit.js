import React from "react";

export default function ConfirmRequestPursuit({ pursuitRequest }) {
  const {
    bar_title,
    date,
    clock,
    type_pay,
    des_insurance,
  } = pursuitRequest.step_0;
  return (
    <div className="render-confirm-request-wrapper">
      <div className="render-confirm-request-wrapper-item">
        <b>نام محموله :</b>
        <b className="pr-1">{bar_title}</b>
      </div>
      <div className="render-confirm-request-wrapper-item">
        <b>تاریخ ثبت درخواست :</b>
        <b className="pr-1">{date}</b>
      </div>
      <div className="render-confirm-request-wrapper-item">
        <b>ساعت ثبت در خواست :</b>
        <b className="pr-1">{clock}</b>
      </div>{" "}
      <div className="render-confirm-request-wrapper-item">
        <b>نوع پرداخت :</b>
        <b className="pr-1">{type_pay}</b>
      </div>{" "}
      {des_insurance !== "null" && (
        <div className="render-confirm-request-wrapper-item">
          <b>مبلغ بیمه :</b>
          <b className="pr-1">{des_insurance}</b>
          <b className="pr-1">تومان</b>
        </div>
      )}
    </div>
  );
}
