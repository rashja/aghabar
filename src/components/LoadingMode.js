import React, { useState } from "react";

export default function LoadingMode({ item, handleOthersMode, editBar }) {
  /*------------------------------------ states ------------------------------- */
  const [checkedMode, setCheckedMode] = useState(false);
  /*---------------------------------- handle pick ---------------------------- */
  const handlePick = () => {
    setCheckedMode(!checkedMode);
    handleOthersMode(item);
  };
  /*-------------------------------------------------------------------------- */
  return (
    <>
      <label className="checkbox-input-container d-flex">
        <input onClick={handlePick} type="checkbox" checked={checkedMode} />
        <span className="check-mark"></span>
        <span className="pr-2">{item.title}</span>
      </label>{" "}
    </>
  );
}
