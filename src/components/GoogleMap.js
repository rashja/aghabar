import React, { useState } from "react";
import { Map, GoogleApiWrapper, Marker } from "google-maps-react";
import { useSelector } from "react-redux";
import darkMarker from "../assets/images/dark-map-marker.png";
import blueMarker from "../assets/images/blue-map-marker.png";

const mapStyles = {
  width: "100%",
  height: "500px",
  position: "initial",
  borderRadius: "5px",
};

const GoogleMap = (props) => {
  /*---------------------------------- states ----------------------------------- */
  const initialStates = {
    markerLat: 35.6892,
    markerLng: 51.389,
    beginning: true,
    beginningCount: 0,
    destinationsCount: 0,
    destinations: false,
  };
  const [state] = useState(initialStates);
  /*----------------------------- dispatch & selector --------------------------- */
  const stateSelector = useSelector(
    ({ newRequests: { multipleCoordinates } }) => ({
      multipleCoordinates,
    })
  );
  const { multipleCoordinates } = stateSelector;
  /*--------------------------------- click on map ------------------------------ */
  const onMarkerClick = (prop, marker, e) => {
    props.clickOnMapEvent({
      lat: e.latLng.lat().toFixed(6),
      lng: e.latLng.lng().toFixed(6),
    });
  };
  const handleMarkerClick = (destination) => {
    props.deleteLocations(destination);
  };
  /*------------------------------- custom marker ------------------------------ */
  const destinationCustomMarker = {
    url: darkMarker,
    size: new props.google.maps.Size(100, 80),
    scaledSize: new props.google.maps.Size(110, 80),
    labelOrigin: new props.google.maps.Point(30, 32),
  };
  const beginningCustomMarker = {
    url: blueMarker,
    size: new props.google.maps.Size(100, 80),
    scaledSize: new props.google.maps.Size(110, 80),
    labelOrigin: new props.google.maps.Point(36, 32),
  };
  /*------------------------------- multiple marker ------------------------------ */
  if (props.multipleMarker) {
    return (
      <Map
        google={props.google}
        zoom={15}
        style={props.newMapStyle ? props.newMapStyle : mapStyles}
        initialCenter={{ lat: state.markerLat, lng: state.markerLng }}
        center={
          props.lat && props.lng
            ? { lat: props.lat, lng: props.lng }
            : { lat: state.markerLat, lng: state.markerLng }
        }
        onClick={props.disableClick === undefined && onMarkerClick}
      >
        {multipleCoordinates.map((destination, index) => (
          <Marker
            onClick={() => handleMarkerClick(destination)}
            key={index}
            position={{
              lat: Number(destination.x),
              lng: Number(destination.y),
            }}
            icon={
              Number(destination.type) === 1
                ? beginningCustomMarker
                : destinationCustomMarker
            }
            label={{
              fontWeight: "bold",
              fontSize: "16px",
              text: destination.text
                ? destination.text
                : Number(destination.type) === 1
                ? `مبدا ${destination.priority}`
                : `مقصد ${destination.priority}`,
            }}
          />
        ))}
      </Map>
    );
  } else {
    /*------------------------------- show map ------------------------------ */
    return (
      <Map
        google={props.google}
        zoom={15}
        style={props.newMapStyle ? props.newMapStyle : mapStyles}
        initialCenter={
          props.lat && props.lng
            ? { lat: props.lat, lng: props.lng }
            : { lat: state.markerLat, lng: state.markerLng }
        }
        onClick={props.disableClick === undefined && onMarkerClick}
      >
        <Marker
          position={
            props.lat && props.lng
              ? { lat: props.lat, lng: props.lng }
              : { lat: state.markerLat, lng: state.markerLng }
          }
        />
      </Map>
    );
  }
};

export default GoogleApiWrapper({
  apiKey: "AIzaSyCDSJQdkle0dif9YdTznpTI6HPfXQqmT-o",
  language: "fa",
})(GoogleMap);
