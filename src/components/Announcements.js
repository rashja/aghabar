import React, { useEffect, useState, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import ComponentWrapper from "../HOC/ComponentCover";
import { requestAnnouncements } from "../actions/announcements";
import { announcementsList } from "../reducers/selectors";
import AnnouncementItem from "./AnnouncementItem";
import useGenerator from "../customHooks/useGenerator";

function Announcements() {
  /*------------------------------ generator --------------------------- */
  const generator = useGenerator();
  /*-------------------------------- states ------------------------------ */
  const [page, setPage] = useState(1);
  /*-------------------------- dispatch & selector ----------------------- */
  const dispatch = useDispatch();
  const announcementsListSelector = useSelector(announcementsList);
  /*--------------------------------- memo ------------------------------- */
  const memorizedAnnouncementsList = useMemo(() => announcementsListSelector, [
    announcementsListSelector,
  ]);
  /*------------------------------- side effects ------------------------- */
  useEffect(() => {
    dispatch(
      requestAnnouncements(
        generator.time,
        generator.token("getNotifications"),
        generator.userId,
        page
      )
    );
  }, []);

  useEffect(() => {
    document.addEventListener("scroll", function (event) {
      if (
        window.innerHeight + window.scrollY >=
        document.getElementById("HTML").offsetHeight
      ) {
        setPage((page) => page + 1);
      }
    });
  }, ["pagination"]);

  useEffect(() => {
    if (page > 1) {
      dispatch(
        requestAnnouncements(
          generator.time,
          generator.token("getNotifications"),
          generator.userId,
          page
        )
      );
    }
  }, [page]);
  /*---------------------------------------------------------------------- */
  return (
    <div className="announcements-container">
      <h4>اعلانات</h4>
      {memorizedAnnouncementsList.map((item, index) => (
        <div key={index} className="container">
          <div className="row d-column">
            <AnnouncementItem item={item} />
          </div>
        </div>
      ))}
    </div>
  );
}

export default ComponentWrapper(Announcements);
