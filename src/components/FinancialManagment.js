import React, { useEffect, useRef } from "react";
import ComponentWrapper from "../HOC/ComponentCover";
import { requestPaymentList } from "../actions/financialManagement";
import { useDispatch, useSelector } from "react-redux";
import CustomButton from "./CustomButton";
import FinancialTransactions from "./FinancialTransactions";
import useGenerator from "../customHooks/useGenerator";

function FinancialManagment() {
  /*------------------------------ generator --------------------------- */
  const generator = useGenerator();
  /*-------------------------------------- refs ------------------------------- */
  const paymentInputRef = useRef();
  /*---------------------------- dispatch & selectors ------------------------- */
  const dispatch = useDispatch();
  const stateSelector = useSelector(({ financialManagement }) => ({
    financialManagement,
  }));
  const { finance, paymentList } = stateSelector.financialManagement;
  /*-------------------------------- side effects ---------------------------- */
  useEffect(() => {
    dispatch(
      requestPaymentList(
        generator.token("getPaymentList"),
        generator.time,
        generator.userId
      )
    );
  }, []);
  /*------------------------------- charge payment --------------------------- */
  const renderChargePayment = () => {
    return (
      <div className="payment-wrapper">
        <div className="payment-wrapper-finance">
          <div className="h6 ml-4">موجودی :</div>
          <div>
            <span className="h5 text-warning">{finance}</span>
            <span className="h5 mr-1">تومان</span>
          </div>
        </div>
        <div className="payment-form-wrapper">
          <form onSubmit={(e) => e.preventDefault()} className="d-flex">
            <input
              placeholder="مبلغ به تومان"
              className="input text-center"
              type="text"
              ref={paymentInputRef}
            />
            <div className="payment-form-btn">
              <CustomButton btnClass="yellow-btn" type="submit">
                افزایش موجودی
              </CustomButton>
            </div>
          </form>
        </div>
      </div>
    );
  };
  /*-------------------------------- transactions ---------------------------- */
  const renderTransactions = () => {
    return (
      <div>
        {paymentList &&
          paymentList.map((item, index) => (
            <FinancialTransactions key={index} item={item} />
          ))}
      </div>
    );
  };
  /*-------------------------------------------------------------------------- */
  return (
    <div className="financial-management-container d-column">
      <h3 className="title-header">مدیریت مالی</h3>
      {renderChargePayment()}
      {renderTransactions()}
    </div>
  );
}

export default ComponentWrapper(FinancialManagment);
