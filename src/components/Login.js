import React, { useState, useRef, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Logo from "./Logo";
import {
  sendMobile,
  signUp,
  getPreCode,
  sendOtpCode,
  resetsendMobileMode,
  resetInvalidOtp,
  errorOtpInvalid,
  getRules,
} from "../actions/login";
import { requestGetProfile, ClearGetProfileMode } from "../actions/profile";
import { showToastify } from "../actions/loading";
import { ToastContainer } from "react-toastify";
import Modal from "./Modal";
import check from "../assets/images/check.png";
import downArrow from "../assets/images/down-arrow.png";
import Loading from "./Loading";
import Timer from "./Timer";
import moment from "jalali-moment";
import useValidations from "../customHooks/Validations";
var md5 = require("md5");

export default function Login(props) {
  const mobileNumberInputRef = useRef(null);
  const btnModalOpenRef = useRef(null);
  const nameInputRef = useRef(null);
  const lastNameInputRef = useRef(null);
  const mobileInputRef = useRef(null);
  const regentCodeInputRef = useRef(null);
  const nationalCodeInputRef = useRef(null);
  const companyMobileInputRef = useRef(null);
  const preCodeInputRef = useRef(null);
  const companyNameInputRef = useRef(null);
  const otpInputRef = useRef(null);
  const timerRef = useRef(null);
  /*--------------------------------- states --------------------------- */
  const initailStates = {
    mode: "CHOOSE_LOGIN",
    checkMemberType: "حقیقی",
    agreementLaw: false,
    preCodeId: "",
    preCode: "",
    mobNumber: "",
    otpCode: "",
    resendTimer: false,
  };
  const [state, setState] = useState(initailStates);
  /*------------------------ custom hooks validate ---------------------- */
  const validations = useValidations();
  /*------------------------- dispatch & selectors ---------------------- */
  const dispatch = useDispatch();
  const stateSelector = useSelector((state) => ({
    loading: state.loading,
    ordersList: state.ordersList,
    login: state.login,
    profile: state.profile,
  }));
  const { loader } = stateSelector.loading;
  const { baseInfo, userInfo, rules } = stateSelector.ordersList;
  const { getProfileMode } = stateSelector.profile;
  const {
    registerSuccessMode,
    sendMobileMode,
    logId,
    invalidOtp,
  } = stateSelector.login;
  /*----------------------------- side effects -------------------------- */
  useEffect(() => {
    if (userInfo !== null) {
      props.history.push("/");
    } else {
      const time = moment().locale("").format("YYYY/MM/DD");
      const token = md5(`${time}getRules`);
      dispatch(getRules(time, token));
    }
  }, []);

  useEffect(() => {
    if (registerSuccessMode) {
      if (btnModalOpenRef.current) {
        btnModalOpenRef.current.click();
      }
    }
  }, [registerSuccessMode]);

  useEffect(() => {
    if (sendMobileMode) {
      setState({
        ...state,
        mode: "OTP_CODE",
      });
      dispatch(resetsendMobileMode());
    }
  }, [sendMobileMode]);

  useEffect(() => {
    if (getProfileMode) {
      dispatch(ClearGetProfileMode());
      props.history.push("/");
    }
  }, [getProfileMode]);

  useEffect(() => {
    if (userInfo !== null && userInfo.user_id && userInfo.user_id.length > 0) {
      const time = moment().locale("").format("YYYY/MM/DD");
      const token = md5(`${userInfo && userInfo.user_id}${time}getProfile`);
      dispatch(requestGetProfile(time, token, userInfo && userInfo.user_id));
    }
  }, [userInfo]);
  /*-------------------------------- sign in ---------------------------- */
  //handle send mobile number
  const handleSendMobile = (e) => {
    e.preventDefault();
    if (
      mobileNumberInputRef.current &&
      mobileNumberInputRef.current.value.length > 0
    ) {
      const mobileNumber = mobileNumberInputRef.current.value;
      if (validations.mobileValidate(mobileNumber)) {
        setState({
          ...state,
          resendTimer: false,
          mobNumber: mobileNumber,
        });
        const time = moment().locale("").format("YYYY/MM/DD");
        const token = md5(`${time}login`);
        dispatch(sendMobile(mobileNumber, time, token));
      } else {
        dispatch(showToastify(".شماره وارد شده صحیح نمی باشد", "error"));
      }
    } else {
      dispatch(showToastify(".لطفا شماره موبایل خود را وارد کنید", "error"));
    }
  };
  //render sign in
  const renderSignIn = () => {
    return (
      <div className="sign-in-actions-wrapper">
        <p className="sign-in-actions-wrapper-title mt-3 mb-3">
          لطفا برای ورود به اپلیکیشن شماره موبایل خود را وارد نمایید.
        </p>
        <form className="d-column" onSubmit={handleSendMobile}>
          <input
            ref={mobileNumberInputRef}
            placeholder="شماره موبایل"
            className="input text-center mt-3 mb-3"
            type="text"
            name="mobile"
          />
          <button
            type="submit"
            className="btn-df btn btn-warning active mt-3 mb-3"
          >
            ارسال
          </button>
        </form>
      </div>
    );
  };
  /*-------------------------------- sign up --------------------------- */
  //go t sign in
  const handleGoToSignIn = () => {
    setState({
      ...state,
      mode: "SIGN_IN",
    });
  };
  //render modal
  const renderSuccessRegisterModal = () => {
    return (
      <>
        <div className="justify-content">
          <img style={{ width: "106px" }} src={check} />
        </div>
      </>
    );
  };
  //choose member type
  const handleChooseMemberType = (type) => {
    if (type !== state.checkMemberType) {
      nameInputRef.current.value = "";
      lastNameInputRef.current.value = "";
      mobileInputRef.current.value = "";
      nationalCodeInputRef.current.value = "";
      if (regentCodeInputRef.current) {
        regentCodeInputRef.current.value = "";
      }
      if (companyMobileInputRef.current) {
        companyMobileInputRef.current.value = "";
      }
      if (companyNameInputRef.current) {
        companyNameInputRef.current.value = "";
      }
      if (preCodeInputRef.current) {
        preCodeInputRef.current.value = "";
      }
    }
    setState({
      ...state,
      checkMemberType: type,
    });
  };
  //handle sign up
  const handleSignUp = () => {
    if (
      nameInputRef.current.value.length === 0 &&
      lastNameInputRef.current.value.length === 0 &&
      mobileInputRef.current.value.length === 0
    ) {
      dispatch(showToastify("لطفا همه فیلد ها رو پر کنید", "error"));
    } else {
      const time = moment().locale("").format("YYYY/MM/DD");
      const token = md5(`${time}register`);
      dispatch(
        signUp(
          time,
          token,
          (state.checkMemberType === "حقیقی" && "1") ||
            (state.checkMemberType === "حقوقی" && "2") ||
            (state.checkMemberType === "متصدی" && "3"),
          nameInputRef.current.value,
          lastNameInputRef.current.value,
          mobileInputRef.current.value,
          state.checkMemberType === "حقیقی"
            ? regentCodeInputRef.current.value
            : "",
          nationalCodeInputRef.current.value,
          state.checkMemberType !== "حقیقی"
            ? companyNameInputRef.current.value
            : "",
          state.checkMemberType === "حقوقی"
            ? companyMobileInputRef.current.value
            : "",
          state.checkMemberType === "حقوقی" ? preCodeInputRef.current.value : ""
        )
      );
    }
  };
  //register before
  const handleRegisteredBefore = () => {
    setState({
      ...state,
      mode: "SIGN_IN",
    });
  };
  //agrrement law
  const handleAgreement = () => {
    setState({
      ...state,
      agreementLaw: !state.agreementLaw,
    });
  };
  //handle pre code modal
  const handleOpenPreCodeModal = () => {
    const time = moment().locale("").format("YYYY/MM/DD");
    const token = md5(`${time}checkVersionOfSystem`);
    dispatch(getPreCode(time, token));
  };
  //render sign up
  const renderSignUp = () => {
    return (
      <div className="sign-up-actions-wrapper">
        <div className="sign-up-member-type margin-5">
          <div className="sign-up-member-type-title">
            <b>لطفا نوع عضویت خود را انتخاب کنید</b>
          </div>
          <label
            onClick={() => handleChooseMemberType("حقیقی")}
            className="radio-input-container border-btm d-flex"
          >
            <input
              type="radio"
              checked={state.checkMemberType === "حقیقی" ? "checked" : ""}
              name="radio"
            />
            <span className="checkmark"></span>
            <span>اشخاص حقیقی</span>
          </label>
          <label
            onClick={() => handleChooseMemberType("حقوقی")}
            className="radio-input-container border-btm d-flex "
          >
            <input
              type="radio"
              checked={state.checkMemberType === "حقوقی" ? "checked" : ""}
              name="radio"
            />
            <span className="checkmark"></span>
            <span>اشخاص حقوقی</span>
          </label>
          <label
            onClick={() => handleChooseMemberType("متصدی")}
            className="radio-input-container d-flex"
          >
            <input
              type="radio"
              checked={state.checkMemberType === "متصدی" ? "checked" : ""}
              name="radio"
            />
            <span className="checkmark"></span>
            <span>شرکت حمل و نقل (متصدی)</span>
          </label>
        </div>
        <input
          ref={nameInputRef}
          placeholder={state.checkMemberType === "متصدی" ? "نام متصدی" : "نام"}
          className="input text-center"
        />
        <input
          ref={lastNameInputRef}
          placeholder={
            state.checkMemberType === "متصدی"
              ? "نام خانوادگی متصدی"
              : "نام خانوادگی"
          }
          className="input text-center"
        />
        <input
          ref={mobileInputRef}
          placeholder="شماره تلفن همراه"
          className="input text-center"
        />
        {state.checkMemberType !== "حقیقی" && (
          <input
            ref={companyNameInputRef}
            placeholder="نام شرکت"
            className="input text-center"
          />
        )}
        <input
          ref={nationalCodeInputRef}
          placeholder="کد ملی"
          className="input text-center"
        />
        {state.checkMemberType !== "متصدی" && (
          <input
            ref={regentCodeInputRef}
            placeholder="کد معرف (اختیاری)"
            className="input text-center"
          />
        )}
        {state.checkMemberType === "حقوقی" && (
          <div className="sign-up-legal-mobile-container">
            <input
              ref={companyMobileInputRef}
              placeholder="شماره تماس شرکت"
              className="input text-center"
            />
            <lable className="sign-up-pre-code">
              <div
                onClick={handleOpenPreCodeModal}
                data-toggle="modal"
                data-target="#pre-code"
                className="pre-code-icon-wrapper"
              >
                <img
                  style={{ width: "25px", cursor: "pointer" }}
                  alt="down-arrow"
                  src={downArrow}
                />
              </div>
              <input
                value={state.preCode}
                ref={preCodeInputRef}
                placeholder="پیش شماره"
                className="input text-center pre-code"
              />
            </lable>
          </div>
        )}

        <div className="sign-up-agree-condition">
          <label className="checkbox-input-container">
            <input
              onClick={handleAgreement}
              type="checkbox"
              checked={state.agreementLaw}
            />
            <span className="check-mark"></span>
          </label>
          <span
            data-toggle="modal"
            data-target="#sighnUp-rules"
            className="text-warning pointer"
          >
            قوانین و شرایط
          </span>
          <span className="text-white">را مطالعه کرده‌ام</span>
        </div>
        <button
          disabled={state.agreementLaw === false ? true : false}
          onClick={handleSignUp}
          className="btn-df btn btn-warning active margin-5"
        >
          ثبت نام
        </button>
        <button
          onClick={handleRegisteredBefore}
          className="sign-up-btn btn btn-df margin-5"
        >
          قبلا ثبت نام کردید؟{" "}
        </button>
        <button
          id="testbtn"
          style={{ display: "none" }}
          ref={btnModalOpenRef}
          data-toggle="modal"
          data-target="#Register-success"
        >
          open modal
        </button>
      </div>
    );
  };
  /*---------------------------- pre Code modal ----------------------- */
  //handle pick pre code
  const handlePickPreCode = (infoPreCode) => {
    setState({
      ...state,
      preCode: infoPreCode.code,
      preCodeId: infoPreCode.id,
    });
    document.getElementById(infoPreCode.id).style.color = "#e52e71";
    if (state.preCodeId !== "") {
      document.getElementById(state.preCodeId).style.color = "";
    }
  };
  //render pre modal
  const renderPreCodeModal = () => {
    return (
      <div className="pre-Code-Modal-wrapper">
        {baseInfo &&
          baseInfo.area_code.map((preCode, index) => (
            <div
              id={preCode.id}
              onClick={() => handlePickPreCode(preCode)}
              key={index}
              className="pre-Code-Modal"
            >
              <div>{preCode.title}</div>
              <div>{preCode.code}</div>
            </div>
          ))}
      </div>
    );
  };
  /*-------------------------------- otp code -------------------------- */
  //handle otp code
  const handleSendOtpCode = (e) => {
    e.preventDefault();
    if (otpInputRef.current && otpInputRef.current.value.length === 0) {
      dispatch(showToastify("لطفا کد خود را وارد کنید.", "error"));
    } else if (otpInputRef.current && otpInputRef.current.value.length !== 4) {
      dispatch(errorOtpInvalid());
    } else {
      const time = moment().locale("").format("YYYY/MM/DD");
      const token = md5(`${time}checkLogin`);
      if (otpInputRef.current) {
        dispatch(
          sendOtpCode(
            state.mobNumber,
            time,
            logId,
            otpInputRef.current.value,
            token
          )
        );
      }
    }
  };
  //otp onchange input
  const otpChangeInput = (e) => {
    if (invalidOtp) {
      dispatch(resetInvalidOtp());
    }
  };
  //reset timer
  const handleResetTimer = (timer) => {
    if (timer === 0) {
      setState({ ...state, resendTimer: true });
    }
  };
  //resend otp code
  const handleResend = () => {
    setState({
      ...state,
      resendTimer: false,
    });
    timerRef.current.handleResetTimer();
    const time = moment().locale("").format("YYYY/MM/DD");
    const token = md5(`${time}login`);
    dispatch(sendMobile(state.mobNumber, time, token));
  };
  //edit number
  const editNumber = () => {
    setState({
      ...state,
      mode: "SIGN_IN",
    });
  };
  //render otp code
  const renderOtpCode = () => {
    return (
      <div className="sign-in-actions-wrapper">
        <div className="sign-in-actions-wrapper-otp-title">
          کد ۴ رقمی یک بار مصرف که برای شماره
          <span className="text-primary">{state.mobNumber}</span>
          ارسال شده است را وارد کنید.
        </div>
        <form className="d-column" onSubmit={handleSendOtpCode}>
          <input
            placeholder="_ _ _ _"
            className={[
              "sign-in-actions-wrapper-otp-input",
              "text-center",
              invalidOtp ? "text-danger" : "",
            ].join(" ")}
            ref={otpInputRef}
            onChange={otpChangeInput}
            type="text"
          />
          {invalidOtp && (
            <p className="text-danger">کد وارد شده نا معتبر است.</p>
          )}
          <div className="sign-in-actions-wrapper-otp-btns">
            <button type="submit" className="btn-df btn btn-warning active">
              ارسال
            </button>
            <button
              className="btn-df btn border border-dark"
              onClick={editNumber}
            >
              اصلاح شماره
            </button>
          </div>
        </form>
        <div className="sign-in-actions-wrapper-otp-timer-wrapper">
          <Timer ref={timerRef} reset={(t) => handleResetTimer(t)} />
          {state.resendTimer === false ? (
            <div>
              <span>ثانیه تا ارسال مجدد</span>
            </div>
          ) : (
            <span className="pointer text-primary" onClick={handleResend}>
              ارسال مجدد
            </span>
          )}
        </div>
      </div>
    );
  };
  /*------------------------------ choose login ------------------------ */
  //sign Func
  const handleSignFunction = (mode) => {
    setState({
      ...state,
      mode: mode,
    });
  };
  //render choose login
  const renderChooseLogin = () => {
    return (
      <div>
        <button
          onClick={() => handleSignFunction("SIGN_IN")}
          className="btn-df btn btn-danger"
        >
          ورود
        </button>
        <button
          onClick={() => handleSignFunction("SIGN_UP")}
          className="btn-df btn btn-danger"
        >
          ثبت نام
        </button>
      </div>
    );
  };
  /*------------------------------------------------------------------- */
  return (
    <div className="background-view">
      {loader && <Loading />}
      <Modal
        title="ثبت نام با موفقیت انجام شد."
        noClose
        confirmBtnName="ورود به سایت"
        noCloseTag
        confirmColorBtnClass="btn-warning active"
        handleConfirm={handleGoToSignIn}
        Id="Register-success"
      >
        {renderSuccessRegisterModal()}
      </Modal>
      <Modal
        title="پیش شماره"
        noClose
        confirmBtnName="انتخاب"
        noCloseTag
        confirmColorBtnClass="btn-info active"
        Id="pre-code"
      >
        {renderPreCodeModal()}
      </Modal>
      <Modal
        title="قوانین سایت"
        noClose
        confirmBtnName="مطالعه شد"
        noCloseTag
        confirmColorBtnClass="btn-info active"
        Id="sighnUp-rules"
      >
        <b>{rules && rules.rules_user}</b>
      </Modal>
      <ToastContainer closeButton={false} style={{ fontSize: "19px" }} />
      <div className="login-container">
        {state.mode !== "SIGN_UP" && (
          <div style={{ position: "relative" }}>
            <Logo width="200px" height="200px" />
          </div>
        )}
        {state.mode === "CHOOSE_LOGIN" && renderChooseLogin()}

        {state.mode === "SIGN_IN" && renderSignIn()}
        {state.mode === "OTP_CODE" && renderOtpCode()}
      </div>
      {state.mode === "SIGN_UP" && renderSignUp()}
    </div>
  );
}
