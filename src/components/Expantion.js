import React, { useState } from "react";
import downArrow from "../assets/images/down-ar.png";
import upArrow from "../assets/images/up-ar.png";

const Expantion = ({ children, title, waitingMode, successMode, lock }) => {
  const [mode, setMode] = useState(false);
  return (
    <>
      <div
        className={mode ? "expantion-wrapper-open" : "expantion-wrapper-close"}
      >
        <div
          onClick={lock ? null : () => setMode(!mode)}
          className="d-flex justify-content-between cursor-pointer header-wrapper"
        >
          <div className="d-flex">
            <div
              className={
                (waitingMode && "bg-warning") ||
                (successMode && "bg-success") ||
                "bg-dark"
              }
              style={{
                marginTop: "5px",
                width: "35px",
                height: "35px",
                borderRadius: "50%",
              }}
            ></div>
            <div className="pr-3 h6 pt-3">{title}</div>
          </div>
          <div className="pl-3 pt-3">
            <img
              alt="arrow"
              style={{ width: "19px" }}
              src={mode ? upArrow : downArrow}
            />
          </div>
        </div>
        <div className="mt-3">{children}</div>
      </div>
    </>
  );
};

export default Expantion;
