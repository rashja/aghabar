import React, { Component } from 'react';

class Timer extends Component {
  constructor() {
    super();
    this.state = {
      minutes: 1,
      seconds: 0
    }
  }
  componentDidMount() {
      this.myInterval = setInterval(() => {
        const { seconds, minutes } = this.state
        if (seconds > 0) {
          this.setState(({ seconds }) => ({
            seconds: seconds - 1
          }))
        }
        if (seconds === 0) {
          if (minutes === 0) {
            clearInterval(this.myInterval)
          } else {
            this.setState(({ minutes }) => ({
              minutes: minutes - 1,
              seconds: 59
            }))
          }
        }
      }, 1000)
    
    }
    componentDidUpdate(prevProps,prevStates){
      const { seconds, minutes } = this.state;
      if(prevStates.seconds !== this.state.seconds){
        if(minutes === 0 && seconds === 0){
          this.props.reset(minutes + seconds )
        }
      }
    }

    handleResetTimer = () => {
      this.setState({minutes:1},()=>this.componentDidMount())
    }

  render() {
  const { minutes, seconds } = this.state;
    return (
      <div style={{margin:'0 10px'}} >
      { `0${minutes}` }:{ seconds < 10 ? `0${ seconds }` : seconds }
    </div>
    );
  }
}
  export default Timer;