import React, { useState } from "react";
import leftArrow from "../assets/images/left-arrow.png";
import CustomButton from "./CustomButton";
import { useDispatch, useSelector } from "react-redux";
import { resendRequest } from "../actions/ordersList";
import { ImageUrl } from "../apis/constants";
import Modal from "react-modal";
import useGenerator from "../customHooks/useGenerator";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

Modal.defaultStyles.overlay.backgroundColor = "rgba(0, 0, 0, 0.4)";

function SuccessOrderItem({ item }) {
  /*------------------------------ generator --------------------------- */
  const generator = useGenerator();
  /*------------------------------------- states ------------------------------ */
  const [modalIsOpen, setIsOpen] = useState(false);
  /*----------------------------- dispatch & selector ------------------------- */
  const dispatch = useDispatch();
  const {
    tr_id,
    fname_driver,
    lname_driver,
    phone_driver,
    truck_title,
    title_bar,
    bar_class,
    cost,
    des_insurance,
    type_pay,
    weight,
    height,
    width,
    package_type,
    loading_mode,
    count_address_s,
    count_address_r,
    price,
    date_start,
    date_finish,
    pic_signature_transferee,
    picture_transferee,
  } = item;
  /*-------------------------------- resend request ---------------------------- */
  const handleResendRequest = () => {
    dispatch(
      resendRequest(
        generator.token("resendRequest"),
        generator.time,
        generator.userId,
        tr_id
      )
    );
  };
  /*------------------------------- driver Info ---------------------------- */
  const renderDriverInfo = () => {
    return (
      <div>
        <div className="detail-title-item mb-2">
          <b>اطلاعات راننده</b>
        </div>
        <div className="d-flex">
          <p>نام راننده :</p>
          <p className="ml-1">{fname_driver}</p>
          <p>{lname_driver}</p>
        </div>
        <div className="d-flex">
          <p>شماره موبایل راننده :</p>
          <p>{phone_driver}</p>
        </div>
        <div className="d-flex">
          <p>نوع و برند کامیون :</p>
          <p>{truck_title}</p>
        </div>
      </div>
    );
  };
  /*---------------------------------- bar Info ---------------------------- */
  const renderBarInfo = () => {
    return (
      <div>
        <div className="detail-title-item mb-2">
          <b>اطلاعات بار</b>
        </div>
        <div className="d-flex">
          <p>نام محموله :</p>
          <p>{title_bar}</p>
        </div>
        <div className="d-flex">
          <p>کلاسه بار :</p>
          <p>{bar_class}</p>
        </div>
        <div className="d-flex">
          <p>قیمت حمل بار وهزینه صدور بارنامه :</p>
          <p>{cost}</p>
        </div>
        <div className="d-flex">
          <p>بیمه :</p>
          <p>{des_insurance}</p>
        </div>
        <div className="d-flex">
          <p>نوع پرداخت :</p>
          <p>{type_pay === "1" ? "پیش کرایه" : "پس کرایه"}</p>
        </div>
        <div className="d-flex">
          <p>وزن تقریبی بار :</p>
          <p>{weight}</p>
        </div>
        <div className="d-flex">
          <p>طول و عرض بار :</p>
          <p>{height * width}</p>
        </div>
        <div className="d-flex">
          <p>نوع بسته بندی :</p>
          <p>{package_type}</p>
        </div>
        <div className="d-flex">
          <p>حالت بارگیری :</p>
          {loading_mode.map((loading) => (
            <span className="ml-1">{loading}</span>
          ))}
        </div>
      </div>
    );
  };
  /*------------------------------- beginning Info ------------------------- */
  const renderBeginning = () => {
    return (
      <div className="detail-directions-wrapper mb-3 ">
        <div className="mb-2">
          <b>لیست آدرس های مبدا</b>
        </div>
        {count_address_s.map((add, index) => (
          <div className="detail-directions-item" key={index}>
            <div className="d-flex">
              <b>{`مبدا ${index + 1} :`}</b>
              <p>{`${add.province} ${add.city} ${add.address} ${add.plaque} ${add.unit}`}</p>
            </div>
            <div className="d-flex">
              <p>دریافت کننده :</p>
              <p>{add.name}</p>
            </div>
            <div className="d-flex">
              <p>شماره موبایل :</p>
              <p>{add.mobile}</p>
            </div>
          </div>
        ))}
      </div>
    );
  };
  /*----------------------------- destination Info ------------------------- */
  const renderDestination = () => {
    return (
      <div className="detail-directions-wrapper mb-3 ">
        <div className="mb-2">
          <b>لیست آدرس های مقصد</b>
        </div>
        {count_address_r.map((add, index) => (
          <div className="detail-directions-item" key={index}>
            <div className="d-flex">
              <b>{`مقصد ${index + 1} :`}</b>
              <p>{`${add.province} ${add.city} ${add.address} ${add.plaque} ${add.unit}`}</p>
            </div>
            <div className="d-flex">
              <p>دریافت کننده :</p>
              <p>{add.name}</p>
            </div>
            <div className="d-flex">
              <p>شماره موبایل :</p>
              <p>{add.mobile}</p>
            </div>
          </div>
        ))}
      </div>
    );
  };
  /*------------------------------- pics loading --------------------------- */
  const renderLoadingPics = () => {
    return (
      <div className="pics-loading-wrapper">
        {pic_signature_transferee !== null && (
          <div>
            <img
              style={{ width: "100px" }}
              alt="sign"
              src={`${ImageUrl}users/${pic_signature_transferee}`}
            />
          </div>
        )}
        {pic_signature_transferee !== null && (
          <div>
            <img
              style={{ width: "100px" }}
              alt="transfere"
              src={`${ImageUrl}users/${picture_transferee}`}
            />
          </div>
        )}
      </div>
    );
  };
  /*------------------------------- resend request ------------------------- */
  const renderSuccessOrderDetails = () => {
    return (
      <div className="details-order-wrapper">
        <div>
          {renderDriverInfo()}
          {renderBarInfo()}
        </div>
        <div>
          {renderBeginning()}
          {renderDestination()}
          {pic_signature_transferee !== null &&
            picture_transferee !== null &&
            pic_signature_transferee !== "" &&
            picture_transferee !== "" &&
            renderLoadingPics()}
        </div>
      </div>
    );
  };
  /*------------------------------------------------------------------------- */
  return (
    <div className="col-12 col-lg-6">
      <div className="success-order-item-wrapper">
        <div className="success-order-item-title">{bar_class}</div>
        <div className="d-column">
          <div className="success-order-item-price">
            <span>{price}</span>
            <span>تومان</span>
          </div>
          <div className="success-order-item-dates-wrapper">
            <div className="d-column">
              <p>شروع بارگیری</p>
              <div className="success-order-item-date">{date_start}</div>
            </div>
            <div className="icon-arrow-wrapper">
              <img style={{ width: "30px" }} alt="left-arrow" src={leftArrow} />
            </div>
            <div className="d-column">
              <p>پایان بارگیری</p>
              <div className="success-order-item-date">{date_finish}</div>
            </div>
          </div>
          <div className="success-order-item-btn-wrapper">
            <CustomButton onClick={() => setIsOpen(true)} btnClass="black-btn">
              جزئيات سفارش
            </CustomButton>
            <CustomButton onClick={handleResendRequest} btnClass="black-btn">
              درخواست مجدد
            </CustomButton>
          </div>
        </div>
      </div>
      <Modal
        isOpen={modalIsOpen}
        onAfterOpen={() => setIsOpen(true)}
        onRequestClose={() => setIsOpen(false)}
        style={customStyles}
        contentLabel="successItem Modal"
      >
        <div className="d-column">
          <h4
            style={{
              borderBottom: "1px solid #dad6d6",
              marginBottom: "10px",
              paddingBottom: "10px",
            }}
          >
            جزییات بیشتر
          </h4>
          {renderSuccessOrderDetails()}
          <div
            className="success-wrapper-btn"
            style={{ width: "150px", marginTop: "20px" }}
          >
            <CustomButton
              onClick={() => setIsOpen(false)}
              btnClass="yellow-btn"
            >
              بستن
            </CustomButton>
          </div>
        </div>
      </Modal>
    </div>
  );
}

export default SuccessOrderItem;
