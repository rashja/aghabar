import { useCallback } from "react";
import moment from "jalali-moment";
import { useSelector } from "react-redux";
var md5 = require("md5");

const useGenerator = () => {
  const stateSelector = useSelector(({ ordersList: { userInfo } }) => ({
    userInfo,
  }));
  const { userInfo } = stateSelector;
  const time = moment().locale("").format("YYYY/MM/DD");
  return {
    token: useCallback((_methodName) =>
      md5(`${userInfo && userInfo.id}${time}${_methodName}`)
    ),
    time,
    userId: userInfo && userInfo.id,
  };
};

export default useGenerator;
