import React, { Component } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { ToastContainer } from "react-toastify";
import Loading from "../components/Loading";

const ComponentCover = (WrappedComponent) => {
  return class extends Component {
    /*------------------------------ state -------------------------------- */
    state = {
      //your state is here
    };
    /*------------------------------ life cycles -------------------------- */
    componentDidMount() {
      const { ordersList, history } = this.props;
      if (ordersList.userInfo === null) {
        history.push("/login");
      }
    }
    /*--------------------------------------------------------------------- */
    render() {
      const { loading } = this.props;
      return (
        <>
          {loading && loading.loader && <Loading />}
          <ToastContainer closeButton={false} style={{ fontSize: "19px" }} />
          <div className="route-wrapper">
            <WrappedComponent {...this.props} />
          </div>
        </>
      );
    }
  };
};

const mapDispatch = (dispatch) => ({
  // your actions
});

const mapState = ({ ordersList, loading }) => ({
  ordersList,
  loading,
});

const ComponentWrapper = compose(
  connect(mapState, mapDispatch),
  ComponentCover
);

export default ComponentWrapper;
