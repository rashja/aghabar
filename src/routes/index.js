import React, { lazy, Suspense } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Logo from "../components/Logo";
import menu from "../assets/images/menu/menu.png";
import Menu from "../components/Menu";
import { openMenu, closeMenu } from "../actions/ordersList";
import Loading from "../components/Loading";
/*----------------------------- lazy Loading -------------------------- */
const OrdersList = lazy(() => import("../components/OrdersList"));
const Login = lazy(() => import("../components/Login"));
const InfoBar = lazy(() => import("../components/InfoBar"));
const PursuitRequest = lazy(() => import("../components/PursuitRequest"));
const Announcements = lazy(() => import("../components/Announcements"));
const NotFounPage = lazy(() => import("../components/NotFounPage"));
const Supports = lazy(() => import("../components/Supports"));
const NewRequests = lazy(() => import("../components/NewRequests"));
const CriticsAndSuggestions = lazy(() =>
  import("../components/CriticsAndSuggestions")
);
const FinancialManagment = lazy(() =>
  import("../components/FinancialManagment")
);
const ExternalTransactions = lazy(() =>
  import("../components/ExternalTransactions")
);

const Routes = () => {
  /*------------------------- dispatch & selector -------------------- */
  const dispatch = useDispatch();
  const stateSelector = useSelector((state) => ({
    ordersList: state.ordersList,
  }));
  const { userInfo, showMenu } = stateSelector.ordersList;
  /*---------------------------- handle menu ------------------------- */
  const handleOpenMenu = () => dispatch(openMenu());
  const handleCloseMenu = () => dispatch(closeMenu());
  /*------------------------------------------------------------------ */
  return (
    <>
      <BrowserRouter>
        <div className="top-header">
          {" "}
          {userInfo !== null && (
            <>
              <div className="index-logo">
                <Logo width="100px" height="100px" />
              </div>
              {showMenu === false && (
                <div className="index-navbar pointer" onClick={handleOpenMenu}>
                  <img style={{ width: "30px" }} alt="menu" src={menu} />
                </div>
              )}
              <>
                {showMenu === true && (
                  <div onClick={handleCloseMenu} className="bg-cover" />
                )}
                <Menu showMenu={showMenu} handleCloseMenu={handleCloseMenu} />
              </>
            </>
          )}
        </div>
        <Suspense fallback={<Loading />}>
          <Switch>
            <Route exact component={OrdersList} path="/" />
            <Route component={NewRequests} path="/addRequest" />
            <Route component={Login} path="/login" />
            <Route component={FinancialManagment} path="/financialManagment" />
            <Route component={Announcements} path="/announcements" />
            <Route component={Supports} path="/supports" />
            <Route component={InfoBar} path="/infoBar" />
            <Route component={PursuitRequest} path="/pursuitRequest" />
            <Route
              component={ExternalTransactions}
              path="/externalTransactions"
            />
            <Route
              component={CriticsAndSuggestions}
              path="/criticsAndSuggestions"
            />
            <Route exact component={NotFounPage} />
          </Switch>
        </Suspense>
      </BrowserRouter>
    </>
  );
};

export default Routes;
