import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { composeWithDevTools } from "redux-devtools-extension";
import rootSaga from "../saga/index";
import reducer from "../reducers/combiner";

const sagaMiddleware = createSagaMiddleware();

const persistConfig = {
  key: "app",
  storage,
  blacklist: ["login", "loading", "announcements", "profile", "newRequests"],
};

const persistedReducer = persistReducer(persistConfig, reducer);

const store = createStore(
  persistedReducer,
  composeWithDevTools(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(rootSaga);

const persistor = persistStore(store);

export { store, persistor };
